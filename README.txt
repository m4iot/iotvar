* Content of the directory:
- module iotvar-mucontext-data-model-context: context data model for using muContext platform in IoTVar.
  The context data model is adapted to IoTVar needs, thus replacing the default context model of muContext.
   * this maven module must be installed before installing the module iothandler.

- iothandler: The projects that form the iotvar middleware
 - Application: A project for running performance testing
 - Core: The main part of the middleware where the abstractions are implemented
 - Simulation: The simulator for all the platforms (Used for demos and performance testing)

 WebSite: https://fusionforge.int-evry.fr/www/iotvar/

* Build iotvar
To compile the IoTvar middlware, first you need to compile the iotvar-mucontext-data-model-context and
then the iothandler. 

Firstly, be sure that you fulfill the requirements 
- Java Version ≥ 11 
java -version 
echo $JAVA_HOME 

- Maven Version ≥ 3.2.5), then use the commands below to do both:

(cd iotvar-mucontext-data-model-context/; mvn clean install)

(cd iothandler/; mvn clean install)

Inside the iothandler folder, you will find a README explaining how to run the demos for each platform.
