/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package iotvar.mucontext.datamodel.context;

import java.net.URI;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class ContextObservable {
	public final String name;
	public final URI uri;
	@XmlElementWrapper(name = "observations")
	@XmlElement(name = "observation")
	public Set<ContextObservation<?>> observations;

	ContextObservable(final String name, final URI uri) {
		this.name = name;
		this.uri = uri;
		this.observations = new LinkedHashSet<ContextObservation<?>>();
	}

	ContextObservable() {
		this("", null);
	}

	ContextObservable addContextObservation(
			final ContextObservation<?> observation) {
		observations.add(observation);
		return this;
	}

	ContextObservable addContextObservations(
			final List<ContextObservation<?>> obs) {
		observations.addAll(obs);
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ContextObservable)) {
			return false;
		}
		ContextObservable other = (ContextObservable) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * NB: does not include context observations in order to avoid cycles.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ContextObservable [name=" + name + ", uri=" + uri + "]";
	}
}
