/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Pierrick MARIE
 */
package iotvar.mucontext.datamodel.context;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

import qocim.datamodel.QoCIndicator;


public class ContextObservation<T> {
	public final String id;
	public final T value;
	public final ContextObservable observable;
	public final ContextGPSPosition position; 
	public final Date date;
	@XmlElement(name = "qocindicator")
	public final LinkedList<QoCIndicator> list_qoCIndicator;

	ContextObservation() {
		this("", null, null, null, null);
	}

	ContextObservation(final String id, final T value, final ContextObservable observable, final ContextGPSPosition position, final Date date) {
		this.id = id;
		this.value = value;
		this.observable = observable;
		this.position=position;
		this.date = date;
		list_qoCIndicator = new LinkedList<QoCIndicator>();
	}

	public ContextObservation<?> addQoCIndicators(final List<QoCIndicator> qocIndicators) {
		list_qoCIndicator.addAll(qocIndicators);
		return this;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ContextObservation)) {
			return false;
		}
		final ContextObservation<?> other = (ContextObservation<?>) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		for (final QoCIndicator loop_qoCIndicator : list_qoCIndicator) {
			if (!other.list_qoCIndicator.contains(loop_qoCIndicator)) {
				return false;
			}
		}
		for (final QoCIndicator loop_qoCIndicator : other.list_qoCIndicator) {
			if (!list_qoCIndicator.contains(loop_qoCIndicator)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (id == null ? 0 : id.hashCode());
		return result;
	}

	/**
	 *
	 * NB: does not include context report in order to avoid cycles.
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append(
			"ContextObservation [id=" + id + ", value=" + value + ", observable=" + observable + ", QoC indicators= {");
		for (final QoCIndicator loop_qoCIndicator : list_qoCIndicator) {
			buffer.append(loop_qoCIndicator.id() + ", ");
		}
		return buffer.toString() + "} ]";
	}
}
