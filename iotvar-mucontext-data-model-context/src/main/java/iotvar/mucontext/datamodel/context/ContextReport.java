/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package iotvar.mucontext.datamodel.context;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import mucontext.datamodel.multiscalability.MultiscalabilityReport;

@XmlRootElement
public class ContextReport {
	public final String id;
	@XmlElementWrapper(name = "observations")
	@XmlElement(name = "observation")
	public Set<ContextObservation<?>> observations;
	@XmlElementWrapper(name = "multiscalabilityreports")
	@XmlElement(name = "multiscalabilityreport")
	public Set<MultiscalabilityReport> multiscalabilityreports;
	@XmlElementWrapper(name = "reports")
	@XmlElement(name = "report")
	public Set<ContextReport> included;

	ContextReport() {
		this("");
	}

	ContextReport(final String id) {
		this.id = id;
		observations = new LinkedHashSet<ContextObservation<?>>();
		multiscalabilityreports = new LinkedHashSet<MultiscalabilityReport>();
		included = new LinkedHashSet<ContextReport>();
	}

	ContextReport addContextObservation(final ContextObservation<?> observation) {
		observations.add(observation);
		return this;
	}

	ContextReport addContextObservations(final List<ContextObservation<?>> obs) {
		for (ContextObservation<?> observation : obs) {
			addContextObservation(observation);
		}
		return this;
	}

	ContextReport addContextReport(final ContextReport included) {
		this.included.add(included);
		return this;
	}

	ContextReport addContextReports(final LinkedHashSet<ContextReport> inc) {
		included.addAll(inc);
		return this;
	}

	ContextReport addMultiscalabilityReport(final MultiscalabilityReport report) {
		multiscalabilityreports.add(report);
		return this;
	}

	ContextReport addMultiscalabilityReports(final List<MultiscalabilityReport> reports) {
		for (MultiscalabilityReport report : reports) {
			addMultiscalabilityReport(report);
		}
		return this;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ContextReport)) {
			return false;
		}
		ContextReport other = (ContextReport) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (id == null ? 0 : id.hashCode());
		return result;
	}

	public ContextObservation<?> observationMatchObservable(final String observableName) {
		for (ContextObservation<?> observation : observations) {
			if (observation.observable.name.equalsIgnoreCase(observableName)) {
				return observation;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "ContextReport [id=" + id + ", observations=" + observations + ", multiscalabilityreports="
				+ multiscalabilityreports + ", included=" + included + "]";
	}
}
