/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package demo.onem2m;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import demo.qodisco.MapDisplay;
import iotvar.IoTVariableOM2M;
import iotvar.basics.FilterPublishSubscribeMethod;
import iotvar.basics.Freshness;
import iotvar.basics.HandlerStrategy;
import iotvar.basics.Location;

public class IoTVarDemoOnem2mSyncMap {
	public static void main(String[] args) throws InterruptedException {

		String om2mURL = "localhost:8080/~";

		if (args.length == 1) {
			om2mURL = args[0];
		}

		List<FilterPublishSubscribeMethod> filters = new ArrayList<FilterPublishSubscribeMethod>();

		filters.add(new FilterPublishSubscribeMethod("freshness", 10000));
		IoTVariableOM2M<Float> var = new IoTVariableOM2M<>("Temperature", new Location("EiffelTower", 48.8, 2.3, 9),
				new Freshness(1, TimeUnit.SECONDS), 10, filters, om2mURL, HandlerStrategy.SYNC);
		MapDisplay<Float> map = new MapDisplay<>(var);

		var.registerIoTListener(map);
		Thread.sleep(15000);
	}

}
