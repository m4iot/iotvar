/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package demo.mudebs;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Vector;

import iotvar.mucontext.datamodel.context.ContextDataModelFacade;
import iotvar.mucontext.datamodel.context.ContextGPSPosition;
import iotvar.mucontext.datamodel.context.ContextObservable;
import iotvar.mucontext.datamodel.context.ContextObservation;
import iotvar.mucontext.datamodel.context.ContextReport;
import mucontext.api.ContextConsumer;
import mucontext.contextcapsule.BasicContextCapsule;
import mudebs.common.MuDEBSException;

public class BasicContextConsumerWithIoTVarContextDataModel implements ContextConsumer{

	private ContextDataModelFacade facade;
	private  BasicContextCapsule capsule;

    public BasicContextConsumerWithIoTVarContextDataModel(ContextDataModelFacade facade, BasicContextCapsule capsule){
        this.facade = facade;
        this.capsule = capsule;
    }

    public final void consume(final String contextData) {
        ContextReport report = facade.unserialiseFromXML(contextData);
        System.out.println("Example context capsule consumes report = "
                + report);
        ContextObservation<?> observation = report
                .observationMatchObservable("Something");
        String uriPath = "mucontext://localhost:3000";
        ContextGPSPosition position;
        position = facade.createContextGPSPosition(48.8,2.3);
        ContextObservable observable;
        try {
            observable = facade.createContextObservable("SomethingElse",
                    new URI(uriPath + "/SomethingElse"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }
        assert(observation != null);
        ContextObservation<?> observation1 = facade.createContextObservation(
                "o42", observation.value, observable, position);
        report = facade.createContextReport("report");
        facade.addContextObservation(report, observation1);
        try {
            capsule.push("someproducercontract", facade.serialiseToXML(report));
        } catch (MuDEBSException e) {
            e.printStackTrace();
            return;
        }
        System.out.println("Example context capsule publishes report hhhhhhh = "
                + report);
    }
}
