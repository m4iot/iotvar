/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package demo.mudebs;

import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import javax.xml.transform.TransformerException;

import demo.qodisco.MapDisplay;
import demo.qodisco.TextDisplay;
import iotvar.IoTVariableMuDEBS;
import iotvar.basics.Freshness;
import iotvar.basics.Location;
import mucontext.contextcapsule.BasicContextCapsule;
import mucontext.datamodel.context.ContextDataModelFacade;
import mucontext.datamodel.contextcontract.BasicContextConsumerContract;
import mucontext.datamodel.contextcontract.BasicContextProducerContract;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.OperationalMode;
import simulation.mudebs.MuDEBSBroker;
import simulation.mudebs.MuDEBSRegister;

public class IoTVarDemoMudebsMap {

	public static void main(String[] args)
			throws MuDEBSException, URISyntaxException, TransformerException, InterruptedException {

		String brokerPath = "mudebs://localhost:2000/BrokerA";

		if (args.length == 1) {
			brokerPath = args[0];
		}

		/**
		 * the MuDEBS filter.
		 */
		final String capsuleAdvFilter = "function evaluate(doc) {" + "load(\"nashorn:mozilla_compat.js\");"
				+ "importPackage(javax.xml.xpath);" + "var xpath = XPathFactory.newInstance().newXPath();"
				+ "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
				+ "if (!n1.getTextContent().equalsIgnoreCase('SomethingElse')) return false;"
				+ "xpath = XPathFactory.newInstance().newXPath();"
				+ "var n2 = xpath.evaluate('//entity/name', n1, XPathConstants.NODE);"
				+ "if (!n2.getTextContent().equals('SomebodyElse" + "')) return false;" + "return true;}";
		final String capsuleSubFilter = "function evaluate(doc) {" + "load(\"nashorn:mozilla_compat.js\");"
				+ "importPackage(javax.xml.xpath);" + "var xpath = XPathFactory.newInstance().newXPath();"
				+ "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
				+ "if (!n1.getTextContent().equalsIgnoreCase('Something')) return false;"
				+ "xpath = XPathFactory.newInstance().newXPath();"
				+ "var n2 = xpath.evaluate('//entity/name', n1, XPathConstants.NODE);"
				+ "if (!n2.getTextContent().equals('Somebody')) return false;" + "return true;}";

		final String applicationSubFilter = "function evaluate(doc) {" + "load(\"nashorn:mozilla_compat.js\");"
				+ "importPackage(javax.xml.xpath);" + "var xpath = XPathFactory.newInstance().newXPath();"
				+ "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
				+ "if (!n1.getTextContent().equalsIgnoreCase('SomethingElse')) return false;"
				+ "xpath = XPathFactory.newInstance().newXPath();"
				+ "var n2 = xpath.evaluate('//entity/name', n1, XPathConstants.NODE);"
				+ "if (!n2.getTextContent().equals('SomebodyElse')) return false;" + "return true;}";
		final String[] basicApplicationArgs = ("--uri mudebs://localhost:2102/ApplicationHelloWorld --broker " + brokerPath)
				.split(" ");

		BasicContextCapsule capsule = new BasicContextCapsule(
				("--uri mudebs://localhost:2101/CapsuleHelloWorld --broker " + brokerPath).split(" "));
		ContextDataModelFacade facade = new ContextDataModelFacade("facade");
		BasicContextConsumer consumer = new BasicContextConsumer(facade, capsule);
		capsule.setContextConsumerContract(new BasicContextConsumerContract("someconsumercontract",
				OperationalMode.GLOBAL, capsuleSubFilter, null), consumer);
		capsule.setContextProducerContract(new BasicContextProducerContract("someproducercontract",
				OperationalMode.LOCAL, capsuleAdvFilter, null));

		IoTVariableMuDEBS<Double> var = new IoTVariableMuDEBS<>("Temperature", new Location("Home", 48.8, 2.3, 9),
				new Freshness(1, TimeUnit.SECONDS), 10, null, basicApplicationArgs, applicationSubFilter);

		MapDisplay<Double> map = new MapDisplay<>(var);
		var.registerIoTListener(map);
		Thread.sleep(5000);

	}

}
