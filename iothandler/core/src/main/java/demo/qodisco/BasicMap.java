/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package demo.qodisco;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.JMapViewerTree;
import org.openstreetmap.gui.jmapviewer.Layer;
import org.openstreetmap.gui.jmapviewer.MapMarkerCircle;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.OsmTileLoader;
import org.openstreetmap.gui.jmapviewer.events.JMVCommandEvent;
import org.openstreetmap.gui.jmapviewer.interfaces.JMapViewerEventListener;
import org.openstreetmap.gui.jmapviewer.interfaces.TileLoader;
import org.openstreetmap.gui.jmapviewer.interfaces.TileSource;
import org.openstreetmap.gui.jmapviewer.tilesources.BingAerialTileSource;
import org.openstreetmap.gui.jmapviewer.tilesources.OsmTileSource;


public class BasicMap extends JFrame implements JMapViewerEventListener {
	

	 private final JMapViewerTree treeMap;
	 private Layer markerLayer;
	
	
	 public BasicMap() {
	     treeMap = new JMapViewerTree("Zones");
	     treeMap.addLayer("marqueurs");
	
	     map().addJMVListener(this);
	     
	     map().setZoom(8);

	     setLayout(new BorderLayout());
	     setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	     setExtendedState(JFrame.MAXIMIZED_BOTH);
	     JPanel panelTop = new JPanel();
	     JPanel panelBottom = new JPanel();

	     

	     JComboBox<TileSource> tileSourceSelector = new JComboBox<TileSource>(new TileSource[] {
	             new OsmTileSource.Mapnik(),
	             new OsmTileSource.CycleMap(),
	             new BingAerialTileSource(),
	     });
	     
	     tileSourceSelector.addItemListener(new ItemListener() {

	         public void itemStateChanged(ItemEvent e) {
	             map().setTileSource((TileSource) e.getItem());
	         }
	     });
	     JComboBox<TileLoader> tileLoaderSelector;
	     tileLoaderSelector = new JComboBox<TileLoader>(new TileLoader[] {new OsmTileLoader(map())});
	     tileLoaderSelector.addItemListener(new ItemListener() {

	         public void itemStateChanged(ItemEvent e) {
	             map().setTileLoader((TileLoader) e.getItem());
	         }
	     });
	     map().setTileLoader((TileLoader) tileLoaderSelector.getSelectedItem());
	     panelTop.add(tileSourceSelector);
	     panelTop.add(tileLoaderSelector);
	     final JCheckBox showMapMarker = new JCheckBox("Map markers visible");
	     showMapMarker.setSelected(map().getMapMarkersVisible());
	     showMapMarker.addActionListener(new ActionListener() {

	         public void actionPerformed(ActionEvent e) {
	             map().setMapMarkerVisible(showMapMarker.isSelected());
	         }
	     });
	     panelBottom.add(showMapMarker);

	     
	     final JCheckBox showToolTip = new JCheckBox("ToolTip visible");
	     showToolTip.addActionListener(new ActionListener() {

	         public void actionPerformed(ActionEvent e) {
	             map().setToolTipText(null);
	         }
	     });
	     panelBottom.add(showToolTip);

	     final JCheckBox showTileGrid = new JCheckBox("Tile grid visible");
	     showTileGrid.setSelected(map().isTileGridVisible());
	     showTileGrid.addActionListener(new ActionListener() {

	         public void actionPerformed(ActionEvent e) {
	             map().setTileGridVisible(showTileGrid.isSelected());
	         }
	     });
	     panelBottom.add(showTileGrid);
	     final JCheckBox showZoomControls = new JCheckBox("Show zoom controls");
	     showZoomControls.setSelected(map().getZoomControlsVisible());
	     showZoomControls.addActionListener(new ActionListener() {

	         public void actionPerformed(ActionEvent e) {
	             map().setZoomContolsVisible(showZoomControls.isSelected());
	         }
	     });
	     panelBottom.add(showZoomControls);
	     final JCheckBox scrollWrapEnabled = new JCheckBox("Scrollwrap enabled");
	     scrollWrapEnabled.addActionListener(new ActionListener() {

	         public void actionPerformed(ActionEvent e) {
	             map().setScrollWrapEnabled(scrollWrapEnabled.isSelected());
	         }
	     });
	     panelBottom.add(scrollWrapEnabled);


	   
	     add(treeMap, BorderLayout.CENTER);

	     
	     
	     

	     map().addMouseMotionListener(new MouseAdapter() {
	         @Override
	         public void mouseMoved(MouseEvent e) {
	             Point p = e.getPoint();
	             boolean cursorHand = map().getAttribution().handleAttributionCursor(p);
	             if (cursorHand) {
	                 map().setCursor(new Cursor(Cursor.HAND_CURSOR));
	             } else {
	                 map().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	             }
	             if (showToolTip.isSelected()) map().setToolTipText(map().getPosition(p).toString());
	         }
	     });
	     
	     setVisible(true);
	 }

	 public JMapViewer map() {
	     return treeMap.getViewer();
	 }


	 public void processCommand(JMVCommandEvent command) {
	    
	     }
	 	 
	 
	 public void removeMarker(MapMarkerDot mapMarker){
		 map().removeMapMarker(mapMarker);
	 }
	 public void  updateValue(MapMarkerDot mapMarker,String type,String newValue){
		 mapMarker.setName(type + " :" + newValue);
		 map().repaint();
	 }
	 
	 public void  updateCoordAndValue(MapMarkerDot mapMarker,String type,String newValue,double newLat,double newLon){
		 mapMarker.setName(type + " :" + newValue);
		 mapMarker.setLat(newLat);
		 mapMarker.setLon(newLon);
		 
		
		 
		 map().setDisplayToFitMapMarkers();
		 
	 }
	 
	 public void addMarker (MapMarkerDot nameOfMarker,Layer markerLayer, String markerName, String markerValue, double lat, double lon){
		 nameOfMarker = new MapMarkerDot(markerLayer, markerName + " :" + markerValue, lat, lon );
		 map().addMapMarker(nameOfMarker);
		 map().setDisplayToFitMapMarkers();
		 
		 
	 }
	 
	 public void addCircleAroundDevice(MapMarkerDot nameOfMarker, double width ){
		 Coordinate c = new Coordinate(nameOfMarker.getLon(),nameOfMarker.getLat());
	     MapMarkerCircle circle = new MapMarkerCircle(c, width);
	     Color color = new Color(50, 0, 0,100);
	     circle.setBackColor(color);
	     map().addMapMarker(circle);
		 
	 }

	 


	 
	 
	 public void updateMarker(MapMarkerDot firstMarker, double lat, double lon) throws InterruptedException {
		 
		 firstMarker.setLat(lat);
		 firstMarker.setLon(lon);
		 
		 
	 }

	public Layer getMarkerLayer() {
		return markerLayer;
	}
	
	public JMapViewerTree getTreeMap() {
		return treeMap;
	}

	 
	 }