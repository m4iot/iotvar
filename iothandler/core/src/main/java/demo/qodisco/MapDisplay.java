/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package demo.qodisco;

import iotvar.IoTVarObserver;
import iotvar.IoTVariableCommon;
import iotvar.basics.Observation;
import iotvar.exception.IoTVarNotFoundException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;


import java.awt.*;
import java.text.DecimalFormat;
/**
 * MapDisplay class contains the methods to operate the marker 
 * on the basicMap for each IoTVariable
 * 
 * @author dylan
 * @see    iotvar.IoTVarObserver
 */
public class MapDisplay<T> implements IoTVarObserver<T> {
	private static final Logger logger = LogManager.getLogger(MapDisplay.class);

	/** Marker */
	private MapMarkerDot tmarker;
	/** Search Type of an IoT variable */
	private final String type;
	/** To initialize the whole map */
	private final static BasicMap basicMap = new BasicMap();
	private final static Font font = new Font("name", Font.BOLD, 20);
	
	private static DecimalFormat df2 = new DecimalFormat(".##");
	
	/**  
	 * Initialize a marker
	 * Once an IoT variable is declared, a corresponding marker is than created on the map 
	 * 
	 * @param iotvar
	 *        a {@code IoTVariable}
	 */
	public MapDisplay(IoTVariableCommon<T> iotvar) {
		logger.info("hi\n");
		tmarker = new MapMarkerDot(basicMap.getMarkerLayer(), "centrage", 
				iotvar.getSearchCriterion().getLocation().getLatitude(), 
				iotvar.getSearchCriterion().getLocation().getLongitude());
		type = iotvar.getSearchCriterion().getType();
		basicMap.map().addMapMarker(tmarker);
		tmarker.setFont(font);
		tmarker.setColor(Color.RED);tmarker.setBackColor(Color.RED);
 	

	}
	
	
	
	/** Update observations or throw an exception according to the Http response */
	public void onUpdate(Observation<T> newObservation) {
		
		/** This condition is defined in Handler: if no 
		 * result found, update(null) will be called 
		 */
		if (newObservation == null) {
			this.tmarker.setName("IoT varaible searched doesn't exist in qodisco");
			updateIssue();
		}
		this.tmarker.setLat(newObservation.getLatitude());
		this.tmarker.setLon(newObservation.getLongitude());
		this.tmarker.setName(this.type + " :" + 
				df2.format(newObservation.getValue()) + "  Date : " + 
				newObservation.getDate());
		//basicMap.map().setDisplayToFitMapMarkers();
		basicMap.map().repaint();
		
		
	}
	/** When no result return, this method will be called */
	public void updateIssue() {
		throw new IoTVarNotFoundException("IoT varaible searched doesn't exist in qodisco");
		
	}

}
