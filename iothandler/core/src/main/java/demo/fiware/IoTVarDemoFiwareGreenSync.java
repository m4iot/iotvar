/*******************************************************************************
*
* This file is part of the IoTvar middleware.
*
* Copyright (C) 2017-2019 Télécom SudParis
*
* The IoTvar software is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* The IoTvar software platform is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
*
* Contributor(s):
* Chantal Taconet chantal.taconet@telecom-sudparis.eu
* Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
* Rui Xiong (QoDisco platform/synchronous mode)
* Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
* Thomas Coutelou (refactoring, MuDebs platform)
* Charles Caporali (refactoring, MuDebs platform)
* Houssem Touansi (OM2M platform, publish/subscribe strategy)
* Jing YE (OpenDDS platform)
* Idriss El Aichaoui (OpenDDS platform)
* Pedro Victor Borges (FIWARE platform)
*
*******************************************************************************/
package demo.fiware;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import iotvar.IoTVariableFiware;
import iotvar.basics.Freshness;
import iotvar.basics.HandlerStrategy;
import iotvar.basics.Location;
import iotvar.platform.fiware.Orion;

public class IoTVarDemoFiwareGreenSync {

	public static void main(String[] args) throws InterruptedException {
		// Orion objects with information about Orion

		Orion orion = new Orion();
		
		int numberOfVariable = 5;
		int freshness = 1;
		int timeOfExecution = 300;
		int timeOfWarmUp = 60;
		
		if(args.length == 1) {
			numberOfVariable = Integer.parseInt(args[0]);
		}

		if (args.length == 4) {
			String protocol = args[0];
			String ip = args[1];
			String port = args[2];
			String notificationURL = args[3];
			orion = new Orion(protocol, ip, port, notificationURL);
		}

		FiwareTextDisplay<String> display = new FiwareTextDisplay<String>();

		// Demo Sync
		for (int i = 0; i < numberOfVariable; i++) {
			IoTVariableFiware<String> var1 = new IoTVariableFiware<String>("sensor" + (i + 1), "Sensor", "temperature",
					null, new Freshness(1, TimeUnit.SECONDS), 10,
					(String) null, orion, HandlerStrategy.SYNC_GREEN, String.class);
			var1.registerIoTListenerP(display);
		}
		
		new TimedExit(300);

		Thread.sleep(5000);
	}

}
