/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package demo.fiware;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import iotvar.IoTVariableFiware;
import iotvar.basics.Freshness;
import iotvar.basics.HandlerStrategy;
import iotvar.basics.Location;
import iotvar.platform.fiware.Orion;
import iotvar.platform.fiware.OrionFilter;
import iotvar.platform.fiware.OrionSQLOPerator;

public class IoTVarMapDemoFiwareAsync {
	public static void main(String[] args) throws InterruptedException {
		// Orion objects with information about Orion

		Orion orion = new Orion();

		if (args.length == 4) {
			String protocol = args[0];
			String ip = args[1];
			String port = args[2];
			String notificationURL = args[3];
			orion = new Orion(protocol, ip, port, notificationURL);
		}

		FiwareTextDisplay<Float> display = new FiwareTextDisplay<Float>();

		// Demo Async
		IoTVariableFiware<Float> var1 = new IoTVariableFiware<Float>("sensor1", "Sensor", "temperature",
				new Location("location", 48.6223426, 2.4404356, 100.0), new Freshness(1, TimeUnit.SECONDS), 10,
				(String) null, orion, HandlerStrategy.PUBSUB, Float.class);
		MapDisplay<Float> md = new MapDisplay<Float>(var1);
		var1.registerIoTListenerP(md);

		// Creating orion filters
		OrionFilter f1 = new OrionFilter("temperature", "20", OrionSQLOPerator.GREATER);
		OrionFilter f2 = new OrionFilter("humidity", "500", OrionSQLOPerator.LESS_EQUAL);
		ArrayList<OrionFilter> filterList = new ArrayList<OrionFilter>();
		filterList.add(f1);
		filterList.add(f2);

		// Demo Async with filters
		IoTVariableFiware<Float> var2 = new IoTVariableFiware<Float>("sensor2", "Sensor", "temperature",
				new Location("location", 48.6223426, 2.4404356, 100.0), new Freshness(1, TimeUnit.SECONDS), 10,
				filterList, orion, HandlerStrategy.PUBSUB, Float.class);
		MapDisplay<Float> md2 = new MapDisplay<Float>(var2);
		var2.registerIoTListenerP(md2);

		Thread.sleep(5000);
	}
}
