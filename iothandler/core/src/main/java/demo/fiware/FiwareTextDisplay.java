/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package demo.fiware;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import iotvar.IoTVarObserverP;
import iotvar.basics.ObservationP;
import iotvar.exception.IoTVarNotFoundException;

public class FiwareTextDisplay<T> implements IoTVarObserverP<T>{
	private static final Logger logger = LogManager.getLogger(FiwareTextDisplay.class);
	 
	public void onUpdate(ObservationP<T> newObservation) {

		if (newObservation == null) {
			updateIssue();
		}
		
//		logger.info("Received the value " + newObservation.getValue() + " in the display from " + newObservation.getId() + ".");
		
	}

	public void updateIssue() {
		throw new IoTVarNotFoundException("IoT variable searched doesn't exist in fiware");
	}

}
