/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar;

import iotvar.basics.Freshness;
import iotvar.basics.HandlerStrategy;
import iotvar.basics.Location;
import iotvar.basics.ObservationP;
import iotvar.platform.IoTVarScheduledThreadPool;
import iotvar.platform.fiware.Orion;
import iotvar.platform.fiware.OrionFilter;
import iotvar.platform.fiware.OrionGreenHandler;
import iotvar.platform.fiware.OrionPubSubHandler;
import iotvar.platform.fiware.OrionSyncHandler;
import iotvar.platform.fiware.util.NioSocketServer;
import iotvar.utils.LimitQueue;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * @author Pedro
 */
public class IoTVariableFiware<T> extends IoTVariableCommon<T> {

	/*
	 * Scheduler for the synchronous variable update
	 */
	private IoTVarScheduledThreadPool pool;

	/*
	 * Logging
	 */
	static final Logger logger = LogManager.getLogger(IoTVariableFiware.class);

	/*
	 * Orion class with entity information
	 */
	public Orion orion;

	/**
	 * The Listener where the IoT variable parameterized is registered
	 */
	private IoTVarObserverP<T> iotVarObserverP = null;

	/**
	 * The historic of observations corresponding to the IoT variable parameterized
	 */
	protected LimitQueue<ObservationP<T>> observationInstancesP = null;

	/**
	 * The list of Weak Reference of all Fiware IoT variable in IoTVar.
	 */
	@SuppressWarnings("rawtypes")
	static List<IoTVariableFiware> iotVariablesFiware = new CopyOnWriteArrayList<IoTVariableFiware>();

	/*
	 * ID of the entity
	 */
	public String id;

	/*
	 * Type of the entity
	 */
	public String type;

	/*
	 * Attribute of the entity being updated
	 */
	public String attribute;

	/*
	 * Subscription id (only used if entity is asynchronous)
	 */
	public String pubSubID = null;

	/*
	 * PubSub HTTP asynchronous server
	 */
	public NioSocketServer server;

	/*
	 * Orion Filters
	 */
	public List<OrionFilter> filters;

	/*
	 * Orion Filters string
	 */
	public String sfilters;

	/*
	 * Orion Filters string
	 */
	public Class<?> c;

	/**
	 * @param id              Id or id pattern of the Orion entity
	 * @param type            Type or type pattern of the Orion entity
	 * @param attribute       Attribute of the entity being searched
	 * @param location        Location of the entity (Not mandatory)
	 * @param freshness       Time between variable synchronous updates or minimum
	 *                        time between two asynchronous updates
	 * @param historySize     Size of the variable values history
	 * @param filters         Filter list containing Orion filters
	 * @param orion           The Orion object containing information about Orion
	 *                        (URL, port, etc)
	 * @param handlerStrategy Type of handler (Synchronous or Asynchronous)
	 */
	public IoTVariableFiware(String id, String type, String attribute, Location location, Freshness freshness,
			int historySize, List<OrionFilter> filters, Orion orion, HandlerStrategy handlerStrategy) {

		super(type, location, freshness, historySize, null);

		this.pattern = handlerStrategy;
		this.orion = orion;
		this.id = id;
		this.type = type;
		this.attribute = attribute;
		this.observationInstancesP = new LimitQueue<ObservationP<T>>(historySize);
		this.nbObservationHistory = observationInstancesP.size();
		this.filters = filters;

		// Register for a global view of the iot variable
		WeakReference<IoTVariableCommon<T>> iotvar = new WeakReference<IoTVariableCommon<T>>(this);

		// Register in the local list for in case of asynchronous variable
		if (handlerStrategy == HandlerStrategy.PUBSUB) {
			// FiwareVariableList.iotVariablesFiware.add(iotvar);
		}

		IoTVariableFiware.iotVariablesFiware.add(this);
		IoTVariableCommon.iotVariables.add(iotvar);
		pool = new IoTVarScheduledThreadPool();
		this.registerIoTVar(this);
		logger.info("IoTVariable for " + this.searchCriterion.getType() + " created");
	}

	/**
	 * @param id              Id or id pattern of the Orion entity
	 * @param type            Type or type pattern of the Orion entity
	 * @param attribute       Attribute of the entity being searched
	 * @param location        Location of the entity (Not mandatory)
	 * @param freshness       Time between variable synchronous updates or minimum
	 *                        time between two asynchronous updates
	 * @param historySize     Size of the variable values history
	 * @param filters         Filter list containing Orion filters
	 * @param orion           The Orion object containing information about Orion
	 *                        (URL, port, etc)
	 * @param handlerStrategy Type of handler (Synchronous or Asynchronous)
	 * @param c               Class for automatic variable conversion when update
	 *                        arrives
	 */
	public IoTVariableFiware(String id, String type, String attribute, Location location, Freshness freshness,
			int historySize, List<OrionFilter> filters, Orion orion, HandlerStrategy handlerStrategy, Class<?> c) {

		super(type, location, freshness, historySize, null);

		this.pattern = handlerStrategy;
		this.orion = orion;
		this.id = id;
		this.type = type;
		this.attribute = attribute;
		this.observationInstancesP = new LimitQueue<ObservationP<T>>(historySize);
		this.nbObservationHistory = observationInstancesP.size();
		this.filters = filters;
		this.c = c;

		// Register for a global view of the iot variable
		WeakReference<IoTVariableCommon<T>> iotvar = new WeakReference<IoTVariableCommon<T>>(this);

		// Register in the local list for in case of asynchronous variable
		if (handlerStrategy == HandlerStrategy.PUBSUB) {
			// FiwareVariableList.iotVariablesFiware.add(iotvar);
		}

		IoTVariableFiware.iotVariablesFiware.add(this);
		IoTVariableCommon.iotVariables.add(iotvar);
		pool = new IoTVarScheduledThreadPool();
		this.registerIoTVar(this);
		logger.info("IoTVariable for " + this.searchCriterion.getType() + " created");
	}

	/**
	 * @param id              Id or id pattern of the Orion entity
	 * @param type            Type or type pattern of the Orion entity
	 * @param attribute       Attribute of the entity being searched
	 * @param location        Location of the entity (Not mandatory)
	 * @param freshness       Time between variable synchronous updates or minimum
	 *                        time between two asynchronous updates
	 * @param historySize     Size of the variable values history
	 * @param sfilters        Filter string (See orion documentation on filtering
	 *                        for format)
	 * @param orion           The Orion object containing information about Orion
	 *                        (URL, port, etc)
	 * @param handlerStrategy Type of handler (Synchronous or Asynchronous)
	 */
	public IoTVariableFiware(String id, String type, String attribute, Location location, Freshness freshness,
			int historySize, String sfilters, Orion orion, HandlerStrategy handlerStrategy) {

		super(type, location, freshness, historySize, null);

		this.pattern = handlerStrategy;
		this.orion = orion;
		this.id = id;
		this.type = type;
		this.attribute = attribute;
		this.observationInstancesP = new LimitQueue<ObservationP<T>>(historySize);
		this.nbObservationHistory = observationInstancesP.size();
		this.sfilters = sfilters;

		// Register for a global view of the iot variable
		WeakReference<IoTVariableCommon<T>> iotvar = new WeakReference<IoTVariableCommon<T>>(this);

		// Register in the local list for in case of asynchronous variable
		if (handlerStrategy == HandlerStrategy.PUBSUB) {
			// FiwareVariableList.iotVariablesFiware.add(iotvar);
		}

		IoTVariableFiware.iotVariablesFiware.add(this);
		IoTVariableCommon.iotVariables.add(iotvar);
		pool = new IoTVarScheduledThreadPool();
		this.registerIoTVar(this);
		logger.info("IoTVariable for " + this.searchCriterion.getType() + " created");
	}

	/**
	 * @param id              Id or id pattern of the Orion entity
	 * @param type            Type or type pattern of the Orion entity
	 * @param attribute       Attribute of the entity being searched
	 * @param location        Location of the entity (Not mandatory)
	 * @param freshness       Time between variable synchronous updates or minimum
	 *                        time between two asynchronous updates
	 * @param historySize     Size of the variable values history
	 * @param sfilters        Filter string (See orion documentation on filtering
	 *                        for format)
	 * @param orion           The Orion object containing information about Orion
	 *                        (URL, port, etc)
	 * @param handlerStrategy Type of handler (Synchronous or Asynchronous)
	 * @param c               Class for automatic variable conversion when update
	 *                        arrives
	 */
	public IoTVariableFiware(String id, String type, String attribute, Location location, Freshness freshness,
			int historySize, String sfilters, Orion orion, HandlerStrategy handlerStrategy, Class<?> c) {

		super(type, location, freshness, historySize, null);

		this.pattern = handlerStrategy;
		this.orion = orion;
		this.id = id;
		this.type = type;
		this.attribute = attribute;
		this.observationInstancesP = new LimitQueue<ObservationP<T>>(historySize);
		this.nbObservationHistory = observationInstancesP.size();
		this.sfilters = sfilters;
		this.c = c;

		// Register for a global view of the iot variable
		WeakReference<IoTVariableCommon<T>> iotvar = new WeakReference<IoTVariableCommon<T>>(this);

		// Register in the local list for in case of asynchronous variable
		if (handlerStrategy == HandlerStrategy.PUBSUB) {
			// FiwareVariableList.iotVariablesFiware.add(iotvar);
		}

		IoTVariableFiware.iotVariablesFiware.add(this);
		IoTVariableCommon.iotVariables.add(iotvar);
		pool = new IoTVarScheduledThreadPool();
		this.registerIoTVar(this);
		logger.info("IoTVariable for " + this.searchCriterion.getType() + " created");
	}

	/**
	 * @param iotvar The variable to create the handler
	 */
	public void registerIoTVar(IoTVariableFiware<T> iotvar) {
		switch (this.pattern) {
		case SYNC:
			long time = searchCriterion.getFreshness() == null ? 1 : searchCriterion.getFreshness().getValue();
			OrionSyncHandler<T> syncHandler = new OrionSyncHandler<T>(this);
			pool.scheduleAtFixedRate(syncHandler, 0, time, searchCriterion.getFreshness().getTimeunit());
			break;
		case PUBSUB:
			server = new NioSocketServer();
			server.startServer();
			OrionPubSubHandler<T> pubSubHandler = new OrionPubSubHandler<T>(orion);
			String ret = pubSubHandler.subscribe(this);
			if (ret == null) {
				logger.error("Could not contact Orion on " + orion.getOrionURI());
			} else {
				System.out.println(ret);
			}
			break;
		case SYNC_GREEN:
			long time_green = searchCriterion.getFreshness() == null ? 1 : searchCriterion.getFreshness().getValue();
			OrionGreenHandler<T> syncGreenHandler = OrionGreenHandler.getInstance();
			syncGreenHandler.addNewEntity(iotvar);
			break;
		default:
			break;
		}
	}

	/**
	 * Update the observation of the IoT variable and push the observation into the
	 * Observation list.
	 * 
	 * @param newObservation The new observation to be updated
	 */
	public void updateObservationFiware(ObservationP<T> newObservation) {

		while (iotVarObserverP == null) {
			logger.info("Wait for Listener to be registered ...");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		iotVarObserverP.onUpdate(newObservation);
		observationInstancesP.push(newObservation);
		nbObservationHistory = observationInstances.size();
		logger.info("Observation updated for " + this.searchCriterion.getType() + " with value of "
				+ newObservation.getValue());
		synchronized (this) {
			if (compute == true)
				numberofmsg++;
		}

	}

	/**
	 * @param observer Set IotVar observer
	 */
	public void registerIoTListenerP(IoTVarObserverP<T> observer) {
		this.iotVarObserverP = observer;

	}

	@SuppressWarnings("rawtypes")
	public synchronized static List<IoTVariableFiware> getFiwareIotVariables() {
		// garbageCollector();
		return iotVariablesFiware;
	}

	public IoTVarObserverP<T> getIotVarObserverP() {
		// TODO Auto-generated method stub
		return this.iotVarObserverP;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		IoTVariableFiware<T> variable = (IoTVariableFiware) obj;

		if (variable.id == this.id && variable.type == this.type && variable.pattern == this.pattern
				&& variable.attribute == this.attribute) {
			return true;
		}

		return false;
	}
}
