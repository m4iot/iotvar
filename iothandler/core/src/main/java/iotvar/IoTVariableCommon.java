/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar;

import iotvar.basics.*;
import iotvar.utils.LimitQueue;
// import org.apache.log4j.LogManager;
// import org.apache.log4j.Logger;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class IoTVariableCommon<T> {
    //static final Logger logger = LogManager.getLogger(IoTVariableCommon.class);

    /**
     * The size of the historic of observation (10 by default, if not precised)
     */
    protected int nbObservationHistory;
    /**
     * The list of Weak Reference of all IoT variable in IoTVar.
     */
    static List<WeakReference<? extends IoTVariableCommon>> iotVariables = new ArrayList<WeakReference<? extends IoTVariableCommon>>();
    /**
     * The search criterion of the IoTVariable. This include the Location, the Type and the Freshness of the IoT variable
     */
    protected SearchCriterion searchCriterion;
    /**
     * The historic of observations corresponding to the IoT variable
     */
    protected LimitQueue<Observation<T>> observationInstances;
    /**
     * The Listener where the IoT variable is register
     */
    protected IoTVarObserver<T> iotVarObserver = null;
    /**
     * The pattern use by the IoT variable. This could be synchronous call, publish-subscribe or content-based publish-subscribe.
     * If not precised in the declaration of the IoT variable, it is set inside the iotvar.properties file.
     */
    protected HandlerStrategy pattern;
    /**
     * The List of filters used by the IoT variable if it uses the content-based publish-subscribe pattern
     */
    protected List<FilterPublishSubscribeMethod> filters;
    /** Perf */
    public long time = 0;
    /*
     * Counter for the messages received
     */
    public static long numberofmsg=0;
    
    
    /**
     * @param type Type of the variable being searched
     * @param location Location around where to search for the variable
     * @param freshness Update interval or quality of context for variables
     * @param historySize The size of the value history list
     * @param filters Filters to refine variable searching
     */
    public IoTVariableCommon(String type, Location location, Freshness freshness, int historySize, List<FilterPublishSubscribeMethod> filters) {
        this.searchCriterion = new SearchCriterion(type,location,freshness);
        this.observationInstances = new LimitQueue<Observation<T>>(historySize);
//        this.observationInstances.push(new Observation<T>(0,location.getLongitude(),location.getLatitude()));
        this.nbObservationHistory = 1;
        this.filters = filters;
    }
    public static boolean compute=false ;

    /**
     * Update the observation of the IoT variable and push the observation into the Observation list.
     * @param newObservation The new observation to be updated
     */
    public void updateObservation(Observation<T> newObservation) {

        while (iotVarObserver == null) {
            //logger.info("Wait for Listener to be registered ...");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        iotVarObserver.onUpdate(newObservation);
        observationInstances.push(newObservation);
        nbObservationHistory = observationInstances.size();
        //logger.info("Observation updated for " + this.searchCriterion.getType() + " with value of " + newObservation.getValue());
        synchronized(this) {
            if(compute==true)
            numberofmsg++;
        }


    }

    public LimitQueue<Observation<T>> getObservationInstances() {
        return observationInstances;
    }
    public Observation<T> getLastObservation() {
        return  observationInstances.get(nbObservationHistory-1);
    }

    /**
     * @return the iotVariables
     */
    public static List<WeakReference<? extends IoTVariableCommon>> getIotVariables() {
        garbageCollector();
        return iotVariables;
    }
    /**
     * @param iotVariables the iotVariables to set
     */
    public static void setIotVariables(List<WeakReference<? extends IoTVariableCommon>> iotVariables) {
        IoTVariableMuDEBS.iotVariables = iotVariables;
    }


    /**
     * @return the searchCriterion
     */
    public SearchCriterion getSearchCriterion() {
        return searchCriterion;
    }
    /**
     * @param searchCriterion the searchCriterion to set.
     */
    public void setSearchCriterion(SearchCriterion searchCriterion) {
        this.searchCriterion = searchCriterion;
    }


    /**
     * Remove useless instance of IoTVariable in the iotVariables list.
     */
    private static void garbageCollector(){
        for (WeakReference<? extends IoTVariableCommon> instance: iotVariables) {
            if (instance==null || instance.get()==null) {
                iotVariables.remove(instance);
            }
        }
    }
    public void registerIoTListener(IoTVarObserver<T> observer) {
        this.iotVarObserver = observer;

    }

    /**
     * @return the iotVarObserver
     */
    public IoTVarObserver<T> getIotVarObserver() {
        return iotVarObserver;
    }

    /**
     * Getter for the number of observation store in observationInstance.
     * @return nbObservationHistory
     */
    public int getNbHistory() {
        return nbObservationHistory;
    }

    /**
     * Getter for the pattern used by the IoTVariable.
     * @return pattern
     */
    public HandlerStrategy getpattern() {
        return pattern;
    }

	public static long getNumberofmsg() {
		return numberofmsg;
	}

	public static void setNumberofmsg(long numberofmsg) {
		IoTVariableCommon.numberofmsg = numberofmsg;
	}

	public boolean isCompute() {
		return compute;
	}

	public static void setCompute(boolean compute) {
		IoTVariableCommon.compute=compute;
	}


}
