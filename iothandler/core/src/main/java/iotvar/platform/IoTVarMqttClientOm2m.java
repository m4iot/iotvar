/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform;

import java.io.IOException;
import java.io.StringReader;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Random;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.*;


import iotvar.IoTVariableCommon;
import iotvar.basics.Observation;

public class IoTVarMqttClientOm2m implements MqttCallback {
	private static final Logger logger = LogManager.getLogger(IoTVarMqttClientOm2m.class);

	
	/**
	 * The Mqtt client use to communicate with the broker.
	 */
	private static MqttClient mqttClient;
	/**
	 * The iotvar related with the mqttclient.
	 */
	public IoTVariableCommon iotvar ; 

	/**
	 * Initializes the Mqtt Client, connects to the broker and initializes variables.
	 * @param brokerAddress Address of the broker to communicate with.
	 */
	public IoTVarMqttClientOm2m(String brokerAddress,String clientId){
		MemoryPersistence persistence = new MemoryPersistence();
		try {
			
                System.out.println(brokerAddress);
				mqttClient = new MqttClient(brokerAddress, clientId, persistence);
				MqttConnectOptions connOpts = new MqttConnectOptions();
				connOpts.setCleanSession(true);
				mqttClient.setCallback(this);
				mqttClient.connect(connOpts);
				logger.info("Connected to the broker at the address " + brokerAddress);
		
		} catch (MqttException e) {
			logger.error("Mqtt problem!",e);
		}
		
		
	}
	
	
	/**
	 * Send a message to the broker to subscribe to a topic and add it to the IoTTopics
	 * Hashmap.
	 * @param topicName
	 * 		The topic which the IoTVar subscribe.
	 * @param iotvar
	 * 		The IoTVariable we want to subscribe.
	 */
	public void subscribe(String topicName, IoTVariableCommon iotvar){
		try {
			mqttClient.subscribe(topicName);
			logger.info("Subscribe to the topic " + topicName);
			this.iotvar = iotvar;

		} catch (MqttException e) {
			logger.error("Mqtt error " + e);

		}
	}
	
	public void publish(String topic ,MqttMessage message ) throws MqttPersistenceException, MqttException {
		mqttClient.publish(topic, message);
		
	}
	/**
	 * Unsubscribe to a topic : Send a delete message to Qodisco, remove from the 
	 * IoTTopics hashmap and unsubscribe from the broker.
	 * @param topicName 
	 * 		The topic which the IoTVar subscribed.
	 * @param iotvar
	 * 		The IoTVariable we want to unsubscribe.
	 */
	public void unsubscribe(String topicName, WeakReference<IoTVariableCommon> iotvar) {
		try {
			mqttClient.unsubscribe(topicName);
			logger.debug("Fully disconnected from the broker and unsubscribe to the topic " + topicName);
		} catch (MqttException e) {
			logger.error("Mqtt exception error " + e);
		}
	}

	public void connectionLost(Throwable arg0) {}

	public void deliveryComplete(IMqttDeliveryToken arg0){}

	/**
	 * On the arrival of a Mqtt Message, it update the observation of the corresponding 
	 * IoTVariable, associated with the topic. To treat the message this method call the
	 * treatMqttMessage mathod of the IoTVarMiddleware.
	 */
	public void messageArrived(String arg0, MqttMessage arg1) {
		logger.info("new message arrived");

		String response = new String(arg1.getPayload());
		logger.info("|Topic : " + arg0);
		logger.info("|Message : "+ response);
		System.out.println(response.getBytes().length);
		this.treatMqttMessage(response, this.iotvar );

	}


    /**
     * This method treat the Mqtt message received by IoTVar specific to QoDisco.
     * It updates the observation of the corresponding IoT variable.
     */
    public void treatMqttMessage(String response, IoTVariableCommon iotvar) {
    	

    	JSONObject obj = new JSONObject(response) ;
    	String value = obj.getJSONObject("m2m:rqp").getJSONObject("m2m:pc").getJSONObject("m2m:cin").getString("con");
        iotvar.updateObservation(new Observation(Double.parseDouble(value),iotvar.getSearchCriterion().getLocation().getLongitude()
        ,iotvar.getSearchCriterion().getLocation().getLatitude()));


    }
}
