/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import iotvar.IoTVariableMuDEBS;
import iotvar.platform.mudebs.ContextMuDEBSConsumer;
import mucontext.api.ContextConsumerContract;
import mucontext.contextapplication.BasicContextApplication;
import iotvar.mucontext.datamodel.context.ContextDataModelFacade;
import mucontext.datamodel.contextcontract.BasicContextConsumerContract;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.OperationalMode;

public class IoTVarContextManager{

	/**
	 * HashMap of contract linked with their IoTVariables
	 */
	private static HashMap<ContextConsumerContract, IoTVariableMuDEBS> iotvarTable;
	
	/**
	 *  custom data model design for IoTvar
	 */
    private static ContextDataModelFacade facade;
    
    /**
     * Context of the Application
     */
    private static BasicContextApplication contextApp;
    
    /**
     * List of the running IoTvar
     */
	protected static List<WeakReference<IoTVariableMuDEBS>> iotvarList;

    
   

    
    public IoTVarContextManager(ContextDataModelFacade facade2, String[] basicApplicationArgs) throws MuDEBSException {
    	IoTVarContextManager.facade=facade2;
    	IoTVarContextManager.contextApp = new BasicContextApplication(basicApplicationArgs);
    	IoTVarContextManager.iotvarList=new ArrayList<WeakReference<IoTVariableMuDEBS>>();
    	IoTVarContextManager.iotvarTable = new HashMap<ContextConsumerContract, IoTVariableMuDEBS>();
    }
   
    public void subscribe(WeakReference<IoTVariableMuDEBS> iotvar, String subFilter) throws MuDEBSException {
    	BasicContextConsumerContract contract =
                new BasicContextConsumerContract(UUID.randomUUID().toString(), OperationalMode.GLOBAL,
                        subFilter, null);
    	ContextMuDEBSConsumer consumer = new ContextMuDEBSConsumer(iotvar.get(), facade);
    	contextApp.setContextConsumerContract(contract, consumer, ACK.NO);
    	synchronized (iotvarList) {
    		IoTVarContextManager.iotvarList.add(iotvar);
        	iotvarTable.put(contract,iotvarList.get(iotvarList.size()-1).get());
		}
    	
    }
    
    
	
	

}
