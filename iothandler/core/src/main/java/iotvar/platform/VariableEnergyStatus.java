package iotvar.platform;

public enum VariableEnergyStatus {
	ENERGY_LOW, ENERGY_MEDIUM, ENERGY_HIGH
}
