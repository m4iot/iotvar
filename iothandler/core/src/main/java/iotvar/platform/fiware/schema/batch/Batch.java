package iotvar.platform.fiware.schema.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "entities", "attrs", "expression" })
@Generated("jsonschema2pojo")
public class Batch {

	@JsonProperty("entities")
	private List<Entity> entities = null;
	@JsonProperty("attrs")
	private List<String> attrs = null;
	@JsonProperty("expression")
	private Expression expression;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("entities")
	public List<Entity> getEntities() {
		return entities;
	}

	@JsonProperty("entities")
	public void setEntities(List<Entity> entities) {
		this.entities = entities;
	}

	@JsonProperty("attrs")
	public List<String> getAttrs() {
		return attrs;
	}

	@JsonProperty("attrs")
	public void setAttrs(List<String> attrs) {
		this.attrs = attrs;
	}

	@JsonProperty("expression")
	public Expression getExpression() {
		return expression;
	}

	@JsonProperty("expression")
	public void setExpression(Expression expression) {
		this.expression = expression;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}