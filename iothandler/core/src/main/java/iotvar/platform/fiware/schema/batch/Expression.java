package iotvar.platform.fiware.schema.batch;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "q", "georel", "geometry", "coords" })
@Generated("jsonschema2pojo")
public class Expression {

	@JsonProperty("q")
	private String q;
	@JsonProperty("georel")
	private String georel;
	@JsonProperty("geometry")
	private String geometry;
	@JsonProperty("coords")
	private String coords;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("q")
	public String getQ() {
		return q;
	}

	@JsonProperty("q")
	public void setQ(String q) {
		this.q = q;
	}

	@JsonProperty("georel")
	public String getGeorel() {
		return georel;
	}

	@JsonProperty("georel")
	public void setGeorel(String georel) {
		this.georel = georel;
	}

	@JsonProperty("geometry")
	public String getGeometry() {
		return geometry;
	}

	@JsonProperty("geometry")
	public void setGeometry(String geometry) {
		this.geometry = geometry;
	}

	@JsonProperty("coords")
	public String getCoords() {
		return coords;
	}

	@JsonProperty("coords")
	public void setCoords(String coords) {
		this.coords = coords;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}