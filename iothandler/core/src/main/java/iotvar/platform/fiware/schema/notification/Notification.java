/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.fiware.schema.notification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "subscriptionId", "data" })
public class Notification {

	@JsonProperty("subscriptionId")
	private String subscriptionId;
	@JsonProperty("data")
	private List<Datum> data = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("subscriptionId")
	public String getSubscriptionId() {
		return subscriptionId;
	}

	@JsonProperty("subscriptionId")
	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	@JsonProperty("data")
	public List<Datum> getData() {
		return data;
	}

	@JsonProperty("data")
	public void setData(List<Datum> data) {
		this.data = data;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("subscriptionId", subscriptionId).append("data", data)
				.append("additionalProperties", additionalProperties).toString();
	}

}