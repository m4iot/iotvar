/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.fiware.schema.subscription;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "q", "mq", "georel", "geometry", "coords" })
public class Expression {

	@JsonProperty("q")
	private String q;
	@JsonProperty("mq")
	private String mq;
	@JsonProperty("georel")
	private String georel;
	@JsonProperty("geometry")
	private String geometry;
	@JsonProperty("coords")
	private String coords;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("q")
	public String getQ() {
		return q;
	}

	@JsonProperty("q")
	public void setQ(String q) {
		this.q = q;
	}

	@JsonProperty("mq")
	public String getMq() {
		return mq;
	}

	@JsonProperty("mq")
	public void setMq(String mq) {
		this.mq = mq;
	}

	@JsonProperty("georel")
	public String getGeorel() {
		return georel;
	}

	@JsonProperty("georel")
	public void setGeorel(String georel) {
		this.georel = georel;
	}

	@JsonProperty("geometry")
	public String getGeometry() {
		return geometry;
	}

	@JsonProperty("geometry")
	public void setGeometry(String geometry) {
		this.geometry = geometry;
	}

	@JsonProperty("coords")
	public String getCoords() {
		return coords;
	}

	@JsonProperty("coords")
	public void setCoords(String coords) {
		this.coords = coords;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}