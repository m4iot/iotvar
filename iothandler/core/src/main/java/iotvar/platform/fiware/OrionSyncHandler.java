/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.fiware;

import java.lang.reflect.Constructor;
import java.net.URISyntaxException;
import java.util.HashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import iotvar.IoTVariableFiware;
import iotvar.basics.ObservationP;
import iotvar.basics.SearchCriterion;

public class OrionSyncHandler<type> extends OrionHandler<type> implements Runnable {

	private static final Logger logger = LogManager.getLogger(OrionSyncHandler.class);
	
	private IoTVariableFiware<type> iotvar = null;

	public OrionSyncHandler(IoTVariableFiware<type> iotvar) {
		super(iotvar.orion);
		this.iotvar = iotvar;
	}

	private String findEntity() throws URISyntaxException {
		HashMap<String, String> params = new HashMap<String, String>();
		String q = super.filterToExpressionString(iotvar.filters);

		if (q != null) {
			params.put("q", q);
		}
		// TODO: Change the code so that the params are loaded at the time of construction of the variable
		// We order by the date modified to retrieve the most recently updated
		params.put("orderBy", "!dateModified");
		params.put("idPattern", iotvar.id);
		params.put("typePattern", iotvar.type);
		params.put("attrs", iotvar.attribute + "," + iotvar.getSearchCriterion().getLocation().getName());
		params.put("options", "values");
		params.put("offset", "0");
		params.put("limit", "1");

		if (iotvar.getSearchCriterion().getLocation() != null) {
			params.put("georel",
					"near;maxDistance:" + Double.toString(iotvar.getSearchCriterion().getLocation().getRadius()));
			params.put("geometry", "point");
			params.put("coords", iotvar.getSearchCriterion().getLocation().getLatitude() + ","
					+ iotvar.getSearchCriterion().getLocation().getLongitude());
		}

		return super.get("/entities", params);
	}

	@SuppressWarnings("unchecked")
	public void run() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();

			String json = this.findEntity();
			if(json.equals("[]")) {
				iotvar.getIotVarObserverP().updateIssue();
				return;
			}
			JsonNode jsonNode = objectMapper.readValue(json, JsonNode.class);
			JsonNode values = jsonNode.get(0);
			// type response = (type) values.get(0).asText();
			String[] localization = values.get(1).asText().split(",");

			Double lat;
			Double lon;

			if (localization == null) {
				lat = iotvar.getSearchCriterion().getLocation().getLatitude();
				lon = iotvar.getSearchCriterion().getLocation().getLongitude();
			} else {
				if (localization.length == 2) {
					lat = Double.parseDouble(localization[0]);
					lon = Double.parseDouble(localization[1]);
				} else {
					// Location is incomplete or just wrong
					lat = iotvar.getSearchCriterion().getLocation().getLatitude();
					lon = iotvar.getSearchCriterion().getLocation().getLongitude();
				}
			}

			if (iotvar.c == null) {
				type response = (type) values.get(0);
				iotvar.updateObservationFiware(
						new ObservationP<type>(response, Double.valueOf(lon), Double.valueOf(lat)));
			} else {
				Constructor<?> cons = iotvar.c.getConstructor(String.class);
				Object o = cons.newInstance(values.get(0).asText());
				iotvar.updateObservationFiware(
						new ObservationP<type>((type) o, Double.valueOf(lon), Double.valueOf(lat)));
			}

		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
