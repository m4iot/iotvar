/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.fiware;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HeaderMap;
import io.undertow.util.HeaderValues;
import io.undertow.util.Headers;
import io.undertow.util.Methods;
import iotvar.IoTVariableFiware;
import iotvar.basics.ObservationP;
import iotvar.platform.fiware.schema.notification.Notification;
import iotvar.platform.fiware.util.UuidGenerator;

public class OrionServerHttpHandler<type> implements HttpHandler {

	private static final Logger logger = LogManager.getLogger(OrionServerHttpHandler.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		if (exchange.getRequestMethod().equals(Methods.POST)) {
			exchange.getRequestReceiver().receiveFullString((exchangeReq, data) -> {
				exchangeReq.dispatch(() -> {
					// Handle incoming subscription
					ObjectMapper objectMapper = new ObjectMapper();
					try {
						Notification not = objectMapper.readValue(data, Notification.class);

						HeaderMap hm = exchange.getRequestHeaders();

						/*
						 * Check subscription is valid for the session, and if not, delete it
						 */
						for (HeaderValues header : hm) {
							String value = header.get(0);
							if (header.getHeaderName().toString().toUpperCase().equals("IOTVAR_SESSION_ID")) {
								if (!value.equals(UuidGenerator.getInstance().getSessionUUID())) {
									for (HeaderValues headert : hm) {
										String valuet = headert.get(0);
										if (headert.getHeaderName().toString().toUpperCase()
												.equals("IOTVAR_ORION_URL")) {
											logger.info("Deleting subscription " + not.getSubscriptionId());
											this.delete("/subscriptions/" + not.getSubscriptionId(), valuet);
											exchangeReq.dispatch(exchangeReq.getIoThread(), () -> {
												exchangeReq.getResponseHeaders().put(Headers.CONTENT_TYPE,
														"text/plain");
												exchangeReq.getResponseSender().send("SUCCESS");
											});
											return;
										}

									}
									logger.info("Failed to delete the subscription with id " + not.getSubscriptionId()
											+ ".It was received and does not belong to the current session "
											+ UuidGenerator.getInstance().getSessionUUID() + " .");
									exchangeReq.dispatch(exchangeReq.getIoThread(), () -> {
										exchangeReq.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
										exchangeReq.getResponseSender().send("SUCCESS");
									});
									return;
								}
							}

						}

						type value = null;
						String[] location = null;

						List<IoTVariableFiware> fiwareVars = IoTVariableFiware.getFiwareIotVariables();
						for (IoTVariableFiware ivc : fiwareVars) {
							if (ivc.id.equals(not.getData().get(0).getId().toString())) {
								logger.info(
										"Trying to update data for " + ivc.id + " (" + not.getSubscriptionId() + ")");
								for (Map.Entry<String, Object> entry : not.getData().get(0).getAdditionalProperties()
										.entrySet()) {
									if (entry.getKey() == ivc.attribute) {
										Map<String, Object> map = new HashMap<String, Object>();
										map = objectMapper.readValue(objectMapper.writeValueAsString(entry.getValue()),
												new TypeReference<Map<String, Object>>() {
												});

										logger.info("Asynchronous call v: " + (type) map.get("value"));
										value = (type) map.get("value").toString();
									} else {
										Map<String, Object> map = new HashMap<String, Object>();
										map = objectMapper.readValue(objectMapper.writeValueAsString(entry.getValue()),
												new TypeReference<Map<String, Object>>() {
												});
										if (map.get("type").toString().equals("geo:point")) {
											location = map.get("value").toString().split(",");
											logger.info("Asynchronous call l: " + map.get("value").toString());
										}
									}
								}

								Double lat;
								Double lon;

								if (location == null) {
									lat = ivc.getSearchCriterion().getLocation().getLatitude();
									lon = ivc.getSearchCriterion().getLocation().getLongitude();
								} else {
									if (location.length == 2) {
										lat = Double.parseDouble(location[0]);
										lon = Double.parseDouble(location[1]);
									} else {
										// Location is incomplete or just wrong
										lat = ivc.getSearchCriterion().getLocation().getLatitude();
										lon = ivc.getSearchCriterion().getLocation().getLongitude();
									}
								}

								if (ivc.c == null) {
									type response = (type) value;
									ivc.updateObservationFiware(
											new ObservationP<type>(response, lon, Double.valueOf(lat)));
								} else {
									Constructor<?> cons = ivc.c.getConstructor(String.class);
									Object o = cons.newInstance(value);
									ivc.updateObservationFiware(
											new ObservationP<type>((type) o, lon, Double.valueOf(lat)));
								}
								break;
							}
						}
					} catch (JsonParseException e) {
						System.out.println("Problem parsing JSON");
					} catch (JsonMappingException e) {
						System.out.println("Problem mapping JSON");
					} catch (IOException e) {
						System.out.println("IO Problem parsing JSON");
					} catch (NoSuchMethodException e) {
						System.out.println("Method not found for passed class");
					} catch (SecurityException e) {
						System.out.println("Security Exception");
					} catch (InstantiationException e) {
						System.out.println("Instantiation Exception");
					} catch (IllegalAccessException e) {
						System.out.println("Illegal Access Exception");
					} catch (IllegalArgumentException e) {
						System.out.println("Illegal Argument Exception");
					} catch (InvocationTargetException e) {
						System.out.println("Invocation Target Exception");
					}
					exchangeReq.dispatch(exchangeReq.getIoThread(), () -> {
						exchangeReq.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
						exchangeReq.getResponseSender().send("SUCCESS");
					});
				});
			}, (exchangeReq, exception) -> {
				exchangeReq.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
				exchangeReq.getResponseSender().send("FAILURE");
			});
		} else {
			throw new Exception("Method not supported by Server ");
		}
	}

	private String delete(String action, String orion) {
		try {
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpDelete delete = new HttpDelete(orion + action);
			// fiware-service and fiware-service-path are for security or context separation
			// (both not implemented yet)
			// post.setHeader("fiware-service", service);
			// post.setHeader("fiware-servicepath", path);
			HttpResponse response = httpClient.execute(delete);
			if (response != null) {
				if (response.getEntity() != null) {
					InputStream in = response.getEntity().getContent();
					return IOUtils.toString(in, "UTF-8");
				}
				return "";
			}
			return null;
		} catch (IOException ex) {
			logger.error(ex);
			logger.error("IoTVar is receiving a notification but is not able to delete it. This can cause performance issues.");
		}
		return null;
	}
}
