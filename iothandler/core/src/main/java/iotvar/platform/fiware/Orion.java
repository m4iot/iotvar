/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.fiware;

public class Orion {

	private String orionProtocol;
	private String orionHost;
	private String orionPort;
	private String notificationURL;
	public static final String API_VERSION = "v2";

	public Orion() {
		this.orionProtocol = "http";
		this.orionHost = "localhost";
		this.orionPort = "1026";
		this.notificationURL = "http://localhost";
	}

	public Orion(String orionProtocol, String orionHost, String orionPort, String notificationURL) {
		this.orionProtocol = orionProtocol;
		this.orionHost = orionHost;
		this.orionPort = orionPort;
		this.notificationURL = notificationURL;
	}

	public String getOrionProtocol() {
		return orionProtocol;
	}

	public void setOrionProtocol(String orionProtocol) {
		this.orionProtocol = orionProtocol;
	}

	public String getOrionHost() {
		return orionHost;
	}

	public void setOrionHost(String orionHost) {
		this.orionHost = orionHost;
	}

	public String getOrionPort() {
		return orionPort;
	}

	public void setOrionPort(String orionPort) {
		this.orionPort = orionPort;
	}

	public String getNotificationURL() {
		return notificationURL;
	}

	public void setNotificationURL(String notificationURL) {
		this.notificationURL = notificationURL;
	}

	public String getOrionURI() {
		return orionProtocol + "://" + orionHost + ":" + orionPort + "/" + API_VERSION;
	}
	
	public String getOrionURINoAPI() {
		return orionProtocol + "://" + orionHost + ":" + orionPort;
	}
}
