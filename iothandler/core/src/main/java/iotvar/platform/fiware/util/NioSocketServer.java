/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.fiware.util;

import java.io.IOException;
import java.net.Socket;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import io.undertow.Undertow;
import iotvar.platform.fiware.OrionServerHttpHandler;

public class NioSocketServer {

	private int serverPort = 8888;

	static final Logger logger = LogManager.getLogger(NioSocketServer.class);

	public NioSocketServer() {
		if (!available(serverPort)) {
			logger.error("Port" + serverPort + " is not available");
		}
	}

	public NioSocketServer(int serverPort) {
		this.serverPort = serverPort;

		if (!available(serverPort)) {
			logger.error("Port" + serverPort + " is not available");
		}
	}

	public void startServer() {

		if (!available(serverPort)) {
			logger.info("Port" + serverPort + " is not available (maybe the server is already started)");
			return;
		}

		Undertow server = Undertow.builder().addHttpListener(serverPort, "localhost")
				.setHandler(new OrionServerHttpHandler()).build();
		server.start();
	}

	private boolean available(int port) {
		Socket s = null;
		try {
			s = new Socket("localhost", port);
			return false;
		} catch (IOException e) {
			return true;
		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (IOException e) {
					throw new RuntimeException("Coud not handle socket closing.", e);
				}
			}
		}
	}

	public int getServerPort() {
		return this.serverPort;
	}
}