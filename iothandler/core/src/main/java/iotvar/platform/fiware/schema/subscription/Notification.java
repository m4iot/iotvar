/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/

package iotvar.platform.fiware.schema.subscription;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "attrs", "http", "attrsFormat" })
public class Notification {

	@JsonProperty("attrs")
	private List<String> attrs = null;
	@JsonProperty("http")
	private Http http;
	@JsonProperty("httpCustom")
	private HttpCustom httpCustom;
	@JsonProperty("attrsFormat")
	private String attrsFormat;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("attrs")
	public List<String> getAttrs() {
		return attrs;
	}

	@JsonProperty("attrs")
	public void setAttrs(List<String> attrs) {
		this.attrs = attrs;
	}

	@JsonProperty("http")
	public Http getHttp() {
		return http;
	}

	@JsonProperty("http")
	public void setHttp(Http http) {
		this.http = http;
	}
	
	@JsonProperty("httpCustom")
	public HttpCustom getHttpCustom() {
		return httpCustom;
	}

	@JsonProperty("httpCustom")
	public void setHttpCustom(HttpCustom httpCustom) {
		this.httpCustom = httpCustom;
	}

	@JsonProperty("attrsFormat")
	public String getAttrsFormat() {
		return attrsFormat;
	}

	@JsonProperty("attrsFormat")
	public void setAttrsFormat(String attrsFormat) {
		this.attrsFormat = attrsFormat;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("attrs", attrs).append("http", http).append("attrsFormat", attrsFormat)
				.append("additionalProperties", additionalProperties).toString();
	}

}
