package iotvar.platform.fiware;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledFuture;

import iotvar.IoTVariableFiware;
import iotvar.basics.Freshness;
import iotvar.basics.ObservationP;
import iotvar.platform.IoTVarScheduledThreadPool;
import iotvar.platform.IoTVarStatus;

public class OrionGreenHandler<T> extends OrionHandler<T> {

	private static OrionGreenHandler orionHandler = null;
	
	private CopyOnWriteArrayList<IoTVariableFiware> normalVariableList = new CopyOnWriteArrayList<IoTVariableFiware>();

	private CopyOnWriteArrayList<GreenRunnable> runnableList = new CopyOnWriteArrayList<GreenRunnable>();

	private ConcurrentHashMap<Freshness, CopyOnWriteArrayList<IoTVariableFiware>> searchMap = new ConcurrentHashMap<>();;

	private IoTVarScheduledThreadPool pool = new IoTVarScheduledThreadPool();

	private OrionGreenHandler() {
		super(null);
	}

	@SuppressWarnings("rawtypes")
	synchronized static public OrionGreenHandler getInstance() {
		if (orionHandler == null) {
			orionHandler = new OrionGreenHandler<>();
		}
		return orionHandler;
	}

	public void startHandler(Orion orion) {
		super.orion = orion;
	}
	
	synchronized public void reScheduleVariables(Freshness freshness, Freshness temporaryFreshness) {
		System.out.println("ReSchedule from " + freshness.getValue() + " " + freshness.getTimeunit() 
							+ " to " + temporaryFreshness.getValue() + " " + temporaryFreshness.getTimeunit());
		CopyOnWriteArrayList<GreenRunnable> runnableList = OrionGreenHandler.getInstance().getRunnablelist();
		System.out.println(runnableList.size());
		for (GreenRunnable<T> gr : runnableList) {
			if (temporaryFreshness == null) {
				gr.getTask().cancel(true);
				ScheduledFuture<?> task = pool.scheduleAtFixedRate(gr, 0, freshness.getValue(), freshness.getTimeunit());
				gr.setTask(task);
			} else {
				gr.getTask().cancel(true);
				ScheduledFuture<?> task = pool.scheduleAtFixedRate(gr, 0, temporaryFreshness.getValue(), temporaryFreshness.getTimeunit());
				gr.setTask(task);
			}
		}
			
	}

	synchronized public void addNewEntity(IoTVariableFiware<T> iotvar) {
		Freshness freshness = IoTVarStatus.getInstance().getFreshnessBasedOnStatus(iotvar.getSearchCriterion().getFreshness(), true);
		OrionGreenHandler<T> ogh = OrionGreenHandler.getInstance();
		// Needs to be one separated call because of the custom filters or location
		// Using normal Sync handler for this
		if (iotvar.filters != null || iotvar.getSearchCriterion().getLocation() != null) {
			System.out.println("Variable needs to be in normal sync mode (" + iotvar.id + ")");
			ogh.getNormalvariablelist().add(iotvar);
			pool.scheduleAtFixedRate(new OrionSyncHandler<T>(iotvar), 0, freshness.getValue(), freshness.getTimeunit());
			return;
		}

		if (ogh.getSearchmap().get(freshness) != null) {
			ogh.getSearchmap().get(freshness).add(iotvar);
			System.out.println("Adding new var to searchmap (" + iotvar.id + ")");
//			pool.stopRunnable(runnableList.stream().filter(run -> run.getFreshness() == freshness).findFirst().get());
//			pool.scheduleAtFixedRate(new GreenRunnable<T>(freshness, searchMap.get(freshness)), 0, freshness.getValue(),
//					freshness.getTimeunit());
		} else {
			System.out.println("Adding new var to found searchmap (" + iotvar.id + ")");
			
			CopyOnWriteArrayList<IoTVariableFiware> variableList = new CopyOnWriteArrayList<>();
			variableList.add(iotvar);
			ogh.getSearchmap().put(freshness, variableList);
			GreenRunnable<T> gr = new GreenRunnable<T>(freshness, variableList);
			ogh.getRunnablelist().add(gr);
			ScheduledFuture<?> task = pool.scheduleAtFixedRate(gr, 0, freshness.getValue(), freshness.getTimeunit());
			gr.setTask(task);
		}
		
	}
	
	public CopyOnWriteArrayList<IoTVariableFiware> getNormalvariablelist() {
		return normalVariableList;
	}

	public CopyOnWriteArrayList<GreenRunnable> getRunnablelist() {
		return this.runnableList;
	}

	public ConcurrentHashMap<Freshness, CopyOnWriteArrayList<IoTVariableFiware>> getSearchmap() {
		return searchMap;
	}

}
