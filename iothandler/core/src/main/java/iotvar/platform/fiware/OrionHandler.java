/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.fiware;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import iotvar.IoTVariableFiware;
import iotvar.basics.SearchCriterion;

public class OrionHandler<T> {

	public Orion orion;

	private static final Logger logger = LogManager.getLogger(OrionHandler.class);

	public CloseableHttpClient httpClient = HttpClientBuilder.create().build();

	public OrionHandler(Orion orion) {
		this.orion = orion;
	}

	public String get(String action, HashMap<String, String> params) throws URISyntaxException {
		try {
			URIBuilder uri = new URIBuilder(orion.getOrionURI() + action);

			for (Map.Entry<String, String> entry : params.entrySet()) {
				uri.addParameter(entry.getKey(), entry.getValue());
			}

			HttpGet get = new HttpGet(uri.build());
			get.setHeader("Accept", "application/json");
			// fiware-service and fiware-service-path are for security or context separation
			// (both not implemented yet)
			// get.setHeader("fiware-service", service);
			// get.setHeader("fiware-servicepath", path);
			HttpResponse response = httpClient.execute(get);
			if (response != null) {
				if (response.getEntity() != null) {
					InputStream in = response.getEntity().getContent();
					return IOUtils.toString(in, "UTF-8");
				}
				return "";
			}
			return null;
		} catch (IOException ex) {
			logger.error(ex);
		}
		return null;
	}

	public String post(String action, String json, HashMap<String, String> params) {
		try {
			
			URIBuilder uri = new URIBuilder(orion.getOrionURI() + action);

			for (Map.Entry<String, String> entry : params.entrySet()) {
				uri.addParameter(entry.getKey(), entry.getValue());
			}
			
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(uri.build());
			StringEntity postingString = new StringEntity(json);
			post.setEntity(postingString);
			post.setHeader("Content-type", "application/json");
			// fiware-service and fiware-service-path are for security or context separation
			// (both not implemented yet)
			// post.setHeader("fiware-service", service);
			// post.setHeader("fiware-servicepath", path);
			HttpResponse response = httpClient.execute(post);
			if (response != null) {
				if (response.getEntity() != null) {
					InputStream in = response.getEntity().getContent();
					return IOUtils.toString(in, "UTF-8");
				}
				return "";
			}
		} catch (IOException ex) {
			logger.error(ex);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String put(String action, String json, HashMap<String, String> params) {
		try {
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpPut put = new HttpPut(orion.getOrionURI() + action);
			StringEntity postingString = new StringEntity(json);
			put.setEntity(postingString);
			put.setHeader("Content-type", "application/json");
			// fiware-service and fiware-service-path are for security or context separation
			// (both not implemented yet)
			// post.setHeader("fiware-service", service);
			// post.setHeader("fiware-servicepath", path);
			HttpResponse response = httpClient.execute(put);
			if (response != null) {
				if (response.getEntity() != null) {
					InputStream in = response.getEntity().getContent();
					return IOUtils.toString(in, "UTF-8");
				}
				return "";
			}
			return null;
		} catch (IOException ex) {
			logger.error(ex);
		}
		return null;
	}

	public String delete(String action, HashMap<String, String> params) {
		try {
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpDelete delete = new HttpDelete(orion.getOrionURI() + action);
			// fiware-service and fiware-service-path are for security or context separation
			// (both not implemented yet)
			// post.setHeader("fiware-service", service);
			// post.setHeader("fiware-servicepath", path);
			HttpResponse response = httpClient.execute(delete);
			if (response != null) {
				if (response.getEntity() != null) {
					InputStream in = response.getEntity().getContent();
					return IOUtils.toString(in, "UTF-8");
				}
				return "";
			}
			return null;
		} catch (IOException ex) {
			logger.error(ex);
		}
		return null;
	}

	public static String OrionOperatorToString(OrionSQLOPerator op) {

		if (op == OrionSQLOPerator.EQUAL) {
			return "==";
		}

		if (op == OrionSQLOPerator.UNEQUAL) {
			return "!=";
		}

		if (op == OrionSQLOPerator.MATCH_PATTERN) {
			return "~=";
		}

		if (op == OrionSQLOPerator.GREATER) {
			return ">";
		}

		if (op == OrionSQLOPerator.GREATER_EQUAL) {
			return ">=";
		}

		if (op == OrionSQLOPerator.LESS) {
			return "<";
		}

		if (op == OrionSQLOPerator.LESS_EQUAL) {
			return "<=";
		}

		return "";
	}

	public static String filterToExpressionString(List<OrionFilter> filters) {

		if (filters == null) {
			return null;
		}

		if (filters.size() == 0) {
			return null;
		}

		String q = "";

		for (OrionFilter filter : filters) {
			q += filter.name + OrionOperatorToString(filter.operator) + filter.value.toString() + ";";
		}

		return q;
	}
}
