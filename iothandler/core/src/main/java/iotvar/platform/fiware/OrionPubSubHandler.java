/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.fiware;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import iotvar.IoTVariableFiware;
import iotvar.basics.SearchCriterion;
import iotvar.platform.fiware.schema.subscription.Condition;
import iotvar.platform.fiware.schema.subscription.Entity;
import iotvar.platform.fiware.schema.subscription.Expression;
import iotvar.platform.fiware.schema.subscription.Headers;
import iotvar.platform.fiware.schema.subscription.Http;
import iotvar.platform.fiware.schema.subscription.HttpCustom;
import iotvar.platform.fiware.schema.subscription.Notification;
import iotvar.platform.fiware.schema.subscription.Subject;
import iotvar.platform.fiware.schema.subscription.Subscription;
import iotvar.platform.fiware.util.UuidGenerator;

public class OrionPubSubHandler<T> extends OrionHandler<T> {

	private static final Logger logger = LogManager.getLogger(OrionSyncHandler.class);

	public OrionPubSubHandler(Orion orion) {
		super(orion);
	}

	public String subscribe(IoTVariableFiware<T> iotvar) {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("type", iotvar.type);

		ObjectMapper objectMapper = new ObjectMapper();

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 1); // Date now + 1 day for subscription
		Date date = cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"); // ISO8601 date format
		String formattedDate = dateFormat.format(date); 

		Entity ent = new Entity();
		ent.setId(iotvar.id == null || iotvar.id == "" ? ".*" : iotvar.id);
		ent.setType(iotvar.type == null || iotvar.type == "" ? ".*" : iotvar.type);

		ArrayList<String> attrs = new ArrayList<String>();
		attrs.add(iotvar.attribute);

		ArrayList<Entity> entities = new ArrayList<>();
		entities.add(ent);

		// Expression is used for content based pubsub
		Expression exp = new Expression();
		if(iotvar.sfilters != null && !iotvar.sfilters.equals("")) {
			exp.setQ(iotvar.sfilters);
		}
		String q = super.filterToExpressionString(iotvar.filters);
		if(q != null) {
			exp.setQ(q);
		}
		//exp.setMq(""); // Metadata content search (not implemented)
		if(iotvar.getSearchCriterion().getLocation() != null) {
			exp.setGeorel("near;maxDistance:"+Double.toString(iotvar.getSearchCriterion().getLocation().getRadius()));
			exp.setGeometry("point");
			exp.setCoords(iotvar.getSearchCriterion().getLocation().getLatitude()+","+iotvar.getSearchCriterion().getLocation().getLongitude());
		}

		Condition cond = new Condition();
		cond.setAttrs(attrs);
		cond.setExpression(exp);

		Subject subj = new Subject();
		subj.setEntities(entities);
		subj.setCondition(cond);
		
		Headers headers = new Headers();
		headers.setAdditionalProperty("IOTVAR_SESSION_ID", UuidGenerator.getInstance().getSessionUUID());
		headers.setAdditionalProperty("IOTVAR_ORION_URL", iotvar.orion.getOrionURI());
		
		HttpCustom httpCustom = new HttpCustom();
		httpCustom.setUrl(iotvar.orion.getNotificationURL() + ":" + iotvar.server.getServerPort());
		httpCustom.setHeaders(headers);

		Notification not = new Notification();
//		not.setAttrs(attrs);
		not.setHttpCustom(httpCustom);
		not.setAttrsFormat("normalized");

		Subscription subs = new Subscription();
		subs.setDescription("IoTVar subscription");
		subs.setSubject(subj);
		subs.setNotification(not);
		subs.setExpires(formattedDate);
		if(iotvar.getSearchCriterion().getFreshness() == null) {
			subs.setThrottling(1); // 1 second default
		}else {
			subs.setThrottling((int) iotvar.getSearchCriterion().getFreshness().getValue());
		}

		logger.info("Creating subscription for entity " + iotvar.id + " | " + iotvar.type);

		try {
			return super.post("/subscriptions", objectMapper.writeValueAsString(subs), params);
		} catch (JsonProcessingException e) {
			System.out.println("Could not parse java object to json for subscription.");
		}
		return null;
	}

	public void unsubscribe() throws Exception {
		throw new Exception("Not implemented yet.");
	}

}
