/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/

package iotvar.platform.fiware.schema.subscription;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "description", "subject", "notification", "httpCustom", "expires", "status", "throttling" })
public class Subscription {

	@JsonProperty("description")
	private String description;
	@JsonProperty("subject")
	private Subject subject;
	@JsonProperty("notification")
	private Notification notification;
	@JsonProperty("expires")
	private String expires;
	@JsonProperty("status")
	private String status;
	@JsonProperty("throttling")
	private int throttling;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("subject")
	public Subject getSubject() {
		return subject;
	}

	@JsonProperty("subject")
	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	@JsonProperty("notification")
	public Notification getNotification() {
		return notification;
	}

	@JsonProperty("notification")
	public void setNotification(Notification notification) {
		this.notification = notification;
	}

	@JsonProperty("expires")
	public String getExpires() {
		return expires;
	}

	@JsonProperty("expires")
	public void setExpires(String expires) {
		this.expires = expires;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("throttling")
	public int getThrottling() {
		return throttling;
	}

	@JsonProperty("throttling")
	public void setThrottling(int throttling) {
		this.throttling = throttling;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("description", description).append("subject", subject)
				.append("notification", notification).append("expires", expires).append("status", status)
				.append("throttling", throttling).append("additionalProperties", additionalProperties).toString();
	}

}
