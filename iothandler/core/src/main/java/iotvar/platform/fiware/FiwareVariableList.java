/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.fiware;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import iotvar.IoTVariableFiware;

public class FiwareVariableList<type> {

	/*
	 * Singleton to have all the fiware variables declared. We use Bill Pugh
	 * Singleton Implementation to achieve an implementation that can handle more
	 * threads trying to get the instance at the same time than making use of
	 * synchronize.
	 */

	@SuppressWarnings("rawtypes")
	public static List<WeakReference<? extends IoTVariableFiware>> iotVariablesFiware = new ArrayList<WeakReference<? extends IoTVariableFiware>>();

	@SuppressWarnings("unchecked")
	public static <type> FiwareVariableList<type> getInstance() {
		System.gc();
		return (FiwareVariableList<type>) SingletonHelper.INSTANCE;
	}

	private static class SingletonHelper {
		private static final FiwareVariableList<?> INSTANCE = new FiwareVariableList<>();
	}

	private FiwareVariableList() {

	}

}
