package iotvar.platform.fiware;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import iotvar.IoTVariableFiware;
import iotvar.basics.Freshness;
import iotvar.basics.ObservationP;
import iotvar.platform.IoTVarStatus;
import iotvar.platform.fiware.schema.batch.Batch;
import iotvar.platform.fiware.schema.batch.BatchEntityResponse;
import iotvar.platform.fiware.schema.batch.Entity;

public class GreenRunnable<T> implements Runnable {

	public Freshness freshness = null;
	public Freshness temporaryFreshness = null;
	public CopyOnWriteArrayList<IoTVariableFiware> variableList = null;
	private OrionHandler<T> orionHandler = null;
	private ScheduledFuture<?> task = null;

	public GreenRunnable(Freshness freshness, CopyOnWriteArrayList<IoTVariableFiware> variableList) {
		this.freshness = freshness;
		this.variableList = variableList;
		this.orionHandler = new OrionHandler<T>(variableList.get(0).orion);
	}
	
	synchronized public void setTask(ScheduledFuture<?> task) {
		this.task = task;
	}
	
	public ScheduledFuture<?> getTask() {
		return task;
	}

	@Override
	public void run() {

		try {
			String retornoOrion = orionHandler.post("/op/query", entitiesBatchJson(variableList), getParams());
			
			System.out.println("Received!");
			
			if (retornoOrion == null) {
				if (temporaryFreshness != null) {
					System.out.println("orion null and temp fresh not null");
					return;
				}
				System.out.println("change");
				temporaryFreshness = IoTVarStatus.getInstance().getFreshnessBasedOnStatus(freshness, false);
				OrionGreenHandler.getInstance().reScheduleVariables(freshness, temporaryFreshness);
				return;
			}
			
			if (temporaryFreshness != null){
				System.out.println("temp fresh and orion not null");
				temporaryFreshness = null;
				OrionGreenHandler.getInstance().reScheduleVariables(IoTVarStatus.getInstance().getFreshnessBasedOnStatus(freshness, true), temporaryFreshness);
			}

			ArrayList<BatchEntityResponse> ber = new ObjectMapper().readValue(retornoOrion,
					new TypeReference<ArrayList<BatchEntityResponse>>() {
					});

			for (int i = 0; i < ber.size(); i++) {
				BatchEntityResponse entity = ber.get(i);
				for (int j = 0; j < variableList.size(); j++) {
					IoTVariableFiware<T> iotvar = variableList.get(j);
					if (iotvar.id.equals(entity.getId())) {
						String iotvarValue = "";
						for (int k = 0; k < entity.getAdditionalProperties().size(); k++) {
							if (entity.getAdditionalProperties().containsKey(iotvar.attribute)) {
								LinkedHashMap<String, String> attrs = (LinkedHashMap<String, String>) entity
										.getAdditionalProperties().get(iotvar.attribute);
								iotvarValue = String.valueOf(attrs.get("value"));
							}
						}

						if (iotvar.c == null || iotvar.c == String.class) {
							iotvar.updateObservationFiware(new ObservationP<T>((T) iotvarValue, 0.0, 0.0, iotvar.id));
						} else {
							Constructor<?> cons = iotvar.c.getConstructor(String.class);
							Object o = cons.newInstance("");
							iotvar.updateObservationFiware(new ObservationP<T>((T) o, 0.0, 0.0, iotvar.id));
						}
					}
				}
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Freshness getFreshness() {
		return freshness;
	}

	private String entitiesBatchJson(List<IoTVariableFiware> variableList) throws JsonProcessingException {

		Batch batch = new Batch();
		ArrayList<Entity> entities = new ArrayList<Entity>();
		ArrayList<String> attrs = new ArrayList<String>();

		variableList.forEach(var -> {

			Entity entity = new Entity();
			entity.setId(var.id);
			entity.setType(var.type);
			entities.add(entity);

			attrs.add(var.attribute);
		});

		List<String> deduped = attrs.stream().distinct().collect(Collectors.toList());

		batch.setEntities(entities);
		batch.setAttrs(deduped);

		return new ObjectMapper().writeValueAsString(batch);
	}

	private HashMap<String, String> getParams() throws URISyntaxException {
		HashMap<String, String> params = new HashMap<String, String>();
//		String q = OrionHandler.filterToExpressionString(iotvar.filters);
//
//		if (q != null) {
//			params.put("q", q);
//		}

//		String attrs_location = iotvar.getSearchCriterion().getLocation() != null
//				? ("," + iotvar.getSearchCriterion().getLocation().getName())
//				: "";

		// TODO: Change the code so that the params are loaded at the time of
		// construction of the variable
		// We order by the date modified to retrieve the most recently updated
//		params.put("orderBy", "!dateModified");
//		params.put("idPattern", iotvar.id);
//		params.put("typePattern", iotvar.type);
//		params.put("attrs", iotvar.attribute + attrs_location);
//		params.put("options", "values");
		params.put("offset", "0");
		params.put("limit", "500");

//		if (iotvar.getSearchCriterion().getLocation() != null) {
//			params.put("georel",
//					"near;maxDistance:" + Double.toString(iotvar.getSearchCriterion().getLocation().getRadius()));
//			params.put("geometry", "point");
//			params.put("coords", iotvar.getSearchCriterion().getLocation().getLatitude() + ","
//					+ iotvar.getSearchCriterion().getLocation().getLongitude());
//		}

		return params;
	}

}
