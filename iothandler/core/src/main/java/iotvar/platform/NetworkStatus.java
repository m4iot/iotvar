package iotvar.platform;

public enum NetworkStatus {
	NETWORK_OK, NETWORK_MEDIUM, NETWORK_BAD, NETWORK_UNKNOWN
}
