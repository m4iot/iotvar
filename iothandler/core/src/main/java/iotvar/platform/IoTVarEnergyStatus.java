package iotvar.platform;

public enum IoTVarEnergyStatus {
	ENERGY_LOW, ENERGY_MEDIUM, ENERGY_HIGH
}
