/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.om2m;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import iotvar.IoTVariableOM2M;
import iotvar.basics.SearchCriterion;
import iotvar.platform.IoTVarMqttClientOm2m;
 
public class PublishSubscribeHandlerOM2M implements Runnable{
	private static final Logger logger = LogManager.getLogger(PublishSubscribeHandlerOM2M.class);
	 
	private IoTVarMqttClientOm2m mqttClient;
    IoTVariableOM2M iotvar;
    SearchCriterion searchCriterion ;
    String om2mAddress ;
    
    
    
    
	public PublishSubscribeHandlerOM2M(IoTVariableOM2M iotvar,SearchCriterion searchCriterion,String om2mAddress) {
	this.iotvar = iotvar;
	this.searchCriterion = searchCriterion;
	this.om2mAddress = om2mAddress;
	}



	public void run() {
	    double clientId = Math.random();
	    mqttClient = new IoTVarMqttClientOm2m("tcp://"+om2mAddress.substring(0, om2mAddress.indexOf(':'))+":1883",String.valueOf(10*clientId));	

			mqttClient.subscribe("/oneM2M/req/"+iotvar.getSearchCriterion().getType().substring(0,1).toUpperCase()+iotvar.getSearchCriterion().getType().substring(1).toLowerCase()
					+iotvar.getSearchCriterion().getLocation().getLatitude()+iotvar.getSearchCriterion().getLocation().getLongitude()
					+iotvar.getSearchCriterion().getLocation().getRadius()+"/in-cse/json",iotvar);

		

			
	}
}



