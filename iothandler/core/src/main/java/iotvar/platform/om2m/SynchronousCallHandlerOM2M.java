/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.om2m;

import iotvar.IoTVariableOM2M;
import iotvar.basics.Observation;
import iotvar.basics.SearchCriterion;
import iotvar.exception.IoTVarNotFoundException;
import iotvar.utils.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


/**
 * This class implements the handler of the Synchronous call pattern for OM2M.
 * @author courtais
 *
 */
public class SynchronousCallHandlerOM2M implements Runnable {
	private static final Logger logger = LogManager.getLogger(SynchronousCallHandlerOM2M.class);

	protected IoTVariableOM2M iotvar;
	private final String om2mAddress;
	private final String sensorPath;
	private final SearchCriterion searchCriterion;

	public SynchronousCallHandlerOM2M(IoTVariableOM2M iotvar, String om2mAddress, SearchCriterion searchCriterion) {
		this.iotvar = iotvar;
		this.om2mAddress = om2mAddress;
		this.searchCriterion = searchCriterion;
		this.sensorPath = chooseSensor(searchCriterion.getType());
	}

	/**
	 * This method choose a sensor randomly in OM2M depending on the property observed
	 * @param property
	 * 	The property you want to request
	 * @return
	 * 	The string of the path of the sensor
	 */
	private String chooseSensor(String property) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("fu", "1");
		parameters.put("lbl", "Category/"+property.toLowerCase());
		//parameters.put("lbl", "Location/"+iotvar.get().getSearchCriterion().getLocation().getName());
		parameters.put("ty", "2");
		HttpGet httpGet= buildGetMethod("/in-cse", parameters);
		HttpResponse httpResponse = new HttpResponse();
		CloseableHttpResponse closeableHttpResponse;
		String sensor = null;
		try {
			closeableHttpResponse = httpclient.execute(httpGet);

			try {
				httpResponse.setStatusCode(closeableHttpResponse.getStatusLine().getStatusCode());
				httpResponse.setBody(EntityUtils.toString(closeableHttpResponse.getEntity()));
			} finally{
				closeableHttpResponse.close();
			}

			System.out.println("HTTP Response "+httpResponse.getStatusCode()+"\n"+httpResponse.getBody());

			JSONObject obj = new JSONObject(httpResponse.getBody());






			JSONArray sensorsList = obj.getJSONArray("m2m:uril");

			String[]sensors=new String[sensorsList.length()];
			for (int i=0; i<sensorsList.length();i++)
				sensors[i]=sensorsList.getString(i);
			sensor = sensors[(new Random()).nextInt(sensors.length)];
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return sensor;

	}


	/**
	 * Builder for the get method to send to OM2M.
	 * @param path The path of om2m to communicate with
	 * @param parameters List of the parameters to add to the Get method
	 * @return getMethod
	 */
	private HttpGet buildGetMethod(String path, Map<String, String> parameters){
 		URIBuilder uri = new URIBuilder();
		uri.setScheme("http").setHost(this.om2mAddress).setPath(path);
 		if(parameters!= null) {
			for(Map.Entry<String, String> entry: parameters.entrySet()){
				uri.addParameter(entry.getKey(), entry.getValue());
			}
		}

		HttpGet getMethod = null;
		try {
			getMethod = new HttpGet(uri.build());
			getMethod.addHeader("X-M2M-Origin","admin:admin");
			getMethod.addHeader("accept", "application/json");

		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		return getMethod;
	}

	public void run() {
		try {
			logger.info("Synchronous request");
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpGet httpGet= buildGetMethod(sensorPath+"/data/la", null);

			HttpResponse httpResponse = new HttpResponse();

			try {
				CloseableHttpResponse closeableHttpResponse = httpclient.execute(httpGet);
				try{
					httpResponse.setStatusCode(closeableHttpResponse.getStatusLine().getStatusCode());
					httpResponse.setBody(EntityUtils.toString(closeableHttpResponse.getEntity()));
				}finally{
					closeableHttpResponse.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("HTTP Response "+httpResponse.getStatusCode()+"\n"+httpResponse.getBody());
 			JSONObject obj = (new JSONObject(httpResponse.getBody())).getJSONObject("m2m:cin");

	        if (obj != null) {
	        	double value = 0;
	        	value = obj.getDouble("con");
	           	iotvar.updateObservation(new Observation<Double>(value, searchCriterion.getLocation().getLatitude(), searchCriterion.getLocation().getLongitude()));
	        }


		} catch(IoTVarNotFoundException e) {
			logger.error("Search problem!",e);
		}
	}
}
