/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform;

import iotvar.IoTVariableCommon;

import iotvar.basics.Observation;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.lang.ref.WeakReference;
import java.util.HashMap;



/**
 * This class implements an Mqtt Client for the IoTVariable to connect to a broker,
 * subscribe, unsubscribe or treated message when they arrive from the broker. 
 * @author courtais
 *
 */
public class IoTVarMqttClient implements MqttCallback {
	
	private static final Logger logger = LogManager.getLogger(IoTVarMqttClient.class);

	/**
	 * The hashmap of topics declared in IoTVar and linked with the IoT variable. 
	 */
	private static HashMap<String, IoTVariableCommon> IoTTopics;
	/**
	 * The Mqtt client use to communicate with the broker.
	 */
	private static MqttClient mqttClient;
	
	/**
	 * Initializes the Mqtt Client, connects to the broker and initializes variables.
	 * @param brokerAddress Address of the broker to communicate with.
	 */
	public IoTVarMqttClient(String brokerAddress){
		MemoryPersistence persistence = new MemoryPersistence();
		try {
			if(mqttClient == null) {
                IoTTopics = new HashMap<String, IoTVariableCommon>();
                System.out.println(brokerAddress);
				mqttClient = new MqttClient(brokerAddress, "admin", persistence);
				MqttConnectOptions connOpts = new MqttConnectOptions();
				connOpts.setCleanSession(true);
				mqttClient.setCallback(this);
				mqttClient.connect(connOpts);
				logger.info("Connected to the broker at the address " + brokerAddress);
			} else {
				logger.info("Already connected to the broker at the address " + brokerAddress);
			}
		} catch (MqttException e) {
			logger.error("Mqtt problem!",e);
		}
		
		
	}
	
	/**
	 * Send a message to the broker to subscribe to a topic and add it to the IoTTopics
	 * Hashmap.
	 * @param topicName
	 * 		The topic which the IoTVar subscribe.
	 * @param iotvar
	 * 		The IoTVariable we want to subscribe.
	 */
	public void subscribe(String topicName, IoTVariableCommon iotvar){
		try {
			mqttClient.subscribe(topicName);
			IoTTopics.put(topicName, iotvar);
			logger.info("Subscribe to the topic " + topicName);
		} catch (MqttException e) {
			logger.error("Mqtt error " + e);

		}
	}
	
	public void publish(String topic ,MqttMessage message ) throws MqttPersistenceException, MqttException {
		mqttClient.publish(topic, message);
		
	}
	/**
	 * Unsubscribe to a topic : Send a delete message to Qodisco, remove from the 
	 * IoTTopics hashmap and unsubscribe from the broker.
	 * @param topicName 
	 * 		The topic which the IoTVar subscribed.
	 * @param iotvar
	 * 		The IoTVariable we want to unsubscribe.
	 */
	public void unsubscribe(String topicName, WeakReference<IoTVariableCommon> iotvar) {
		try {
			mqttClient.unsubscribe(topicName);
			logger.debug("Fully disconnected from the broker and unsubscribe to the topic " + topicName);
		} catch (MqttException e) {
			logger.error("Mqtt exception error " + e);
		}
	}

	public void connectionLost(Throwable arg0) {}

	public void deliveryComplete(IMqttDeliveryToken arg0){}

	/**
	 * On the arrival of a Mqtt Message, it update the observation of the corresponding 
	 * IoTVariable, associated with the topic. To treat the message this method call the
	 * treatMqttMessage mathod of the IoTVarMiddleware.
	 */
	public void messageArrived(String arg0, MqttMessage arg1) {

		String response = new String(arg1.getPayload());
		logger.info("|Topic : " + arg0);
		logger.info("|Message : "+ response);
		this.treatMqttMessage(response, IoTTopics.get(arg0) );

        //((PublishSubscribeHandler) IoTTopics.get(arg0)).treatMqttMessage(response, null);
	}


    /**
     * This method treat the Mqtt message received by IoTVar specific to QoDisco.
     * It updates the observation of the corresponding IoT variable.
     */
    public void treatMqttMessage(String response, IoTVariableCommon iotvar) {


        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = dbFactory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(response)));
            NodeList list = doc.getElementsByTagName("binding");

            if (list != null && list.getLength() > 0) {
                double value = 0, latitude = 0, longitude = 0;
                for (int i = 0; i < list.getLength(); ++i) {
                    Node n = list.item(i);
                    String attribut = n.getAttributes().getNamedItem("name").getNodeValue();
                    if (attribut.equalsIgnoreCase("value"))
                        value = Double.parseDouble(n.getChildNodes().item(1).getFirstChild().getNodeValue());
                    else if (attribut.equalsIgnoreCase("latitude"))
                        latitude = Double.parseDouble(n.getChildNodes().item(1).getFirstChild().getNodeValue());
                    else if (attribut.equalsIgnoreCase("longitude"))
                        longitude = Double.parseDouble(n.getChildNodes().item(1).getFirstChild().getNodeValue());

                }
                iotvar.updateObservation(new Observation(value, longitude, latitude));
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
