package iotvar.platform;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.openejb.jee.TimeUnitAdapter;

import iotvar.IoTVariableFiware;
import iotvar.basics.Freshness;
import iotvar.basics.HandlerStrategy;
import iotvar.platform.fiware.GreenRunnable;
import iotvar.platform.fiware.OrionGreenHandler;

public class IoTVarStatus<T> implements Runnable {

	private static final Float BAD_CONNECTION_PING = 350.0F;
	
	private static final Float ENERGY_VARIABLE_SECOND_CONSTANT = 20F;
	private static final Float ENERGY_NETWORK_SECOND_CONSTANT = 20F;
	private static final Float ENERGY_USED_CPU_VARIABLE_SECOND = 5F;
	
	private static final Float ENERGY_MODIFIER_NETWORK_OK_SECOND = 1F;
	private static final Float ENERGY_MODIFIER_NETWORK_MEDIUM_SECOND = 2F;
	private static final Float ENERGY_MODIFIER_NETWORK_BAD_SECOND = 3F;
	
	private static final Float ENERGY_MODIFIER_GREEN_SYNC_SECOND = 1F;
	private static final Float ENERGY_MODIFIER_PUB_SUB_SECOND = 2F;
	
	private static final Float ENERGY_MODIFIER_WIFI = 10F;
	private static final Float ENERGY_MODIFIER_ETHERNET = 1F;
	
	private static final Float ENERGY_MODIFIER_CPU = .5F;
	
	private static IoTVarStatus<?> object;

	private static boolean started = false;
	private String url = "localhost";
	private boolean simulateBadConnection = false;

	private Float calculatedEnergyUsage = -1.0f;

	private Boolean isInternetConnected = false;
	private Boolean isFiwareConnected = false;

	private Float pingInternet = -1.0f;
	private Float pingFiware = -1.0f;

	private NetworkStatus networkStatusInternet = NetworkStatus.NETWORK_UNKNOWN;
	private NetworkStatus networkStatusFiware = NetworkStatus.NETWORK_UNKNOWN;

	private NetworkInterfaceConnection networkInterfaceConnection = NetworkInterfaceConnection.UNKNOWN;

	public static IoTVarStatus<?> getInstance() {
		object = object == null ? new IoTVarStatus<>() : object;
		if (!started) {
			started = true;
			startMonitoring(object);
		}
		return object;
	}

	private static void startMonitoring(IoTVarStatus object) {
		System.out.println("Started monitoring IoTVar energy context.");
		object.initializeStatus();
		new IoTVarScheduledThreadPool().scheduleAtFixedRate(object, 1, 10, TimeUnit.SECONDS);
	}

	public boolean isInternetConnected() {
		try {
			ProcessBuilder builder = new ProcessBuilder("ping", "-c", "1", "www.google.com");
			builder.redirectErrorStream(true);
			Process process = builder.start();
			int x = process.waitFor();
			if (x == 0) {
				// System.out.println("Internet reachable (" + x + ")");
				return true;
			} else {
				// System.out.println("Internet not reachable (" + x + ")");
				return false;
			}

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean canReachServer(String url) {
		try {
			ProcessBuilder builder = new ProcessBuilder("ping", "-c", "1", url);
			builder.redirectErrorStream(true);
			Process process = builder.start();
			int x = process.waitFor();
			if (x == 0) {
				// System.out.println("Internet reachable (" + x + ")");
				return true;
			} else {
				// System.out.println("Internet not reachable (" + x + ")");
				return false;
			}

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		return false;
	}

	public Float averagePingTimeInternet() {
		if (simulateBadConnection)
			return BAD_CONNECTION_PING;
		try {
			List<String> result;
			List<Process> processes = ProcessBuilder.startPipeline(List.of(
					new ProcessBuilder("ping", "-c", "5", "www.google.com").inheritIO()
							.redirectOutput(ProcessBuilder.Redirect.PIPE),
					new ProcessBuilder("tail", "-1").redirectError(ProcessBuilder.Redirect.INHERIT),
					new ProcessBuilder("awk", "{print $4}").redirectError(ProcessBuilder.Redirect.INHERIT),
					new ProcessBuilder("cut", "-d", "/", "-f", "2").redirectError(ProcessBuilder.Redirect.INHERIT)));
			try (Scanner s = new Scanner(processes.get(processes.size() - 1).getInputStream())) {
				result = s.useDelimiter("\\R").tokens().collect(Collectors.toList());
			}
			return Float.valueOf(result.get(0));
		} catch (Exception e) {
			e.printStackTrace();
			return -1.0f;
		}
	}

	public Float averagePingTimeServer(String url) {
		if (simulateBadConnection)
			return BAD_CONNECTION_PING;
		try {
			List<String> result;
			List<Process> processes = ProcessBuilder.startPipeline(List.of(
					new ProcessBuilder("ping", "-c", "5", url).inheritIO().redirectOutput(ProcessBuilder.Redirect.PIPE),
					new ProcessBuilder("tail", "-1").redirectError(ProcessBuilder.Redirect.INHERIT),
					new ProcessBuilder("awk", "{print $4}").redirectError(ProcessBuilder.Redirect.INHERIT),
					new ProcessBuilder("cut", "-d", "/", "-f", "2").redirectError(ProcessBuilder.Redirect.INHERIT)));
			try (Scanner s = new Scanner(processes.get(processes.size() - 1).getInputStream())) {
				result = s.useDelimiter("\\R").tokens().collect(Collectors.toList());
			}
			return Float.valueOf(result.get(0));
		} catch (Exception e) {
			e.printStackTrace();
			return -1.0f;
		}
	}

	public NetworkInterfaceConnection getNetworkInterface() {
		try {
			ProcessBuilder builder = new ProcessBuilder("nmcli", "radio", "wifi");
			builder.redirectErrorStream(true);
			Process process = builder.start();
			int x = process.waitFor();
			List<String> result;
			if (x == 0) {
				try (Scanner s = new Scanner(process.getInputStream())) {
					result = s.useDelimiter("\\R").tokens().collect(Collectors.toList());
				}
				if (result.get(0).toString().equals("enabled")) {
					return NetworkInterfaceConnection.WIFI;
				} else {
					if (canReachServer(getUrl())) {
						return NetworkInterfaceConnection.ETHERNET;
					}
				}
			} else {
				return NetworkInterfaceConnection.UNKNOWN;
			}

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		return NetworkInterfaceConnection.UNKNOWN;
	}

	public NetworkStatus getNetworkStatusBasedOnPing(Float ping) {
		if (ping == -1) {
			return NetworkStatus.NETWORK_UNKNOWN;
		} else if (ping <= 150) {
			return NetworkStatus.NETWORK_OK;
		} else if (ping > 150 && ping <= 300) {
			return NetworkStatus.NETWORK_MEDIUM;
		} else {
			return NetworkStatus.NETWORK_BAD;
		}
	}

	public VariableEnergyStatus calculateVariablePatternEnergyStatus(IoTVariableFiware<T> variable) {
		switch (variable.getpattern()) {
		case SYNC: {
			return VariableEnergyStatus.ENERGY_HIGH;
		}
		case PUBSUB: {
			return VariableEnergyStatus.ENERGY_MEDIUM;
		}
		case SYNC_GREEN: {
			return VariableEnergyStatus.ENERGY_LOW;
		}
		case ASYNC_GREEN: {
			return VariableEnergyStatus.ENERGY_LOW;
		}
		default:
			return VariableEnergyStatus.ENERGY_HIGH;
		}
	}

	private void initializeStatus() {
		IoTVarStatus ivs = IoTVarStatus.getInstance();

		Boolean isInternetConnected = ivs.isInternetConnected();
		Boolean isFiwareConnected = ivs.canReachServer(IoTVarStatus.getInstance().url);

		Float pingInternet = ivs.averagePingTimeInternet();
		Float pingFiware = ivs.averagePingTimeServer(IoTVarStatus.getInstance().url);

		NetworkStatus nsi = ivs.getNetworkStatusBasedOnPing(pingInternet);
		NetworkStatus nsf = ivs.getNetworkStatusBasedOnPing(pingFiware);

		NetworkInterfaceConnection nic = ivs.getNetworkInterface();

		ivs.setIsInternetConnected(isInternetConnected);
		ivs.setIsFiwareConnected(isFiwareConnected);
		ivs.setNetworkInterfaceConnection(nic);
		ivs.setNetworkStatusInternet(nsi);
		ivs.setNetworkStatusFiware(nsi);
		ivs.setPingInternet(pingInternet);
		ivs.setPingFiware(pingFiware);
	}
	
	public Freshness getFreshnessBasedOnStatus(Freshness freshness, Boolean isOrionReachable) {
		IoTVarStatus ivs = IoTVarStatus.getInstance();

//		Boolean isInternetConnected = ivs.isInternetConnected();
		Boolean isFiwareConnected = ivs.getIsFiwareConnected();

//		Float pingInternet = ivs.averagePingTimeInternet();
		Float pingFiware = ivs.getPingFiware();

//		NetworkStatus nsi = ivs.getNetworkStatusBasedOnPing(pingInternet);
		NetworkStatus nsf = ivs.getNetworkStatusBasedOnPing(pingFiware);

		NetworkInterfaceConnection nic = ivs.getNetworkInterface();
		
		Freshness orionTu = calculateIsOrionReachableConnectedFreshness(isOrionReachable);
		Freshness fiwareTu = calculateIsFiwareServerConnectedFreshness(isFiwareConnected);
		Freshness networkStatusTu = calculateNetworkStatusFreshness(nsf);
		Freshness networkInterfaceTu = calculateNetworkInterfaceFreshness(nic);
		
		long receivedFreshness = TimeUnit.SECONDS.convert(freshness.getValue(), freshness.getTimeunit());
		long totalFreshnessInSeconds = receivedFreshness + orionTu.getValue() + fiwareTu.getValue() + networkStatusTu.getValue() + networkInterfaceTu.getValue();

		return new Freshness(totalFreshnessInSeconds, TimeUnit.SECONDS);
	}

	private Freshness calculateIsOrionReachableConnectedFreshness(Boolean isOrionReachable) {
		return isOrionReachable ? new Freshness(0, TimeUnit.SECONDS) : new Freshness(10, TimeUnit.SECONDS);
	}

	private Freshness calculateIsFiwareServerConnectedFreshness(Boolean isFiwareConnected) {
		return isFiwareConnected ? new Freshness(0, TimeUnit.SECONDS) : new Freshness(10, TimeUnit.SECONDS);
	}

	private Freshness calculateNetworkStatusFreshness(NetworkStatus ns) {
		switch (ns) {
		case NETWORK_MEDIUM: {
			return new Freshness(5, TimeUnit.SECONDS);
		}
		case NETWORK_BAD: {
			return new Freshness(10, TimeUnit.SECONDS);
		}
		default:
			return new Freshness(0, TimeUnit.SECONDS);
		}
	}

	private Freshness calculateNetworkInterfaceFreshness(NetworkInterfaceConnection nic) {
		switch (nic) {
		case ETHERNET: {
			return new Freshness(0, TimeUnit.SECONDS);
		}
		case WIFI: {
			return new Freshness(2, TimeUnit.SECONDS);
		}
		default:
			return new Freshness(0, TimeUnit.SECONDS);
		}
	}
	
	public float calculateTotalEnergyConsumptionUsingEnergyModel() {
		OrionGreenHandler ogh = OrionGreenHandler.getInstance();
		IoTVarStatus ivs = IoTVarStatus.getInstance();
		
		float energyModifierNetworkStatus = getNetworkStatusEnergyModifier(ivs.getNetworkStatusFiware());
		float energyModifierNetworkInterface = getNetworkInterfaceEnergyModifier(ivs.getNetworkInterface());
		
		float energyUsedByNetwork = ENERGY_NETWORK_SECOND_CONSTANT;
		float energyUsedByCpu = ENERGY_USED_CPU_VARIABLE_SECOND;
		
		float totalEnergyConsumptionOneSecond = 0f;
		CopyOnWriteArrayList<GreenRunnable> grl = ogh.getRunnablelist();
		for(GreenRunnable gr : grl) {
			long variableFreshnessSeconds = TimeUnit.SECONDS.convert(gr.getFreshness().getValue(), gr.getFreshness().getTimeunit());
			totalEnergyConsumptionOneSecond += 
					(
						ENERGY_VARIABLE_SECOND_CONSTANT 
						+
						(energyUsedByNetwork * (energyModifierNetworkStatus * energyModifierNetworkInterface)) 
						+
						energyUsedByCpu * (ENERGY_MODIFIER_CPU)
					) / variableFreshnessSeconds;
		}
		
		return totalEnergyConsumptionOneSecond;
	}
	
	private float getNetworkInterfaceEnergyModifier(NetworkInterfaceConnection nic) {
		switch (nic) {
		case ETHERNET: {
			return ENERGY_MODIFIER_ETHERNET;
		}
		case WIFI: {
			return ENERGY_MODIFIER_WIFI;
		}
		default:
			return ENERGY_MODIFIER_ETHERNET;
		}
	}
	
	private float getNetworkStatusEnergyModifier(NetworkStatus ns) {
		switch (ns) {
		case NETWORK_MEDIUM: {
			return ENERGY_MODIFIER_NETWORK_BAD_SECOND;
		}
		case NETWORK_BAD: {
			return ENERGY_MODIFIER_NETWORK_MEDIUM_SECOND;
		}
		default:
			return ENERGY_MODIFIER_NETWORK_OK_SECOND;
		}
	}

	@Override
	public void run() {
		IoTVarStatus ivs = IoTVarStatus.getInstance();

		Boolean isInternetConnected = ivs.isInternetConnected();
		Boolean isFiwareConnected = ivs.canReachServer(IoTVarStatus.getInstance().url);

		Float pingInternet = ivs.averagePingTimeInternet();
		Float pingFiware = ivs.averagePingTimeServer(IoTVarStatus.getInstance().url);

		NetworkStatus nsi = ivs.getNetworkStatusBasedOnPing(pingInternet);
		NetworkStatus nsf = ivs.getNetworkStatusBasedOnPing(pingFiware);

		NetworkInterfaceConnection nic = ivs.getNetworkInterface();

		ivs.setIsInternetConnected(isInternetConnected);
		ivs.setIsFiwareConnected(isFiwareConnected);
		ivs.setNetworkInterfaceConnection(nic);
		ivs.setNetworkStatusInternet(nsi);
		ivs.setNetworkStatusFiware(nsi);
		ivs.setPingInternet(pingInternet);
		ivs.setPingFiware(pingFiware);

//		System.out.println("Is connected to the internet (reach google): " + isInternetConnected);
//		System.out.println("Current ping with google: " + pingInternet);
//		System.out.println("Network status internet: " + nsi);
//		
//		System.out.println("Is connected to the fiware: " + isFiwareConnected);
//		System.out.println("Current ping with fiware: " + pingFiware);
//		System.out.println("Network status fiware: " + nsf);
//		
//		System.out.println("Network interface in use: " + nic);

	}

	/**
	 * Simple main method to debug and test the class in development
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		IoTVarStatus ivs = IoTVarStatus.getInstance();
		Boolean isInternetConnected = ivs.isInternetConnected();
		Boolean isFiwareConnected = ivs.canReachServer(IoTVarStatus.getInstance().url);

		Float pingInternet = ivs.averagePingTimeInternet();
		Float pingFiware = ivs.averagePingTimeServer(IoTVarStatus.getInstance().url);

		NetworkStatus nsi = ivs.getNetworkStatusBasedOnPing(pingInternet);
		NetworkStatus nsf = IoTVarStatus.getInstance().getNetworkStatusBasedOnPing(pingFiware);

		NetworkInterfaceConnection nic = ivs.getNetworkInterface();

		ivs.setIsInternetConnected(isInternetConnected);
		ivs.setIsFiwareConnected(isFiwareConnected);
		ivs.setNetworkInterfaceConnection(nic);
		ivs.setNetworkStatusInternet(nsi);
		ivs.setNetworkStatusFiware(nsi);
		ivs.setPingInternet(pingInternet);
		ivs.setPingFiware(pingFiware);

		System.out.println("Is connected to the internet (reach google): " + isInternetConnected);
		System.out.println("Current ping with google: " + pingInternet);
		System.out.println("Network status internet: " + nsi);

		System.out.println("Is connected to the fiware: " + isFiwareConnected);
		System.out.println("Current ping with google: " + pingFiware);
		System.out.println("Network status fiware: " + nsf);

		System.out.println("Network interface in use: " + nic);
	}

	public Float getCalculatedEnergyUsage() {
		return calculatedEnergyUsage;
	}

	public void setCalculatedEnergyUsage(Float calculatedEnergyUsage) {
		this.calculatedEnergyUsage = calculatedEnergyUsage;
	}

	private String getUrl() {
		return url;
	}

	private void setUrl(String url) {
		this.url = url;
	}

	public Boolean getIsInternetConnected() {
		return isInternetConnected;
	}

	public void setIsInternetConnected(Boolean isInternetConnected) {
		this.isInternetConnected = isInternetConnected;
	}

	public Boolean getIsFiwareConnected() {
		return isFiwareConnected;
	}

	public void setIsFiwareConnected(Boolean isFiwareConnected) {
		this.isFiwareConnected = isFiwareConnected;
	}

	public Float getPingFiware() {
		return pingFiware;
	}

	public void setPingFiware(Float pingFiware) {
		this.pingFiware = pingFiware;
	}

	public Float getPingInternet() {
		return pingInternet;
	}

	public void setPingInternet(Float pingInternet) {
		this.pingInternet = pingInternet;
	}

	public NetworkStatus getNetworkStatusInternet() {
		return networkStatusInternet;
	}

	public void setNetworkStatusInternet(NetworkStatus networkStatusInternet) {
		this.networkStatusInternet = networkStatusInternet;
	}

	public NetworkStatus getNetworkStatusFiware() {
		return networkStatusFiware;
	}

	public void setNetworkStatusFiware(NetworkStatus networkStatusFiware) {
		this.networkStatusFiware = networkStatusFiware;
	}

	public NetworkInterfaceConnection getNetworkInterfaceConnection() {
		return networkInterfaceConnection;
	}

	public void setNetworkInterfaceConnection(NetworkInterfaceConnection networkInterfaceConnection) {
		this.networkInterfaceConnection = networkInterfaceConnection;
	}

}
