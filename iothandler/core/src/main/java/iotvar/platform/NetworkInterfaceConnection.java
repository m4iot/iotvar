package iotvar.platform;

public enum NetworkInterfaceConnection {
	WIFI, ETHERNET, UNKNOWN
}
