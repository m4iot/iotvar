/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.mudebs;

import iotvar.IoTVariableMuDEBS;
import iotvar.basics.Observation;
import mucontext.api.ContextConsumer;
import iotvar.mucontext.datamodel.context.ContextDataModelFacade;
import iotvar.mucontext.datamodel.context.ContextObservation;
import iotvar.mucontext.datamodel.context.ContextReport;

/**
 * Implements a ContextConsumer which update the IoTvar associated in a thread.
 *
 */

public class ContextMuDEBSConsumer implements ContextConsumer{

	private IoTVariableMuDEBS myIoTvar;
    private ContextDataModelFacade facade;

    /**
     * constructor of a consumer
     * @param iotvar which be updated by the consumer
     * @param facade2 of the application
     */
	public ContextMuDEBSConsumer (IoTVariableMuDEBS iotvar, ContextDataModelFacade facade){
		 this.myIoTvar = iotvar;
		 this.facade = facade;
	 }

	public void consume(String contextData) {

		ContextReport report = facade.unserialiseFromXML(contextData);
        System.out.println("Example context application consumes report = "
                + report);

        for (ContextObservation o :report.observations) {
            /* At the moment o.value support only double */
            for(Object q : o.list_qoCIndicator){
                System.out.println(q);
            }
            try {
                double doubleValue = Double.parseDouble(o.value.toString());
                Observation<Double> ob = new Observation(doubleValue, 0, 0);
                assert(myIoTvar != null);
                myIoTvar.updateObservation(ob);

            } catch (NumberFormatException e)  {
                System.out.println("Failed to cast :" + o.value + " to double");
            }
        }
    }
}
