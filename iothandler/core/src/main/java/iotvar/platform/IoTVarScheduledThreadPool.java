/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import iotvar.utils.IoTVarThreadPool;

public class IoTVarScheduledThreadPool {

    /**
     * Thread pool executor used with synchronous call pattern. It is scheduled by the freshness
     * of the IoT variable launched.
     */
    private static ScheduledThreadPoolExecutor services;
    
    private static IoTVarThreadPool pool;

    /**
     * Size of the ScheduledThreadPoolExecutor used by IoTVar for the synchronous call pattern
     */
    private static int poolsize = 5;

    /**
     * Singleton pattern for the ScheduledThreadPoolExecutor services
     */
    public IoTVarScheduledThreadPool() {
        if(services == null){
            services = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
            services.setRemoveOnCancelPolicy(true);
         }
    }
    
    /**
     * Singleton pattern for the ScheduledThreadPoolExecutor services
     */
    public IoTVarScheduledThreadPool(int poolsize) {
        if(services == null){
            services = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(poolsize);
            services.setRemoveOnCancelPolicy(true);
         }
    }

    public ScheduledFuture<?> scheduleAtFixedRate(Runnable var1, long var2, long var4, TimeUnit var6){
        return services.scheduleAtFixedRate(var1, var2, var4, var6);
    }
    
    public boolean stopRunnable(Runnable r) {
    	return services.remove(r);
    }
}
