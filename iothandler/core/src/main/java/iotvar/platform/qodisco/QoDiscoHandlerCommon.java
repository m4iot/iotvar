/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.qodisco;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import iotvar.IoTVariableCommon;
import iotvar.basics.SearchCriterion;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;

import java.lang.ref.WeakReference;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

abstract class QoDiscoHandlerCommon {

    protected final Cache<String, HttpResponse> cache = CacheBuilder.newBuilder()
            .maximumSize(10000)
            .expireAfterWrite(30, TimeUnit.SECONDS)
            .build();


    private static StringBuilder sbPrefixes;
    static {
        sbPrefixes = new StringBuilder();
        sbPrefixes.append("PREFIX geom: <http://pervasive.semanticweb.org/ont/2004/06/geo-measurement#>"
                +" PREFIX geos: <http://pervasive.semanticweb.org/ont/2004/06/space#>"
                +" PREFIX ssn: <http://purl.oclc.org/NET/ssnx/ssn#>"
                +" PREFIX rdf: <http://www.w3.org/2000/01/rdf-schema#>"
                +" PREFIX dul: <http://www.loa-cnr.it/ontologies/DUL.owl#>"
                +" PREFIX qodisco: <http://consiste.dimap.ufrn.br/ontologies/qodisco#>"
                +" PREFIX qoc: <http://consiste.dimap.ufrn.br/ontologies/qodisco/context#>"
                +" PREFIX uomvocab: <http://purl.oclc.org/NET/muo/muo#>"
                +" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
                +" PREFIX service: <http://www.daml.org/services/owl-s/1.1/Service.owl#>"
                +" PREFIX profile: <http://www.daml.org/services/owl-s/1.1/Profile.owl#>"
                +" PREFIX process: <http://www.daml.org/services/owl-s/1.1/Process.owl#>"
                +" PREFIX weather: <http://consiste.dimap.ufrn.br/ontologies/qodisco/weather#>");
    }
    static final String prefixes = sbPrefixes.toString();

    StringBuilder query;
    HttpGet getMethod;
    HttpClient httpClient;
    String filters;

    protected String qodiscoAddress;
    protected String qodiscoUsername;
    protected String qodiscoPassword;

    protected WeakReference<? extends IoTVariableCommon> iotvar;


    QoDiscoHandlerCommon(WeakReference<? extends IoTVariableCommon> iotvar, String qodiscoAddress, String qodiscoUsername, String qodiscoPassword, SearchCriterion searchCriterion) {
        this.iotvar = iotvar;
        this.qodiscoAddress = qodiscoAddress;
        this.qodiscoUsername = qodiscoUsername;
        this.qodiscoPassword = qodiscoPassword;
        this.query = buildQuery(searchCriterion);
    }

    /**
     * Builder for the Http Client to connect to QoDisco.
     * @return httpCllient
     */
    protected HttpClient buildHttpClient(){
        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(qodiscoUsername, qodiscoPassword);
        provider.setCredentials(AuthScope.ANY, credentials);

        return HttpClientBuilder.create()
                .setDefaultCredentialsProvider(provider)
                .build();
    }

    /**
     * Builder for the get method to send to QoDisco.
     * @return getMethod
     */
    protected HttpGet buildGetMethod(String path){
        URIBuilder uri = new URIBuilder();
        uri.setScheme("http").setHost(this.qodiscoAddress).setPath(path)
                .setParameter("domain", "Default");
        HttpGet getMethod = null;
        try {
            getMethod = new HttpGet(uri.build());
            getMethod.addHeader("accept", "application/json");
            getMethod.addHeader("query", this.query.toString());
            if(this.filters != null) {
                getMethod.addHeader("filter", this.filters);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return getMethod;
    }

    StringBuilder buildQuery(SearchCriterion searchCriterion) {
        double latitude = searchCriterion.getLocation().getLatitude();
        double longitude = searchCriterion.getLocation().getLongitude();
        double r = searchCriterion.getLocation().getRadius() / 6371;
        double dt = Math.toDegrees(Math.asin(r)) / Math.cos(Math.toRadians(latitude));

        StringBuilder query = new StringBuilder();
        query.append(prefixes);
        query.append(" SELECT ?observation ?date ?latitude ?longitude ?value WHERE {")
                .append(" ?sensor a ssn:SensingDevice ;")
                .append(" dul:hasLocation ?location .")
                .append(" ?location geos:hasCoordinates ?coordinates .")
                .append(" ?coordinates geom:latitude ?latitude ;")
                .append(" geom:longitude ?longitude .")
                .append(" ?observation a ssn:Observation ;")
                .append(" ssn:observationResultTime ?date ;")
                .append(" ssn:observedBy ?sensor ;")
                .append(" ssn:observedProperty weather:")
                .append(searchCriterion.getType()).append(" ;")
                .append(" ssn:observationResult ?observationResult .")
                .append(" ?observationResult a ssn:SensorOutput ;");
        //+" qodisco:has_qoc ?qocIndicator ;"
        query.append(" ssn:hasValue ?observationValue .");
        //+" ?qocIndicator a qodisco:QoCIndicator ;"
        //+" qodisco:has_qoc_criterion ?criterion ;"
        //+" qodisco:has_qoc_value ?qocValue ."
        query.append(" ?observationValue a ssn:ObservationValue ;")
                .append(" ssn:hasQuantityValue ?value .")
                .append(" FILTER (?latitude >= ")
                .append(String.valueOf(latitude - Math.toDegrees(r)))
                .append(" && ?latitude <= ")
                .append(String.valueOf(latitude + Math.toDegrees(r)))
                .append(" && ?longitude >= ").append(String.valueOf(longitude - dt)).append(" && ?longitude <= ")
                .append(String.valueOf(longitude + dt)).append(")").append(" }")
                .append("ORDER BY desc(?date)").append("LIMIT 1");
        return query;
    }
}
