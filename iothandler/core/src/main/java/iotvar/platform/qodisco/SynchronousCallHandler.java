/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.qodisco;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import iotvar.IoTVariableCommon;
import iotvar.IoTVariableQoDisco;
import iotvar.basics.Observation;
import iotvar.basics.SearchCriterion;
import iotvar.exception.IoTVarNotFoundException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;


/**
 * Handler for the synchronous call pattern with QoDisco.
 * @author Rui
 *
 */
public class SynchronousCallHandler extends QoDiscoHandlerCommon implements Runnable {

	protected IoTVariableQoDisco iotvar;

	final static Cache<String, HttpResponse> cache = CacheBuilder.newBuilder()
		    .maximumSize(10000)
		    .expireAfterWrite(30, TimeUnit.SECONDS)
		    .build();

	private static final Logger logger = LogManager.getLogger(SynchronousCallHandler.class);

	/**
	 * Counter of the problem raised by the handler. If it's up to 4, the Handler stop his process and cancel the Future.
	 */
	protected int count = 0;
	private HttpGet getMethod;

	public SynchronousCallHandler(IoTVariableQoDisco iotvar, String qodiscoAddress, String qodiscoUsername, String qodiscoPassword, SearchCriterion searchCriterion) {
	    super(new WeakReference<IoTVariableCommon>(iotvar), qodiscoAddress, qodiscoUsername, qodiscoPassword, searchCriterion);
		this.iotvar = iotvar;
        this.getMethod = buildGetMethod("/sync-search");
	}

	// TODO : cleanup and partially merge with PublishSubscribeHandler
	public void run() {
		try {
			logger.info("Synchronous request");


			String output;
			StringBuilder json = new StringBuilder();
			logger.info("String builder");

			HttpClient httpClient = buildHttpClient();
			logger.info("http build");
			HttpResponse response = cache.getIfPresent(this.query);
			if (response == null) {

				logger.info("Execute http");
				response = httpClient.execute(this.getMethod);
				logger.info("HTTP response");

				cache.put(this.query.toString(), response);

			}
			logger.info("reponse in cache");

			if(response.getStatusLine().getStatusCode() != 200) {
				logger.error("Response error " + response.getStatusLine());
			}

			BufferedReader br = new BufferedReader(
	                new InputStreamReader((response.getEntity().getContent())));

			logger.info("[Response:] .... \n");


			while ((output=br.readLine())!=null) {
				json.append(output);

			}
			logger.info(json);


			httpClient.getConnectionManager().shutdown();

			if (json == null || json.equals("") || json.equals("null")) {
				iotvar.updateObservation(null);

			}

			JSONObject obj = null;
			obj = new JSONObject(json.toString());
            JSONArray result = obj.getJSONObject("results").getJSONArray("bindings");

            int length = result.length();

            if (result != null) {
            	double value = 0, latitude = 0, longitude = 0;

            	for (int i = 0; i < length; i++) {
            		JSONObject var = result.getJSONObject(i);
	            	value = var.optJSONObject("value").getDouble("value");
	            	latitude = var.optJSONObject("latitude").getDouble("value");
	            	longitude = var.optJSONObject("longitude").getDouble("value");
            	}
            	logger.info("Longitude : " + longitude);
            	logger.info("Latitude : " + latitude);
            	logger.info(iotvar.getSearchCriterion().getType() + " : " + value);
            	Observation<Double> obs = new Observation(value, longitude, latitude);

            	//On ne fait une nouvelle observation que si la valeur a changé
            	if (!iotvar.getLastObservation().equals(obs)) {
					iotvar.updateObservation(obs);

            	}
            }
            else
				iotvar.updateObservation(null);

		} catch(IoTVarNotFoundException e) {
			logger.error("Search problem!",e);
		} catch (IOException e) {
			logger.error("I/O problem!",e);
			System.exit(-1);

		}

	}

}
