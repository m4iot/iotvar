/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.platform.qodisco;

import iotvar.IoTVariableQoDisco;
import iotvar.basics.FilterPublishSubscribeMethod;
import iotvar.basics.SearchCriterion;
import iotvar.exception.IoTVarNotFoundException;
import iotvar.platform.IoTVarMqttClient;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Handler for the Publsh-Subscribe pattern for Qodisco. It initializes connexion with QoDisco, send the
 * http get request and subscribe to the topic gave in the response by QoDisco
 * .
 * @author Clement Courtais
 * @author Houssem TOUANSI
 *
 */
public class PublishSubscribeHandler<T> extends QoDiscoHandlerCommon {

	private static final Logger logger = LogManager.getLogger(PublishSubscribeHandler.class);

	/**
	 * Variables to connect to Qodisco
	 */
	private IoTVarMqttClient mqttClient;
    private HttpGet getMethod;
 
	public PublishSubscribeHandler(WeakReference<IoTVariableQoDisco<T>> iotvar, String qodiscoAddress, String qodiscoUsername, String qodiscoPassword, SearchCriterion searchCriterion, List<FilterPublishSubscribeMethod> filters) {
        super(iotvar, qodiscoAddress, qodiscoUsername, qodiscoPassword,  searchCriterion);
        if(filters != null) {
            this.filters = buildFilterXml(filters);
        }
        this.query = buildQuery(searchCriterion);
        this.getMethod = this.buildGetMethod("/async-search-filter");
     }

    public PublishSubscribeHandler(WeakReference<IoTVariableQoDisco<T>> iotvar, String qodiscoAddress, String qodiscoUsername, String qodiscoPassword, SearchCriterion searchCriterion) {
    	super(iotvar, qodiscoAddress, qodiscoUsername, qodiscoPassword, searchCriterion);
    	this.query = buildQuery(searchCriterion);
        this.getMethod = this.buildGetMethod("/async-search");
 
    }

    // TODO : cleanup and partially merge with SynchronousCallHandler
	public void run() {
		try {
			HttpResponse httpResponse = cache.getIfPresent(query);
						
			if (httpResponse == null) {
                this.httpClient = buildHttpClient();
				httpResponse = httpClient.execute(getMethod);
				cache.put(query.toString(), httpResponse);
			}
						
			if(httpResponse.getStatusLine().getStatusCode() != 200) {
				logger.error("Response error " + httpResponse.getStatusLine());
			}
			
			logger.info("[Response:] .... \n");
			HttpEntity responseEntity = httpResponse.getEntity();
			String response = EntityUtils.toString(responseEntity);
			logger.info(response);
			
			JSONObject json = new JSONObject(response);
			logger.info(json);
			String topicName = json.getString("topic");
			String brokerAddress = "tcp://"+qodiscoAddress.substring(0, qodiscoAddress.indexOf(':'))+":1883";
			mqttClient = new IoTVarMqttClient(brokerAddress);
			mqttClient.subscribe(topicName, iotvar.get());

			httpClient.getConnectionManager().shutdown();

			logger.info("Connected to broker and subscribe to topic : "+topicName);
            
		} catch(IoTVarNotFoundException e) {
			logger.error("Search problem!",e);
		} catch (IOException e) {
			logger.error("I/O problem!",e);
			System.exit(-1);
		}
	}

    private String buildFilterXml(List<FilterPublishSubscribeMethod> filters){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(FilterPublishSubscribeMethod f: filters){
            sb.append("{\"name\":\"");
            sb.append(f.getName());
            sb.append("\",\"value\":\"");
            sb.append(f.getValue());
            sb.append("\"}");
        }
        sb.append("]");
        return sb.toString();
    }
}
