/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar;

import iotvar.basics.FilterPublishSubscribeMethod;
import iotvar.basics.Freshness;
import iotvar.basics.HandlerStrategy;
import iotvar.basics.Location;
import iotvar.mucontext.datamodel.context.ContextDataModelFacade;
import iotvar.platform.IoTVarContextManager;
import mudebs.common.MuDEBSException;

import java.lang.ref.WeakReference;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * This class implements the IoT Variable and methods related to search criterion
 * and observations.
 *
 * @author Rui
 */
public class IoTVariableMuDEBS<T> extends IoTVariableCommon<T> {

	private static ContextDataModelFacade facade = new ContextDataModelFacade("facade5");
	private static IoTVarContextManager contextManager;

	private String subFilter;

    static final Logger logger = LogManager.getLogger(IoTVariableMuDEBS.class);

	/**
	 * Constructor of an IoT variable. This constructor implements an IoT variable, create its Observation list and
	 * register it to the IoTVarMiddleware. This method also instantiate the IoTVarMiddleware with a Singleton pattern
	 * @param type The type of data observed by the IoT variable
	 * @param location The Location where the data will be observed
	 * @param freshness The freshness of the variable
	 * @param historySize The size of the historic of observation (10 if not precised)
	 * @param filters The filters used by the variable if the pattern is contentpubsub (null if not precised)
	 */
	public IoTVariableMuDEBS(String type, Location location, Freshness freshness, int historySize, List<FilterPublishSubscribeMethod> filters, String[] basicApplicationArgs, String subFilter) throws MuDEBSException {
		super(type, location, freshness, historySize, filters);
		//this.contextApplication = new BasicContextApplication(basicApplicationArgs);
		this.pattern = HandlerStrategy.PUBSUB;
		this.subFilter = subFilter;
		WeakReference<IoTVariableMuDEBS> iotvar = new WeakReference<IoTVariableMuDEBS>(this);
		IoTVariableCommon.iotVariables.add(iotvar);
		if (contextManager == null) {
			contextManager = new IoTVarContextManager(facade, basicApplicationArgs);
		}
		this.registerIoTVar(iotvar);
		logger.info("IoTVariable for " + this.searchCriterion.getType() + " created");
	}

	private void registerIoTVar(WeakReference<IoTVariableMuDEBS> iotvar) {
		logger.info("Launching Publish-Subscribe pattern for " + iotvar.get().getSearchCriterion().getType());
		try {
			contextManager.subscribe(iotvar, subFilter);
		} catch (MuDEBSException e) {
			e.printStackTrace();
		}
	}

}
