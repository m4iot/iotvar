/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar;

import iotvar.basics.FilterPublishSubscribeMethod;
import iotvar.basics.Freshness;
import iotvar.basics.HandlerStrategy;
import iotvar.basics.Location;
import iotvar.platform.IoTVarScheduledThreadPool;
import iotvar.platform.qodisco.PublishSubscribeHandler;
import iotvar.platform.qodisco.SynchronousCallHandler;
import iotvar.utils.FuturesMapRefactor;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.Future;

public class IoTVariableQoDisco<T> extends IoTVariableCommon<T> {

    private static final Logger logger = LogManager.getLogger(IoTVariableQoDisco.class);
    private String qodiscoAddress;
    private String qodiscoUsername;
    private String qodiscoPassword;

    IoTVarScheduledThreadPool pool;

    public IoTVariableQoDisco(String type, String qodiscoAddress, String qodiscoUsername, String qodiscoPassword, Location location, Freshness freshness, int historySize, List<FilterPublishSubscribeMethod> filters, HandlerStrategy handler) {
        super(type, location, freshness, historySize, filters);
        this.qodiscoAddress = qodiscoAddress;
        this.qodiscoUsername = qodiscoUsername;
        this.qodiscoPassword = qodiscoPassword;
        pool = new IoTVarScheduledThreadPool();

        this.pattern = handler;
        WeakReference<IoTVariableQoDisco<T>> iotvar = new WeakReference<IoTVariableQoDisco<T>>(this);
        this.registerIoTVar(iotvar);
    }

    /** Register an IoTVar and execute the corresponding method (synchronous call, publish-subscribe or
     *  content-based publish-subscribe).
     *
     */
    public void registerIoTVar(WeakReference<IoTVariableQoDisco<T>> iotvar) {
        Freshness freshness = iotvar.get().getSearchCriterion().getFreshness();
        // In case of testing performance we can
        // set 1 second delay for each handler
        // in order to prevent fuseki from receiving
        // too many queries simultanously
        Future<?> future = null;
        switch(iotvar.get().getpattern()) {
            case SYNC :
                logger.info("Launching Synchronous call pattern for " + searchCriterion.getType());
                future = pool.scheduleAtFixedRate( new SynchronousCallHandler(this, qodiscoAddress, qodiscoUsername, qodiscoPassword, searchCriterion),
                        0, freshness.getValue(), freshness.getTimeunit());
                // Register return of each handler according to IoT Variable
                // so that each thread can deal with exception
                // after checking state of its own future
                FuturesMapRefactor.getFutures().put(iotvar,future);
                break;
            case PUBSUB :
                logger.info("Launching Publish-Subscribe pattern for " + searchCriterion.getType());
                new PublishSubscribeHandler<T>(iotvar, qodiscoAddress, qodiscoUsername, qodiscoPassword, searchCriterion).run();
                logger.info("End process PubSubHandler");
                break;
            case CONTENTPUBSUB :
                logger.info("Launching Content-based Publish-Subscribe pattern for " + searchCriterion.getType());
                new PublishSubscribeHandler<T>(iotvar, qodiscoAddress, qodiscoUsername, qodiscoPassword, searchCriterion, filters).run();
                logger.info("End process Content-based PubSubHandler");

                break;
        }

    }

    /**
     * This method is called by the MqttClient to unsubscribe all the IoTVar from the QoDisco database.
     */
    public void unsubscribe(String topicName,  WeakReference<IoTVariableQoDisco<T>> iotvar) {
        try {
            URIBuilder uri = new URIBuilder();
            uri.setScheme("http").setHost(qodiscoAddress).setPath("/async-search")
                    .setParameter("topic", topicName);

            HttpDelete httpDelete = new HttpDelete(uri.build());

            DefaultHttpClient httpClient = new DefaultHttpClient();
            Credentials credentials = new UsernamePasswordCredentials(qodiscoUsername, qodiscoPassword);
            httpClient.getCredentialsProvider().setCredentials(AuthScope.ANY, credentials);

            HttpResponse response = httpClient.execute(httpDelete);

            if(response.getStatusLine().getStatusCode() != 200 ) {
                logger.error("Response error " + response.getStatusLine());
            }

            httpClient.getConnectionManager().shutdown();
        } catch (URISyntaxException e) {
            logger.error("URI syntax error " + e);

        } catch (ClientProtocolException e) {
            logger.error("Client protocol error " + e);

        } catch (IOException e) {
            logger.error("IOException error " + e);

        }
    }

    /**
     * This method clear the QoDisco database. It is called when executing the test.
     */
    public void clearDatabase(){
        StringBuilder sb = new StringBuilder();
        sb.append("PREFIX ssn: <http://purl.oclc.org/NET/ssnx/ssn#>"
                +"DELETE WHERE { ?o a ssn:Observation. }");
        DefaultHttpClient httpClient = new DefaultHttpClient();
        URIBuilder uri = new URIBuilder();
        uri.setScheme("http").setHost("157.159.110.60:3030/dataset").setPath("/update");

        HttpPost postMethod;
        try {
            postMethod = new HttpPost(uri.build());
            postMethod.addHeader("Content-Type", "application/sparql-update");
            //postMethod.addHeader("query", query);


            HttpEntity entity = new ByteArrayEntity(sb.toString().getBytes("UTF-8"));
            postMethod.setEntity(entity);

            httpClient.execute(postMethod);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }

    }
}
