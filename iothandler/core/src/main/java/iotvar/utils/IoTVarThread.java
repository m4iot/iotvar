package iotvar.utils;

import java.util.concurrent.TimeUnit;

public class IoTVarThread implements Runnable {
	
	public IoTVarThread() {
		
	}

	@Override
	public void run() {

		int total = 0;

		for (int i = 0; i < 5; i++) {
			total += 1;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("Total: " + total);
	}

	public static void main(String[] args) {
		IoTVarThreadPool pool = new IoTVarThreadPool(Runtime.getRuntime().availableProcessors());
		try {
			pool.executeAtFixedRate(new IoTVarThread(), 0, 1, TimeUnit.SECONDS);
			pool.executeAtFixedRate(new IoTVarThread(), 0, 1, TimeUnit.SECONDS);
			pool.executeAtFixedRate(new IoTVarThread(), 0, 1, TimeUnit.SECONDS);
			pool.executeAtFixedRate(new IoTVarThread(), 0, 1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
