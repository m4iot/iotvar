package iotvar.utils;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class IoTVarThreadPool {
	
    private final int nThreads;
    private final IoTVarPoolWorker[] threads;
    private final LinkedBlockingQueue<Runnable> queue;
 
    public IoTVarThreadPool(int nThreads) {
        this.nThreads = nThreads;
        queue = new LinkedBlockingQueue<Runnable>();
        threads = new IoTVarPoolWorker[nThreads];
 
        for (int i = 0; i < nThreads; i++) {
            threads[i] = new IoTVarPoolWorker();
            threads[i].start();
        }
    }
 
    public void executeAtFixedRate(Runnable task, int delay, int period, TimeUnit unit) throws InterruptedException {
    	//Thread.sleep(unit.toMillis(delay));
        synchronized (queue) {
            queue.add(task);
            queue.notify();
        }
    }
 
    public int getnThreads() {
		return nThreads;
	}

	private class IoTVarPoolWorker extends Thread {
        public void run() {
            Runnable task;
 
            while (true) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            System.out.println("An error occurred while queue is waiting: " + e.getMessage());
                        }
                    }
                    task = queue.poll();
                }
 
                // If we don't catch RuntimeException,
                // the pool could leak threads
                try {
                    task.run();
                } catch (RuntimeException e) {
                    System.out.println("Thread pool is interrupted due to an issue: " + e.getMessage());
                } finally {
                	
                }
            }
        }
    }
}
