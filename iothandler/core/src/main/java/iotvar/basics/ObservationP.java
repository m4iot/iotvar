/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
/**
 * 
 */
package iotvar.basics;

import java.util.Date;

/**
 * This class implements the Observation object. Observations are linked to IoT variables and store the value, the date 
 * and the location where the observation is made.
 * 
 * @author Rui
 *
 */
public class ObservationP<type> {
	
	private String id;
	private type value;

	/**
	 * Reception date of the observation by the handler
	 */
	private Date date;

	private double longitude;
	private double latitude;
	
	public ObservationP(type value, double longitude, double latitude) {
		this.value = value;
		this.date = new Date();
		this.longitude = longitude;
		this.latitude = latitude;
	}
	
	public ObservationP(type value, double longitude, double latitude, String id) {
		this.value = value;
		this.date = new Date();
		this.longitude = longitude;
		this.latitude = latitude;
		this.id = id;
	}
	/**
	 * @return the value
	 */
	public type getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(type value) {
		this.value = value;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}
	
	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}
	
	@Override
	public String toString() {
		return " [Date: " + date + "]" +
				" [Value: " + value + "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
