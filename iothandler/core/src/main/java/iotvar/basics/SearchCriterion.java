/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
/**
 * 
 */
package iotvar.basics;

/**
 * The SearchCriterion class represents criterion 
 * (or criteria) to be searched 
 * @author Rui
 *
 */
public class SearchCriterion {

	/** Type of variable to be observed */
	private String type;
	private Location location;	
	/** Maximum time between 2 updates (millisecond) */
	private Freshness freshness; 
	
	
	public SearchCriterion(String type, Location location, Freshness freshness) {
		this.type = type;
		this.location = location;
		this.freshness = freshness;
	}
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(Location location) {
		this.location = location;
	}
	
	/**
	 * @return the freshness
	 */
	public Freshness getFreshness() {
		return freshness;
	}

	/**
	 * @param freshness the freshness to set
	 */
	public void setFreshness(Freshness freshness) {
		this.freshness = freshness;
	}

	@Override
	public String toString() {
		return "Type [ " + type + " ]" + "\n"
				+ "Location [ " + location.toString() + " ]" + "\n"
				+ "Freshness [ " + String.valueOf(freshness) + " ]";
	}
	
}
