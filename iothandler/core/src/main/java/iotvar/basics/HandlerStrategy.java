/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.basics;

/**
 * Enumeration of the different strategy allowed by IoTVar : synchronous,
 * asynchronous and asynchronous with filter.
 * The synchronous call pattern communicates with a REST protocol. You can defines the freshness of the 
 * synchronous research and it gives you the last observation corresponding to your request.
 * The publish-subscribe pattern communicates with the Mqtt protocol. It sends you a Mqtt message each time
 * a variable registers a change corresponding to your request.
 * The content-based publish-subscribe pattern communicates with the Mqtt protocol too. It has the same 
 * functionality than the normal publish-subscribe pattern but you can add a filter above the framework you
 * are using (for example it notifies you only if a variable has a change of 10% regarding of its last
 * value).
 * 
 * @author Clement Courtais
 *
 */
public enum HandlerStrategy {
	SYNC,
	PUBSUB,
	CONTENTPUBSUB,
	SYNC_GREEN,
	ASYNC_GREEN
}
