/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar.basics;

import java.util.concurrent.TimeUnit;

/**
 * Freshness class represents time period between every two updates
 * In this project freshness is considered to be the only Qoc criterion
 * @author dylan
 *
 */
public class Freshness {
	
	private long value;
	private TimeUnit timeunit;

	public Freshness(long value, TimeUnit timeunit) {
		this.value = value;
		this.timeunit = timeunit;
	}

	/**
	 * @return the value
	 */
	public long getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(long value) {
		this.value = value;
	}
	/**
	 * @return the timeunit
	 */
	public TimeUnit getTimeunit() {
		return timeunit;
	}
	
	@Override
	public String toString() {
		return " refrsh every " + value + timeunit.toString();
	}
	
	@Override
    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
 
        Freshness frechness = (Freshness) o;
 
        if (value != frechness.value) {
            return false;
        }
        if (!timeunit.equals(frechness.timeunit)) {
            return false;
        }
 
        return true;
    }
	
	@Override
    public int hashCode()
    {
        int result = Long.hashCode(value);
        result = 31 * result + timeunit.hashCode();
        return result;
    }
	
}
