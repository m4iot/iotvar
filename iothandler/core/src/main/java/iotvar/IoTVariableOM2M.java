/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package iotvar;

import iotvar.basics.FilterPublishSubscribeMethod;
import iotvar.basics.Freshness;
import iotvar.basics.HandlerStrategy;
import iotvar.basics.Location;
import iotvar.platform.IoTVarScheduledThreadPool;
import iotvar.platform.om2m.PublishSubscribeHandlerOM2M;
import iotvar.platform.om2m.SynchronousCallHandlerOM2M;
import iotvar.platform.qodisco.PublishSubscribeHandler;
import iotvar.platform.qodisco.SynchronousCallHandler;
import iotvar.utils.FuturesMapRefactor;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class IoTVariableOM2M<T> extends IoTVariableCommon<T> {

    private IoTVarScheduledThreadPool pool;
    static final Logger logger = LogManager.getLogger(IoTVariableOM2M.class);

    private String om2mAddress;

    public IoTVariableOM2M(String type, Location location, Freshness freshness, int historySize, List<FilterPublishSubscribeMethod> filters, String om2mAddress , HandlerStrategy handler) {
        super(type, location, freshness, historySize, filters);
        //this.pattern = HandlerStrategy.SYNC;
        this.pattern = handler;
        this.om2mAddress = om2mAddress;
        WeakReference<IoTVariableOM2M<T>> iotvar = new WeakReference<IoTVariableOM2M<T>>(this);
        pool = new IoTVarScheduledThreadPool();
        this.registerIoTVar(iotvar);
        logger.info("IoTVariable for " + this.searchCriterion.getType() + " created");
    }
  
    public void registerIoTVar(WeakReference<IoTVariableOM2M<T>> iotvar) {
    	
        switch(iotvar.get().getpattern()) {
        case SYNC :
        	logger.info("Launching Synchronous method for " + searchCriterion.getType());
            SynchronousCallHandlerOM2M handler = new SynchronousCallHandlerOM2M(this, om2mAddress, searchCriterion);
            pool.scheduleAtFixedRate(handler,0, searchCriterion.getFreshness().getValue(), searchCriterion.getFreshness().getTimeunit());
            break;
        case PUBSUB :
            logger.info("Launching Publish-Subscribe pattern for " + searchCriterion.getType());
           /* new PublishSubscribeHandler(iotvar, qodiscoAddress, qodiscoUsername, qodiscoPassword, searchCriterion).run();
            logger.info("End process PubSubHandler");*/
             new PublishSubscribeHandlerOM2M(this,searchCriterion,om2mAddress).run();
            logger.info("End process PubSubHandler");
            break;
		default:
			break;
    
    }
    
    
    
    
    
    }
}
