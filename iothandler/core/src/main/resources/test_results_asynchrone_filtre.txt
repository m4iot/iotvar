----- Test Result 2017/08/31-17:08:07.800 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 1
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3313 milliseconds
Total Threads CPU Time: 93 milliseconds
Power consumption :  + 19 + mW
----- Test Result 2017/08/31-17:19:45.404 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 10
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3675 milliseconds
Total Threads CPU Time: 436 milliseconds
Power consumption :  + 52 + mW
----- Test Result 2017/08/31-17:30:58.179 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 50
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 4401 milliseconds
Total Threads CPU Time: 1588 milliseconds
Power consumption :  + 91 + mW
----- Test Result 2017/08/31-17:42:14.329 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 100
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 5253 milliseconds
Total Threads CPU Time: 2765 milliseconds
Power consumption :  + 140 + mW
----- Test Result 2017/08/31-17:53:29.489 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 150
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 5724 milliseconds
Total Threads CPU Time: 4066 milliseconds
Power consumption :  + 193 + mW
----- Test Result 2017/08/31-18:04:44.406 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 200
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 5903 milliseconds
Total Threads CPU Time: 5430 milliseconds
Power consumption :  + 237 + mW
----- Test Result 2017/09/01-09:07:41.704 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 1
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3248 milliseconds
Total Threads CPU Time: 95 milliseconds
Power consumption :  + 21 + mW
----- Test Result 2017/09/01-09:19:19.262 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 10
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3075 milliseconds
Total Threads CPU Time: 396 milliseconds
Power consumption :  + 57 + mW
----- Test Result 2017/09/01-09:30:32.225 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 50
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3178 milliseconds
Total Threads CPU Time: 1338 milliseconds
Power consumption :  + 78 + mW
----- Test Result 2017/09/01-09:41:48.342 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 100
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3430 milliseconds
Total Threads CPU Time: 2419 milliseconds
Power consumption :  + 130 + mW
----- Test Result 2017/09/01-09:53:02.803 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 150
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 4076 milliseconds
Total Threads CPU Time: 3658 milliseconds
Power consumption :  + 181 + mW
----- Test Result 2017/09/01-10:04:18.056 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 200
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 4526 milliseconds
Total Threads CPU Time: 4927 milliseconds
Power consumption :  + 224 + mW
----- Test Result 2017/09/01-10:15:11.404 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 1
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3339 milliseconds
Total Threads CPU Time: 93 milliseconds
Power consumption :  + 18 + mW
----- Test Result 2017/09/01-10:26:49.387 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 10
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3187 milliseconds
Total Threads CPU Time: 410 milliseconds
Power consumption :  + 57 + mW
----- Test Result 2017/09/01-10:38:02.436 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 50
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3326 milliseconds
Total Threads CPU Time: 1277 milliseconds
Power consumption :  + 74 + mW
----- Test Result 2017/09/01-10:49:17.245 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 100
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3360 milliseconds
Total Threads CPU Time: 2327 milliseconds
Power consumption :  + 125 + mW
----- Test Result 2017/09/01-11:00:33.347 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 150
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3909 milliseconds
Total Threads CPU Time: 3580 milliseconds
Power consumption :  + 179 + mW
----- Test Result 2017/09/01-11:11:48.537 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 200
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 4502 milliseconds
Total Threads CPU Time: 4937 milliseconds
Power consumption :  + 220 + mW
----- Test Result 2017/09/01-11:22:41.487 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 1
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3338 milliseconds
Total Threads CPU Time: 95 milliseconds
Power consumption :  + 21 + mW
----- Test Result 2017/09/01-11:34:19.420 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 10
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3254 milliseconds
Total Threads CPU Time: 388 milliseconds
Power consumption :  + 57 + mW
----- Test Result 2017/09/01-11:45:32.235 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 50
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3317 milliseconds
Total Threads CPU Time: 1276 milliseconds
Power consumption :  + 73 + mW
----- Test Result 2017/09/01-11:56:47.543 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 100
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3358 milliseconds
Total Threads CPU Time: 2363 milliseconds
Power consumption :  + 129 + mW
----- Test Result 2017/09/01-12:08:03.813 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 150
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 4030 milliseconds
Total Threads CPU Time: 3661 milliseconds
Power consumption :  + 185 + mW
----- Test Result 2017/09/01-12:19:18.837 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 200
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 4523 milliseconds
Total Threads CPU Time: 5142 milliseconds
Power consumption :  + 228 + mW
----- Test Result 2017/09/01-12:30:11.534 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 1
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3339 milliseconds
Total Threads CPU Time: 94 milliseconds
Power consumption :  + 19 + mW
----- Test Result 2017/09/01-12:41:49.551 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 10
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3256 milliseconds
Total Threads CPU Time: 395 milliseconds
Power consumption :  + 58 + mW
----- Test Result 2017/09/01-12:53:02.317 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 50
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3316 milliseconds
Total Threads CPU Time: 1308 milliseconds
Power consumption :  + 74 + mW
----- Test Result 2017/09/01-13:04:17.416 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 100
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3388 milliseconds
Total Threads CPU Time: 2459 milliseconds
Power consumption :  + 131 + mW
----- Test Result 2017/09/01-13:15:32.893 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 150
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 3885 milliseconds
Total Threads CPU Time: 3747 milliseconds
Power consumption :  + 183 + mW
----- Test Result 2017/09/01-13:26:48.598 -----
*** Initialization data ***
Execution Time = 10 minutes
IoTVariable number = 200
Pool Thread size = 5
Strategy = asyncf
*** Performance results ***
Mean Deviation of freshness: 4502 milliseconds
Total Threads CPU Time: 4997 milliseconds
Power consumption :  + 222 + mW
