package fiware;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import iotvar.IoTVariableFiware;
import iotvar.basics.Freshness;
import iotvar.basics.HandlerStrategy;
import iotvar.basics.Location;
import iotvar.platform.fiware.Orion;
import iotvar.platform.fiware.OrionFilter;
import iotvar.platform.fiware.OrionHandler;
import iotvar.platform.fiware.OrionSQLOPerator;

/*
 * Fiware testing
 * 
 * To run the tests just make sure you have an Orion running on localhost:1026 and that port 8888 is free (Asyncronous testing)
 */
public class FiwareTest {

	// TODO: Check fiware is reachable. If not, skip the tests
	
	private static final Logger logger = LogManager.getLogger(FiwareTest.class);
	private static final double lat = 48.6223426;
	private static final double lon = 2.4404356;

	OrionHandler<String> oh = new OrionHandler<String>(null);
	ObjectMapper objectMapper = new ObjectMapper();
	FiwareTextDisplayTest<String> display = new FiwareTextDisplayTest<String>();
	
//	@Before
	public void setUp() throws Exception {
		String resp = oh.get("", new HashMap<String, String>());
		JsonNode rootNode = objectMapper.readTree(resp);
		logger.info("Test connection to Orion");
		assertTrue(rootNode.get("entities_url").textValue().equals("/v2/entities"));

		logger.info("Create test entity room1 of type Room");
		oh.post("/entities", this.buildPostJson("sensor1", "Sensor"), new HashMap<String, String>());
		
		logger.info("Continue testing");

	}

//	@After
	public void tearDown() throws Exception {
		logger.info("End testing");
	}

//	@Test
	public void testRoom1Sync() throws InterruptedException {
		logger.info("Testing room1 sync");
		
		IoTVariableFiware<String> var1 = new IoTVariableFiware<String>("sensor1", "Sensor", "temperature",
				new Location("location", FiwareTest.lat, FiwareTest.lon, 100.0), new Freshness(4, TimeUnit.SECONDS), 10,
				(String) null, new Orion(), HandlerStrategy.SYNC);
		var1.registerIoTListenerP(display);
		
		double expectedValue = this.randomValue(20, 10);
		
		this.buildPutJson(expectedValue);
		this.simulateUpdate("sensor1", "Sensor", expectedValue);
		Thread.sleep(2000);
		assertEquals(expectedValue, Double.parseDouble(display.getLastValue()), 0.01);
	}
	
//	@Test
	public void testRoom1Async() throws InterruptedException {
		logger.info("testing room1 async");
		
		IoTVariableFiware<String> var1 = new IoTVariableFiware<String>("sensor1", "Sensor", "temperature",
				new Location("location", FiwareTest.lat, FiwareTest.lon, 100.0), new Freshness(4, TimeUnit.SECONDS), 10,
				(String) null, new Orion(), HandlerStrategy.PUBSUB);
		var1.registerIoTListenerP(display);
		// Wait a little for initial subscription
		Thread.sleep(2000);
		
		double expectedValue = this.randomValue(20, 10);
		
		this.buildPutJson(expectedValue);
		this.simulateUpdate("sensor1", "Sensor", expectedValue);
		Thread.sleep(2000);
		assertEquals(expectedValue, Double.parseDouble(display.getLastValue()), 0.01);
	}
	
//	@Test
	public void testRoom1SyncFilter() throws InterruptedException {
		logger.info("Testing room1 sync");
		
		OrionFilter f1 = new OrionFilter("temperature", "20", OrionSQLOPerator.GREATER);
		OrionFilter f2 = new OrionFilter("humidity", "500", OrionSQLOPerator.LESS_EQUAL);
		ArrayList<OrionFilter> filterList = new ArrayList<OrionFilter>();
		filterList.add(f1);
		filterList.add(f2);
		
		IoTVariableFiware<String> var1 = new IoTVariableFiware<String>("sensor1", "Sensor", "temperature",
				new Location("location", FiwareTest.lat, FiwareTest.lon, 100.0), new Freshness(4, TimeUnit.SECONDS), 10,
				filterList, new Orion(), HandlerStrategy.SYNC);
		var1.registerIoTListenerP(display);
		
		double expectedValue = this.randomValue(20, 10);
		
		this.buildPutJson(expectedValue);
		this.simulateUpdate("sensor1", "Sensor", expectedValue);
		Thread.sleep(2000);
		assertEquals(expectedValue, Double.parseDouble(display.getLastValue()), 0.01);
	}
	
//	@Test
	public void testRoom1AsyncFilter() throws InterruptedException {
		logger.info("testing room1 async");
		
		OrionFilter f1 = new OrionFilter("temperature", "20", OrionSQLOPerator.GREATER);
		OrionFilter f2 = new OrionFilter("humidity", "500", OrionSQLOPerator.LESS_EQUAL);
		ArrayList<OrionFilter> filterList = new ArrayList<OrionFilter>();
		filterList.add(f1);
		filterList.add(f2);
		
		IoTVariableFiware<String> var1 = new IoTVariableFiware<String>("sensor1", "Sensor", "temperature",
				new Location("location", FiwareTest.lat, FiwareTest.lon, 100.0), new Freshness(4, TimeUnit.SECONDS), 10,
				filterList, new Orion(), HandlerStrategy.PUBSUB);
		var1.registerIoTListenerP(display);
		// Wait a little for initial subscription
		Thread.sleep(2000);
		
		double expectedValue = this.randomValue(20, 10);
		
		this.buildPutJson(expectedValue);
		this.simulateUpdate("sensor1", "Sensor", expectedValue);
		Thread.sleep(2000);
		assertEquals(expectedValue, Double.parseDouble(display.getLastValue()), 0.01);
	}

	private String buildPostJson(String id, String type) {
		return String
				.format(Locale.US, "{\n" + "  \"id\": \"%s\",\n" + "  \"type\": \"%s\",\n" + "  \"%s\": {\n"
						+ "    \"value\": %.2f,\n" + "    \"type\": \"Float\"\n" + "  },\n" + "  \"humidity\": {\n"
						+ "  	\"value\": %.2f,\n" + "  	\"type\": \"int\"\n" + "  },\n" + "  \"%s\": {\n"
						+ "  	\"value\": \"%s,%s\",\n" + "  	\"type\": \"geo:point\"\n" + "  }\n" + "}", id, type,
						"temperature", this.randomValue(20, 10), this.randomValue(400, 100), "location", FiwareTest.lat, FiwareTest.lon)
				.replaceAll("\\r|\\n", "");
	}

	private String buildPutJson(double rv) {
		return String.format(Locale.US,
				"{\n" + "    \"%s\": {\n" + "        \"value\": %.2f,\n" + "        \"type\": \"Float\"\n" + "    },\n"
						+ "    \"humidity\": {\n" + "        \"value\": %.2f,\n" + "        \"type\": \"int\"\n"
						+ "    },\n" + "    \"location\": {\n" + "        \"type\": \"geo:point\",\n"
						+ "        \"value\": \"%s,%s\",\n" + "        \"metadata\": {}\n" + "    }\n" + "}",
				"temperature", rv, this.randomValue(400, 100), FiwareTest.lat, FiwareTest.lon)
				.replaceAll("\\r|\\n", "");
	}

	private double randomValue(int val1, int val2) {
		double randomValue = (val1 + val2 * new Random().nextDouble());

		return randomValue;
	}

	public String simulateUpdate(String id, String type, double rv) {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("type", type);
		return oh.put("/entities/" + id + "/attrs", this.buildPutJson(rv), params);
	}

}
