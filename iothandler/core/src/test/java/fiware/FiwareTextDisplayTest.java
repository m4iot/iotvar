package fiware;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import iotvar.IoTVarObserverP;
import iotvar.basics.ObservationP;
import iotvar.exception.IoTVarNotFoundException;

public class FiwareTextDisplayTest<type> implements IoTVarObserverP<type>{
	
	private String lastValue = "";
	 
	public void onUpdate(ObservationP<type> newObservation) {

		if (newObservation == null)
			updateIssue();
		this.lastValue = (String) newObservation.getValue();
	}

	public void updateIssue() {
		throw new IoTVarNotFoundException("IoT variable searched doesn't exist in fiware");
	}
	
	public String getLastValue() {
		return this.lastValue;
	}

}
