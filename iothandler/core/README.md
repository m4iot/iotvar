#How to use OpenDDS demo
#Keep in mind that this Demo only includes a subscriber (client iotvar)
#a valid publisher is available on at [iotvar_root]/Sources/iothandler/simulation

#first compile
mvn clean install

#to make the libraries available
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/target/classes/OpenDDS_jars

#to lunch the the demo
mvn exec:java@opendds

