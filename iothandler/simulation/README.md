#How to use OpenDDS simulation
#Keep in mind that this simulation only includes a publisher 
#a valid publisher is available on at [iotvar_root]/Sources/iothandler/core

#first compile
mvn clean install

#to make the libraries available
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/target/classes/OpenDDS_jars

#to lunch the the demo
mvn exec:java@simulatoropendds

