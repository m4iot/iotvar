/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package simulation.mudebs;

import mucontext.api.ContextCollector;
import mucontext.contextcollector.BasicContextCollector;
import mucontext.datamodel.context.*;
import mucontext.datamodel.contextcontract.BasicContextProducerContract;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.OperationalMode;
import simulation.GenericRegister;

import javax.xml.transform.TransformerException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Random;

public class MuDEBSRegister extends GenericRegister {

	private ContextCollector contextCollector;
	private ContextDataModelFacade collectorFacade;
	private ContextObservable collectorObservable;
	private String contract = "somecontract";
	private String muContextUriPath = "mucontext://localhost:3000";

	public MuDEBSRegister(String brokerPath, String collectorPath, String collectorAdvFilter, String sensor,
			String property) throws MuDEBSException, URISyntaxException, TransformerException {
		contextCollector = new BasicContextCollector(("--uri " + collectorPath + " --broker " + brokerPath).split(" "));
		collectorFacade = new ContextDataModelFacade("facade");
		ContextEntity userEntity = collectorFacade.createContextEntity(sensor,
				new URI(muContextUriPath + "/sensor/" + sensor));
		collectorObservable = collectorFacade.createContextObservable(property,
				new URI(muContextUriPath + "/" + property), userEntity);
		contextCollector.setContextProducerContract(
				new BasicContextProducerContract(contract, OperationalMode.LOCAL, collectorAdvFilter, null));
	}

	public void post(double payload) {
		ContextObservation<?> observation1 = collectorFacade.createContextObservation("o", payload,
				collectorObservable);
		ContextReport report = collectorFacade.createContextReport("report");
		collectorFacade.addContextObservation(report, observation1);
		try {
			contextCollector.push(contract, collectorFacade.serialiseToXML(report));
		} catch (MuDEBSException e) {
			e.printStackTrace();
		}
	}

	public void randomPost() {
		double randomValue = (20 + 10 * new Random().nextDouble());
		post(randomValue);
	}
}
