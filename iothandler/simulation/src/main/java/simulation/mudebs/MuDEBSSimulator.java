/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package simulation.mudebs;

import mudebs.common.MuDEBSException;
import javax.xml.transform.TransformerException;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MuDEBSSimulator {

	static String collectorAdvFilter = "function evaluate(doc) { return true; }";

	/**
	 *
	 * @param args [0]: publishing frequency (in seconds) | [1]: time of publishing
	 *             in Seconds |[2]number of sensors
	 * @throws InterruptedException
	 * @throws TransformerException
	 * @throws MuDEBSException
	 * @throws URISyntaxException
	 *
	 */
	public static void main(String[] args)
			throws MuDEBSException, TransformerException, InterruptedException, URISyntaxException {
		if (args.length != 4) {
			throw new IllegalArgumentException("No valid args , usage frequency time sensorsNumber ");
		} else {
			int frequency = Integer.parseInt(args[0]);
			int timePub = Integer.parseInt(args[1]);
			int sensorsNbr = Integer.parseInt(args[2]);
			String mudebsURL = args[3];

			List<MuDEBSRegister> sensorsList = new ArrayList<MuDEBSRegister>();
			try {
				for (int i = 0; i < sensorsNbr; i++) {

					sensorsList.add(
							new MuDEBSRegister(mudebsURL, "mudebs://localhost:210" + (i + 3) + "/CollectorHelloWorld",
									collectorAdvFilter, "Sensor" + (i + 1), "Temperature"));

				}

				System.out.println("Start posting");
				int msgNbr = 1;
				while (timePub != 0) {
					long startingTime = System.currentTimeMillis();

					for (int j = 0; j < sensorsNbr; j++) {

						sensorsList.get(j).post(msgNbr);
						System.out.println("Sensor " + (j + 1) + " (Temperature) post message " + msgNbr);

					}
					msgNbr++;
					long endTime = System.currentTimeMillis();
					long time = endTime - startingTime;
					try {
						if ((frequency * 1000) - time > 0)
							TimeUnit.MILLISECONDS.sleep((frequency * 1000) - time);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					timePub--;
				}
				System.out.println("Terminate all executing tasks");
				System.exit(0);
			} catch (MuDEBSException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			} catch (TransformerException e) {
				e.printStackTrace();
			}

		}
	}
}
