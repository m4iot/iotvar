/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package simulation.qodisco;

import org.apache.http.HttpEntity;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import simulation.GenericRegister;
import simulation.LocationEntity;
import simulation.ObservationEntity;
import simulation.utils.RestHttpClient;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Random;



public class QodiscoRegister extends GenericRegister {

    public String qodisco_user ="admin";
    public String qodisco_password = "admin";
    public String qodisco_address = "localhost:8080/qodisco/api";
    public static String broker_address = "tcp://localhost:1883";
    public String originator="admin:admin";
    public static StringBuilder sb = new StringBuilder();

    public static int n = 0  ;
    
    public static void addPrefixes() {
    	sb.append("PREFIX geom: <http://pervasive.semanticweb.org/ont/2004/06/geo-measurement#>");
        sb.append(" PREFIX geos: <http://pervasive.semanticweb.org/ont/2004/06/space#>");
        sb.append(" PREFIX ssn: <http://purl.oclc.org/NET/ssnx/ssn#>");
        sb.append(" PREFIX rdf: <http://www.w3.org/2000/01/rdf-schema#>");
        sb.append(" PREFIX dul: <http://www.loa-cnr.it/ontologies/DUL.owl#>");
        sb.append(" PREFIX qodisco: <http://consiste.dimap.ufrn.br/ontologies/qodisco#>");
        sb.append(" PREFIX qoc: <http://consiste.dimap.ufrn.br/ontologies/qodisco/context#>");
        sb.append(" PREFIX loc: <http://consiste.dimap.ufrn.br/ontologies/qodisco/location#>");
        sb.append(" PREFIX type: <http://consiste.dimap.ufrn.br/ontologies/qodisco/locType#>");
        sb.append(" PREFIX sensor: <http://consiste.dimap.ufrn.br/ontologies/qodisco/sensor#>");
        sb.append(" PREFIX uomvocab: <http://purl.oclc.org/NET/muo/muo#>");
        sb.append(" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>");
        sb.append(" PREFIX service: <http://www.daml.org/services/owl-s/1.1/Service.owl#>");
        sb.append(" PREFIX profile: <http://www.daml.org/services/owl-s/1.1/Profile.owl#>");
        sb.append(" PREFIX process: <http://www.daml.org/services/owl-s/1.1/Process.owl#>");
        sb.append(" PREFIX weather: <http://consiste.dimap.ufrn.br/ontologies/qodisco/weather#>");
        sb.append(" INSERT DATA {");}
   


    private String sensor;
    private String property;
    private LocationEntity location;

    QodiscoRegister(String sensor ,String property, LocationEntity location, String user, String password, String qodiscoURL){
        this.sensor =sensor;
        this.location = location;
        this.property=property;
        this.qodisco_user = user;
        this.qodisco_password = password;
        this.qodisco_address = qodiscoURL + "/qodisco/api";
        this.originator = user + ":" + password;
    }

    /*public void randomPost() {
    	double randomValue = (20+10*new Random().nextDouble());
        post(randomValue);
    }*/
    
    public void randomPost() {
	double randomValue = (20+10*new Random().nextDouble());
	multiQuery(randomValue);
    }
    
    public void multiQuery(double payload) {
    	if(n==0) {
    		addPrefixes();
    	}
    	 ObservationEntity observationEntity = new ObservationEntity(
                 ("http://consiste.dimap.ufrn.br/ontologies/qodisco#Observation"+ new Date().getTime()), sensor,
                 new Date().getTime(), property, "", 0, payload, location);
 	insert(observationEntity);
 	n++;
 		
    }

    //public static String insert( ObservationEntity observation) {
    public static void insert( ObservationEntity observation) {

      //  StringBuilder sb = new StringBuilder();
        /*sb.append("PREFIX geom: <http://pervasive.semanticweb.org/ont/2004/06/geo-measurement#>");
        sb.append(" PREFIX geos: <http://pervasive.semanticweb.org/ont/2004/06/space#>");
        sb.append(" PREFIX ssn: <http://purl.oclc.org/NET/ssnx/ssn#>");
        sb.append(" PREFIX rdf: <http://www.w3.org/2000/01/rdf-schema#>");
        sb.append(" PREFIX dul: <http://www.loa-cnr.it/ontologies/DUL.owl#>");
        sb.append(" PREFIX qodisco: <http://consiste.dimap.ufrn.br/ontologies/qodisco#>");
        sb.append(" PREFIX qoc: <http://consiste.dimap.ufrn.br/ontologies/qodisco/context#>");
        sb.append(" PREFIX loc: <http://consiste.dimap.ufrn.br/ontologies/qodisco/location#>");
        sb.append(" PREFIX type: <http://consiste.dimap.ufrn.br/ontologies/qodisco/locType#>");
        sb.append(" PREFIX sensor: <http://consiste.dimap.ufrn.br/ontologies/qodisco/sensor#>");
        sb.append(" PREFIX uomvocab: <http://purl.oclc.org/NET/muo/muo#>");
        sb.append(" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>");
        sb.append(" PREFIX service: <http://www.daml.org/services/owl-s/1.1/Service.owl#>");
        sb.append(" PREFIX profile: <http://www.daml.org/services/owl-s/1.1/Profile.owl#>");
        sb.append(" PREFIX process: <http://www.daml.org/services/owl-s/1.1/Process.owl#>");
        sb.append(" PREFIX weather: <http://consiste.dimap.ufrn.br/ontologies/qodisco/weather#>");
        sb.append(" INSERT DATA {");*/
        sb.append("\n");
        sb.append(" <" + observation.getName() + "> a ssn:Observation ;");
        sb.append(" ssn:observationResultTime '" + observation.getDate() + "' ;");
        sb.append(" ssn:observedBy sensor:" + observation.getSensor() + " ;");
        sb.append(" ssn:observedProperty weather:" + observation.getObservedProperty() + ";");
        sb.append(" ssn:observationResult <" + observation.getName() + "_output> .");

        sb.append(" sensor:" + observation.getSensor() + " a ssn:SensingDevice ;");
        sb.append(" dul:hasLocation loc:" + observation.getLocationEntity().getName() + " .");

        sb.append(" loc:" + observation.getLocationEntity().getName() + " a type:" + observation.getLocationEntity().getType() + " ;");
        sb.append(" geos:hasCoordinates qodisco:coordinates_" + observation.getLocationEntity().getName() + " ;");
        sb.append(" geos:name '" + observation.getLocationEntity().getName() + "' .");

        sb.append(" qodisco:coordinates_" + observation.getLocationEntity().getName() + " a geom:LocationCoordinates ;");
        sb.append(" geom:latitude " + observation.getLocationEntity().getLatitude() + " ;");
        sb.append(" geom:longitude " + observation.getLocationEntity().getLongitude() + " .");

        sb.append(" <" + observation.getName() + "_output> a ssn:SensorOutput ;");
        sb.append(" ssn:isProducedBy sensor:" + observation.getSensor() + " ;");
        sb.append(" qodisco:has_qoc <" + observation.getName() + "_qocindicator> ;");
        sb.append(" ssn:hasValue <" + observation.getName() + "_resultvalue> .");
        sb.append(" <" + observation.getName() + "_qocindicator> a qodisco:QoCIndicator ;");
        sb.append(" qoc:has_qoc_criterion <" + observation.getQocCriterion() + "> ;");
        sb.append(" qodisco:has_qoc_value " + observation.getQocValue() + " .");
        sb.append(" <" + observation.getName() + "_qocindicator> a qodisco:QoCIndicator ;");
        sb.append(" qoc:has_qoc_criterion <" + observation.getQocCriterion() + "> ;");
        sb.append(" qodisco:has_qoc_value " + observation.getQocValue() + " .");
        sb.append(" <" + observation.getName() + "_resultvalue> a ssn:ObservationValue ;");
       // sb.append(" ssn:hasQuantityValue " + observation.getValue() + " . }");
        sb.append(" ssn:hasQuantityValue " + observation.getValue() + " . ");

        sb.append("\n");

       // return sb.toString();
    }

	@Override
	/*public void post(double payload) {
        ObservationEntity observationEntity = new ObservationEntity(
                ("http://consiste.dimap.ufrn.br/ontologies/qodisco#Observation"+ new Date().getTime()), sensor,
                new Date().getTime(), property, "", 0, payload, location);
		String query = insert(observationEntity);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        URIBuilder uri = new URIBuilder();
        uri.setScheme("http").setHost(qodisco_address).setPath("/record")
                .setParameter("domain", "Default");

        HttpPost postMethod;
        try {

            postMethod = new HttpPost(uri.build());
            postMethod.addHeader("Content-Type", "application/json");
            //postMethod.addHeader("query", query);


            HttpEntity entity = new ByteArrayEntity(query.getBytes("UTF-8"));
            postMethod.setEntity(entity);

            Credentials credentials = new UsernamePasswordCredentials(qodisco_user, qodisco_password);
            httpClient.getCredentialsProvider().setCredentials(AuthScope.ANY, credentials);

            httpClient.execute(postMethod);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }		   
        
	}*/
	
	 
	public  void post(double payload) {
		long start = System.currentTimeMillis();
    DefaultHttpClient httpClient = new DefaultHttpClient();
    URIBuilder uri = new URIBuilder();
    uri.setScheme("http").setHost(qodisco_address).setPath("/record")
            .setParameter("domain", "Default");

    HttpPost postMethod;
    try {

        postMethod = new HttpPost(uri.build());
        postMethod.addHeader("Content-Type", "application/json");
        //postMethod.addHeader("query", query);
        sb.append("}");
        HttpEntity entity = new ByteArrayEntity(sb.toString().getBytes("UTF-8"));
        postMethod.setEntity(entity);

        Credentials credentials = new UsernamePasswordCredentials(qodisco_user, qodisco_password);
        httpClient.getCredentialsProvider().setCredentials(AuthScope.ANY, credentials);

        httpClient.execute(postMethod);
        
        sb = new StringBuilder();
        n=0;
        System.out.println(System.currentTimeMillis()-start);
    } catch (URISyntaxException e) {
        e.printStackTrace();
    } catch(IOException e) {
        e.printStackTrace();
    }		   
    
}


}
