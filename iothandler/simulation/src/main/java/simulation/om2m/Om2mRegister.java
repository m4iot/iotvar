/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package simulation.om2m;

import simulation.LocationEntity;
import simulation.ObservationEntity;
import simulation.GenericRegister;
import simulation.utils.RestHttpClient;

import java.util.Random;

public class Om2mRegister extends GenericRegister {

	public static String originator = "admin:admin";
	public static String cseProtocol = "http";
	public static String cseIp = "127.0.0.1";
	public static int csePort = 8080;
	public static String cseId = "in-cse";
	public static String cseName = "in-name";
	public static String cntName = "data";

	public String csePoa = cseProtocol + "://" + cseIp + ":" + csePort;

	private String sensor;
	private String property;
	private LocationEntity location;
	private ObservationEntity observationEntity;

	Om2mRegister(String sensor, String property, LocationEntity location, String om2mURL) {
		this.sensor = sensor;
		this.property = property;
		this.location = location;
		this.observationEntity = new ObservationEntity();
		this.csePoa = om2mURL;
		observationEntity.setSensor(sensor);
		observationEntity.setObservedProperty(property);
		observationEntity.setLocationEntity(location);
		String bodyCreate = "<m2m:ae xmlns:m2m=\"http://www.onem2m.org/xml/protocols\" rn=\"" + sensor + "\" >\n"
				+ "    <api>app-sensor</api>\n" + "    <lbl>Type/sensor Category/" + property + " Location/"
				+ location.getName() + "</lbl>\n" + "    <rr>false</rr>\n" + "</m2m:ae>";
		String bodyData = "<m2m:cnt xmlns:m2m=\"http://www.onem2m.org/xml/protocols\" rn=\"data\">\n" + "</m2m:cnt>";
		RestHttpClient.post(originator, csePoa + "/~/" + cseId, bodyCreate, 2);
		RestHttpClient.post(originator, csePoa + "/~/" + cseId + "/" + cseName + "/" + sensor, bodyData, 3);
	}

	public void randomPost() {
		/* Modify the value of the observation */
		double randomValue = (20 + 10 * new Random().nextDouble());
		post(randomValue);
	}

	@Override
	public void post(double payload) {
		observationEntity.setValue(payload);
		String body = "<m2m:cin xmlns:m2m=\"http://www.onem2m.org/xml/protocols\">\n" + "<lbl>Category/" + this.property
				+ "</lbl>" + "<cnf>application/xml</cnf>\n" + "<con>" + observationEntity.getValue() + "</con>\n"
				+ "</m2m:cin>";
		RestHttpClient.post(originator, csePoa + "/~/" + cseId + "/" + cseName + "/" + sensor + "/" + cntName, body, 4);
	}
}
