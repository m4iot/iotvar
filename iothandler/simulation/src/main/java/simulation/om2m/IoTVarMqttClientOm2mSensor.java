/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package simulation.om2m;

 import java.util.HashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
  import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
 
  
public class IoTVarMqttClientOm2mSensor {
	private static final Logger logger = LogManager.getLogger(IoTVarMqttClientOm2mSensor.class);

	/**
	 * The hashmap of topics declared in IoTVar and linked with the IoT variable. 
	 */
 	/**
	 * The Mqtt client use to communicate with the broker.
	 */
	private static MqttClient mqttClient;
	
	/**
	 * Initializes the Mqtt Client, connects to the broker and initializes variables.
	 * @param brokerAddress Address of the broker to communicate with.
	 */
	public IoTVarMqttClientOm2mSensor(String brokerAddress,String clientId){
		MemoryPersistence persistence = new MemoryPersistence();
		try {
                System.out.println(brokerAddress);
				mqttClient = new MqttClient(brokerAddress, clientId, persistence);
				MqttConnectOptions connOpts = new MqttConnectOptions();
				connOpts.setCleanSession(true);
				mqttClient.connect(connOpts);
				logger.info("Connected to the broker at the address " + brokerAddress);
			   
		} catch (MqttException e) {
			logger.error("Mqtt problem!",e);
		}
		
		
	}
	
	
	public void publish(String topic ,MqttMessage message ) throws MqttPersistenceException, MqttException {
		mqttClient.publish(topic, message);
		
	}


   

}
