/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package simulation.om2m;

import simulation.LocationEntity;
import simulation.mudebs.MuDEBSRegister;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Om2mSimulator {

	/**
	 *
	 * @param args [0]: publishing frequency (in seconds) | [1]: number of
	 *             publishing | [2] sensors Number
	 *
	 */
	public static void main(String[] args) {
		if (args.length != 4) {
			throw new IllegalArgumentException("No valid args , usage frequency time sensorsNumber");
		} else {
			int frequency = Integer.parseInt(args[0]);
			int timePublishing = Integer.parseInt(args[1]);
			int sensorsNbr = Integer.parseInt(args[2]);
			String om2mURL = args[3];

			List<Om2mRegister> sensorsList = new ArrayList<Om2mRegister>();

			for (int i = 0; i < sensorsNbr; i++) {
				sensorsList.add(new Om2mRegister("sensor" + (i + 1), "temperature",
						new LocationEntity("Home", 21.232, 7.345, 0.0, "Home"), om2mURL));
			}
			System.out.println("Start posting");
			int msgNbr = 1;
			while (timePublishing != 0) {
				long startingTime = System.currentTimeMillis();
				for (int j = 0; j < sensorsNbr; j++) {
					sensorsList.get(j).randomPost();
					System.out.println("Sensor " + (j + 1) + " (Temperature) post message " + msgNbr);
				}

				msgNbr++;
				long endTime = System.currentTimeMillis();
				long time = endTime - startingTime;

				try {
					if ((frequency * 1000) - time > 0)
						TimeUnit.MILLISECONDS.sleep((frequency * 1000) - time);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				timePublishing--;
			}
			System.out.println("Terminate all executing tasks");
			System.exit(0);

		}
	}
}
