/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/
package simulation.fiware;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import simulation.LocationEntity;

public class OrionRegister {

	private String orionUrl = "http://localhost:1026/v2";
	private CloseableHttpClient httpClient = HttpClientBuilder.create().build();
	private String id;
	private String type;
	private String attr;
	private LocationEntity location;

	public OrionRegister(String id, String type, String attr, LocationEntity location, String orionURL) {

		// Creating all entities in a 100 meters radius area from the center defined in
		// the simulator
		this.id = id;
		this.type = type;
		this.attr = attr;
		this.location = getLocationInLatLngRad(100.0, location);
		this.orionUrl = orionURL;

		String json = buildPostJson();
		System.out.println(json);
		System.out.println(this.post("/entities", json, null));

	}

	private String buildPostJson() {
		return String
				.format(Locale.US, "{\n" + "  \"id\": \"%s\",\n" + "  \"type\": \"%s\",\n" + "  \"%s\": {\n"
						+ "    \"value\": %.2f,\n" + "    \"type\": \"float\"\n" + "  },\n" + "  \"humidity\": {\n"
						+ "  	\"value\": %.2f,\n" + "  	\"type\": \"float\"\n" + "  },\n" + "  \"%s\": {\n"
						+ "  	\"value\": \"%s,%s\",\n" + "  	\"type\": \"geo:point\"\n" + "  }\n" + "}", this.id,
						this.type, this.attr, this.randomValue(20, 10), this.randomValue(400, 100),
						this.location.getName(), this.location.getLatitude(), this.location.getLongitude())
				.replaceAll("\\r|\\n", "");
	}

	private String buildPutJson() {
		return String
				.format(Locale.US,
						"{\n" + "    \"%s\": {\n" + "        \"value\": %.2f,\n" + "        \"type\": \"float\"\n"
								+ "    },\n" + "    \"humidity\": {\n" + "        \"value\": %.2f,\n"
								+ "        \"type\": \"float\"\n" + "    },\n" + "    \"location\": {\n"
								+ "        \"type\": \"geo:point\",\n" + "        \"value\": \"%s,%s\",\n"
								+ "        \"metadata\": {}\n" + "    }\n" + "}",
						this.attr, this.randomValue(20, 10), this.randomValue(400, 100), this.location.getLatitude(),
						this.location.getLongitude())
				.replaceAll("\\r|\\n", "");
	}

	private LocationEntity getLocationInLatLngRad(double radiusInMeters, LocationEntity currentLocation) {
		double x0 = currentLocation.getLongitude();
		double y0 = currentLocation.getLatitude();

		Random random = new Random();

		// Convert radius from meters to degrees.
		double radiusInDegrees = radiusInMeters / 111320f;

		// Get a random distance and a random angle.
		double u = random.nextDouble();
		double v = random.nextDouble();
		double w = radiusInDegrees * Math.sqrt(u);
		double t = 2 * Math.PI * v;
		// Get the x and y delta values.
		double x = w * Math.cos(t);
		double y = w * Math.sin(t);

		// Compensate the x value.
		double new_x = x / Math.cos(Math.toRadians(y0));

		double foundLatitude;
		double foundLongitude;

		foundLatitude = y0 + y;
		foundLongitude = x0 + new_x;

		LocationEntity copy = currentLocation;
		copy.setLatitude(foundLatitude);
		copy.setLongitude(foundLongitude);
		return copy;
	}

	public String simulateUpdate() {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("type", this.type);
		return this.put("/entities/" + this.id + "/attrs", this.buildPutJson(), params);
	}

	private double randomValue(int val1, int val2) {
		double randomValue = (val1 + val2 * new Random().nextDouble());

		return randomValue;
	}

	private String post(String action, String json, HashMap<String, String> params) {
		try {
			HttpPost post = new HttpPost(this.orionUrl + action);
			StringEntity postingString = new StringEntity(json);
			post.setEntity(postingString);
			post.setHeader("Content-type", "application/json");
//			post.setHeader("fiware-service", "iotvar");
//			post.setHeader("fiware-servicepath", "/simulator");
			HttpResponse response = this.httpClient.execute(post);
			if (response != null) {
				if (response.getEntity() != null) {
					InputStream in = response.getEntity().getContent();
					return IOUtils.toString(in, "UTF-8");
				}
				return "";
			}
			return null;
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	private String put(String action, String json, HashMap<String, String> params) {
		try {
			URIBuilder uri = new URIBuilder(this.orionUrl + action);

			for (Map.Entry<String, String> entry : params.entrySet()) {
				uri.addParameter(entry.getKey(), entry.getValue());
			}

			HttpPut put = new HttpPut(uri.build());
			StringEntity postingString = new StringEntity(json);
			put.setEntity(postingString);
			put.setHeader("Content-type", "application/json");
//			put.setHeader("fiware-service", "iotvar");
//			put.setHeader("fiware-servicepath", "/simulator");
			HttpResponse response = this.httpClient.execute(put);
			if (response != null) {
				if (response.getEntity() != null) {
					InputStream in = response.getEntity().getContent();
					return IOUtils.toString(in, "UTF-8");
				}
				return "";
			}
			return null;
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		} catch (URISyntaxException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

}
