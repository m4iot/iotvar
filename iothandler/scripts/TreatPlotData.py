import pandas as pd
import sys
import os
from os import listdir
import matplotlib
import random
import matplotlib.pyplot as plt
def find_filenamesBySuffix( path_to_dir, suffix ):
    filenames = listdir(path_to_dir)
    return [ filename for filename in filenames if filename.endswith( suffix ) ]

def find_filenamesByName( path_to_dir, name ):
    filenames = listdir(path_to_dir)
    return [ filename for filename in filenames if filename==name]


def treatAndMerge(plateforme,filename):
    filenames_json = find_filenamesBySuffix(plateforme+"/",".json")
    for name in filenames_json:
        df = pd.read_json(plateforme+"/"+name)
        name = name.strip()
        name = name[:-5]
        df.to_csv(plateforme+"/"+name+".csv")


    filenames_csv = find_filenamesBySuffix(plateforme+"/",".csv")
    somme = 0
    moyenne = 0
    fixePart = df=pd.read_csv(plateforme+"/"+filenames_csv[0], sep=',')
    fixePart = fixePart[['CPUtimeUnit' ,'PeriodUnit','MemUsageUnit','PublishingPeriod','SensorsNumber'
    ,'freshnessPeriod','iotvarNumber','numberOfPublishing','plateformePattern']]

    for name in filenames_csv:
        df=pd.read_csv(plateforme+"/"+name, sep=',')
        df = df[['CPUtime','MemUsage','numberOfmsg','msgsRecSizeBytes','errorRatePourcentage']]
        somme +=df
    moyenne = somme/len(filenames_csv )
    df3 = pd.merge(moyenne, fixePart, left_index=True, right_index=True)
    df3.to_csv(plateforme+"/resultsPerfs/"+filename+".csv")
    return df3

def loadData(plateforme ,filename):
    df = pd.read_csv(plateforme+"/resultsPerfs/"+filename+".csv")
    return df ;




def plot(Xaxis,Yaxis,df) :
    print(df)
    if Yaxis == "CPUtime" :
        plt.plot((df [Xaxis]).values ,(df [Yaxis]).values/1000000000)
        Yaxis = "CPU_Time"
        unitY = "(S)"
    elif Yaxis == "MemUsage" :
        plt.plot((df [Xaxis]).values, (df [Yaxis]).values/1000000)
        Yaxis = "Memory"
        unitY = "(MB)"


    elif Yaxis == "numberOfmsg" :
        plt.plot((df [Xaxis]).values, (df [Yaxis]).values)
        Yaxis = "Number_Of_Message_Received"

    else :
        plt.plot((df [Xaxis]).values, (df [Yaxis]).values)


    plt.margins(0)
    unitY=""
    unitX=""

    if (Xaxis=="iotvarNumber"):
        Xaxis = "Iotvar_Number"
    if (Xaxis=='freshnessPeriod'or Xaxis=="PublishingPeriod"):
        unitX="(S)"
    plt.ylabel(Yaxis+unitY)
    plt.xlabel(Xaxis+unitX)
    plt.grid(True)
    plt.title(df['plateformePattern'][0])
    plt.show()

def clean(plateforme):
    fjson = find_filenamesBySuffix (plateforme+"/",".json")
    fcsv = find_filenamesBySuffix (plateforme+"/",".csv")
    if len(fjson)>0 or len(fcsv) >0:
        os.system('cd '+plateforme+'; rm *.csv *.json')


if __name__ == '__main__':
    if len(sys.argv) == 5 :

        filename = sys.argv[1]
        plateforme = sys.argv[2]
        Xaxis = sys.argv[3]
        Yaxis = sys.argv[4]
        files = find_filenamesByName( plateforme+"/resultsPerfs/", filename+".csv" )
        if len(files)==0 :
            df = treatAndMerge(plateforme,filename)
            plot(Xaxis,Yaxis,df)
            clean(plateforme)
        else :
            df = loadData(plateforme, filename)
            plot(Xaxis,Yaxis,df)
    else :
        print("Usage : python TreatPlotData.py [filename][plateforme], [Xaxis] , [Yaxis]")
        print("plateforme = Om2m | Qodisco | Mudebs ")
        print("Xaxis|Yaxis = PublishingFrequency | SensorsNumber | freshnessFrequency |iotvarNumber|numberOfPublishing|CPUtime| MemUsage ")
