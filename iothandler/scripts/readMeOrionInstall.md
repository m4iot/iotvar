# Made by Pedro Victor Borges Caldas da Silva
# https://www.linkedin.com/in/pedrovb/


# This script was made for Ubuntu 18.04.
# For official install instructions go to
# https://fiware-orion.readthedocs.io/en/master/admin/build_source/index.html


#
# You don't need to use super user for the instalation
# but extra steps are necessary in this case
#
sudo su


#
# Uptade ubuntu apt-get
#
sudo apt-get update


#
# Basic packages
#


sudo apt-get install emacs
sudo apt-get install aptitude
sudo apt-get install git
sudo apt-get install libssl-dev


#
# To compile the broker, install the following packages:
#
sudo aptitude install cmake
sudo aptitude install g++
sudo aptitude install libcurl4-openssl-dev
sudo aptitude install uuid-dev
sudo aptitude install libgnutls-dev
sudo aptitude install libgcrypt-dev
sudo aptitude install scons
sudo aptitude install libsasl2-dev
sudo aptitude install libboost-dev
sudo aptitude install libboost-regex-dev
sudo aptitude install libboost-thread-dev
sudo aptitude install libboost-filesystem-dev
sudo aptitude install rapidjson-dev
sudo aptitude install libmicrohttpd-dev
sudo aptitude install mongodb-clients
sudo aptitude install mongodb-dev


#
# Clone Orionn
#


cd /
mkdir git
cd git
git clone https://github.com/telefonicaid/fiware-orion.git
cd fiware-orion
git checkout 2.2.0


#
# Near line 80 in CMakeLists.txt there is a command to check for the distro
# Delete only "-Werror" from the strings
#


#
# Ready to build and install the broker
# However, to install in privileged directories, we need to prepare a little first:
# (Only needed if you did not install as su)
#
sudo touch /usr/bin/contextBroker
sudo touch /etc/default/contextBroker
sudo mkdir /etc/init.d/contextBroker
sudo chown <user>:<group> /usr/bin/contextBroker
sudo chown <user>:<group> /etc/default/contextBroker
sudo chown <user>:<group> /etc/init.d/contextBroker


# 
# Build and install orion
#


make di


# 
# Check it's installed
#


contextBroker --version


#
# Something like bellow should appear:
# 2.2.0 (git version: 5a46a70de9e0b809cce1a1b7295027eea0aa757f)
# Copyright 2013-2019 Telefonica Investigacion y Desarrollo, S.A.U
# Orion Context Broker is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Orion Context Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
# 
# Telefonica I+D
#


# 
# Run Orion in foreground mode
# For more command line options use -u (brief) or -U (long) or go to
# https://fiware-orion.readthedocs.io/en/master/admin/cli/index.html
#


contextBroker -fg


# 
# Check that it's ok with curl
#


curl localhost:1026/version


#
# Something similar to this should appear:
# {
# "orion" : {
#   "version" : "2.2.0",
#   "uptime" : "0 d, 0 h, 7 m, 9 s",
#   "git_hash" : "5a46a70de9e0b809cce1a1b7295027eea0aa757f",
#   "compile_time" : "nodate",
#   "compiled_by" : "root",
#   "compiled_in" : "mypc",
#   "release_date" : "nodate",
#   "doc" : "https://fiware-orion.rtfd.io/en/2.2.0/"
# }
# }
#

