import paramiko
import sys
sys.path.insert(0, '/home/touansi/iotvar/Sources/iothandler/scripts')
from Host import Host


class MuDEBSBrokerService:
    def __init__(self, bind_address, bind_port, path):
        self.bind_address = bind_address
        self.bind_port = bind_port
        self.path = path

    def get_start_command(self):
        return '''
            cd ~/mudebs/sources/muDEBS/scripts;
            ./startbroker --uri mudebs://{}:{}/{}''' \
            .format(self.bind_address, self.bind_port, self.path)

    def get_stop_command(self):
        return '''killall java'''

if __name__ == '__main__':
    b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")
    try :
        b313.connect()
        broker = MuDEBSBrokerService("localhost", 2000, "BrokerA")
        b313.deploy(broker)

    finally:
        print("borker launched")
