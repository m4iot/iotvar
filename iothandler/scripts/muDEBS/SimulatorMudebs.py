import paramiko
import sys
sys.path.insert(0, '/home/touansi/iotvar/Sources/iothandler/scripts')
from Host import Host


class MuDEBSSimulatorService:
    def __init__(self, iotvar_dir, publishing_frequency, publishing_time,sensorsNumber):
        self.iotvar_dir = iotvar_dir
        self.publishing_frequency = publishing_frequency
        self.publishing_time = publishing_time
        self.sensorsNumber = sensorsNumber

    def get_start_command(self):
        return '''
            cd {}/simulation;
            export JAVA_HOME=/usr/lib/jvm/java-10-openjdk-10.0.1.10-4.fc28.x86_64/;
            export PATH=$JAVA_HOME/bin:$PATH;
            mvn exec:java -Dexec.mainClass=simulation.mudebs.MuDEBSSimulator -Dexec.args="{} {} {}"
            '''.format(self.iotvar_dir, self.publishing_frequency, self.publishing_time,self.sensorsNumber)

    def get_stop_command(self):
        return '''killall java'''


def simulatorMudebs (frequency , time , sensorsNumber):
    b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")
    try :
        b313.connect()
        simulator = MuDEBSSimulatorService("iotvar/Sources/iothandler",frequency,time,sensorsNumber)
        b313.deploy(simulator)

    finally:
         print("Simulator launched")



if __name__ == '__main__':
    if len(sys.argv) == 4 :
        frequency = int(sys.argv[1])
        time = int(sys.argv[2])
        sensorsNumber = int(sys.argv[3])
        simulatorMudebs(frequency,time,sensorsNumber)

    else :
        print("Usage : python SimulatorMudebs.py [frequency] [time] [sensors number]")
