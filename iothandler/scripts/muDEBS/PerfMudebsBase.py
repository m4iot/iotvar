import paramiko
import sys
sys.path.insert(0, '/home/touansi/iotvar/Sources/iothandler/scripts')
from Host import Host
import time
import subprocess
import os 


class IotvarMuDEBSExampleService:
    def __init__(self, iotvar_dir, broker_address, broker_port, broker_path, iotvar_number, test_time, performance_file,freqPub,timePub,sensorNumb):
        self.iotvar_dir = iotvar_dir
        self.broker_address = broker_address
        self.broker_port = broker_port
        self.broker_path = broker_path
        self.performance_file = performance_file
        self.test_time = test_time
        self.iotvar_number = iotvar_number
        self.freqPub = freqPub
        self.timePub = timePub
        self.sensorNumb = sensorNumb

    def get_start_command(self):
        return '''
            cd {}/application;
            mvn exec:java -Dexec.mainClass=iotvarPerformance.mudebs.IotvarMuDEBSExample \
            -Dexec.args="{} {} {} \
            --uri mudebs://{}:2102/ApplicationHelloWorld \
            --broker mudebs://{}:{}/{} {} {} {}"
            '''.format(self.iotvar_dir, self.iotvar_number, self.test_time, self.performance_file,
            self.broker_address ,self.broker_address, self.broker_port, self.broker_path,self.freqPub,self.timePub,self.sensorNumb)

    def get_stop_command(self):
        return '''killall java'''

def performance_test(nb_iot,test_time,freqPub,timePub,sensorNumb,filename):
    localhost = Host("localhost", "touansi","touansi", "/home/touansi/iotvar/Sources/iothandler")
    b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")
    try :
        b313.connect()
        iotvarMuDEBSExampleService = IotvarMuDEBSExampleService(localhost.working_dir, b313.hostname, 2000, "BrokerA", nb_iot,test_time, filename,freqPub,timePub,sensorNumb)
        os.system(''' cd /home/touansi/iotvar/Sources/iothandler/scripts/ ;
        echo "before test mudebs " >> battery.txt;
        upower -i $(upower -e | grep BAT) | grep --color=never -E "percentage" | tee -a battery.txt;
        upower -i $(upower -e | grep BAT) | grep --color=never -E "energy:" | tee -a battery.txt;
        ''')
        localhost.loclaDeploy(iotvarMuDEBSExampleService)
        print("Client launched")

    finally:
        os.system(''' cd /home/touansi/iotvar/Sources/iothandler/scripts/ ;
        echo "after test mudebs " >> battery.txt;
        upower -i $(upower -e | grep BAT) | grep --color=never -E "percentage" | tee -a battery.txt;
        upower -i $(upower -e | grep BAT) | grep --color=never -E "energy:" | tee -a battery.txt;
        ''')
        b313.ssh_session.exec_command("killall java")
        #stark0.retrieve_file("application/"+filename, filename)
        b313.ssh_session.close()

        print("Performance Test Finished")



if __name__ == '__main__':

    if len(sys.argv) == 7 :
        nb_iot =int(sys.argv[1])
        test_time =int(sys.argv[2])
        freqPub =int(sys.argv[3])
        timePub =int(sys.argv[4])
        sensorNumb=int(sys.argv[5])
        filename= sys.argv[6]

        performance_test(nb_iot,test_time,freqPub,timePub,sensorNumb,filename)

    else :
        print("Usage : python PerfMudebsBase [iotvar_number] [Time_of_each_test (in s)] [freqPub] [timePub] [sensorNumb] [filename]")
