import pandas as pd
import sys
import os
from os import listdir
import matplotlib
import random
import matplotlib.pyplot as plt
def plot(Xaxis,Yaxis,df) :
    listFormat = ['--',':','-','-.','^','o']
    i=0
    for file in df:

        if Yaxis == "CPUtime" :
            plt.plot((file [Xaxis]).values, (file [Yaxis]).values/1000000000 ,listFormat[i],label=file ['plateformePattern'][0])
        elif Yaxis == "MemUsage" :
            plt.plot((file [Xaxis]).values, (file [Yaxis]).values/1000000 ,listFormat[i],label=file ['plateformePattern'][0])
        else :
            plt.plot((file [Xaxis]).values, (file [Yaxis]).values,listFormat[i],label=file ['plateformePattern'][0])


        i=i+1

    unitY=""
    unitX=""

    if Yaxis == "CPUtime" :
        Yaxis = "CPU_Time"
        unitY = "(S)"
    elif Yaxis == "MemUsage"  :
        Yaxis = "Memory"
        unitY = "(MB)"
    if (Xaxis=='freshnessPeriod'or Xaxis=="PublishingPeriod"):
        unitX="(S)"
    if (Xaxis=="iotvarNumber"):
        Xaxis = "Iotvar_Number"
    if Yaxis == "numberOfmsg" :
        Yaxis = "Number_Of_Message_Received"
        
    plt.ylabel(Yaxis+unitY)
    plt.xlabel(Xaxis+unitX)
    plt.grid(True)
    plt.legend()
    plt.margins(0)
    plt.show()

def load_files(listOfPath):
    listOfFiles = []
    for path in listOfPath:
        df = pd.read_csv(path)
        listOfFiles.append(df)

    return listOfFiles


if __name__ == '__main__':
    if len(sys.argv) >3 :
        Xaxis = sys.argv[1]
        Yaxis = sys.argv[2]
        ListOfPath = sys.argv[3:]
        listOfFiles = load_files(ListOfPath)
        plot(Xaxis , Yaxis , listOfFiles)

    else :
        print("Usage : python PlotN.py  [Xaxis], [Yaxis] , [ListOfPath]")
        print("Xaxis|Yaxis = PublishingFrequency | SensorsNumber | freshnessFrequency |iotvarNumber|numberOfPublishing|CPUtime| MemUsage ")
