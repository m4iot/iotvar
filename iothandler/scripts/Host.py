import paramiko
import time
import subprocess
import os

class Host:
    def __init__(self, hostname, username,password, working_dir):
        self.hostname = hostname
        self.username = username
        self.password = password
        self.working_dir = working_dir
        self.ssh_session = None
        self.services = {}

    def connect(self):
        self.ssh_session = paramiko.SSHClient()
        self.ssh_session.load_system_host_keys()
        paramiko.util.log_to_file("filename.log")
        self.ssh_session.connect(self.hostname, username=self.username,password=self.password)
        transport = self.ssh_session.get_transport()
        transport.set_keepalive(1)

    def close(self):
        for s in self.services.keys():
            self.ssh_session.exec_command(s.get_stop_command())
        self.ssh_session.close()

    def deploy(self, service):
        stdin, stdout, stderr = self.ssh_session.exec_command(service.get_start_command())
        self.services[service] = (stdin, stdout, stderr)
        '''stdout=stdout.readlines()
        output=""
        for line in stdout:
            output=output+line

        #print (output)

        stderr=stderr.readlines()
        outputerr=""
        for line in stderr:
            outputerr=outputerr+line

        #print (outputerr)'''
        
    def loclaDeploy(self, service):
        os.system(service.get_start_command())





    def print_service(self, service):
        for l in self.services[service][1]:
            print(l)

    def retrieve_file(self, remotepath, localpath):
        sftp_client = self.ssh_session.open_sftp()
        #print(self.working_dir + "/" + remotepath)
        sftp_client.get(self.working_dir + "/" + remotepath, localpath)
