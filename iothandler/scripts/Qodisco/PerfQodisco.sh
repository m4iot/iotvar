echo "starting fuseki"
python Fuseki.py &
echo "starting qodisco broker"
python QodiscoBroker.py &
sleep 30
echo "starting qodisco Client"
python QodiscoClient.py $1 $2 $3 $4 $5 $6 $7 $8 &
sleep 20
echo "starting the simulator"
python SimulatorQodisco.py $5 $6 $7
