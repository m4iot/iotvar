import paramiko
import sys
sys.path.insert(0, '/home/touansi/iotvar/Sources/iothandler/scripts')
import time
from Host import Host


class QoDiscoService:
    def get_start_command(self):
        return '''
            cd ~/qodisco;
            export JAVA_HOME=/usr/lib/jvm/java-10-openjdk-10.0.1.10-4.fc28.x86_64/;
            export PATH=$JAVA_HOME/bin:$PATH;
            mvn clean install exec:java'''

    def get_stop_command(self):
        return '''killall java'''

if __name__ == '__main__':
    b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")
    b313.connect()
    qodisco = QoDiscoService()
    print("QoDisco launched")
    b313.deploy(qodisco)
