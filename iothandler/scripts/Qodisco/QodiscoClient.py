import paramiko
import sys
sys.path.insert(0, '/home/touansi/iotvar/Sources/iothandler/scripts')
import time
from Host import Host
import os

class IotvarQoDiscoExampleService:
    def __init__(self, iotvar_dir, qodisco_address, iotvar_number, test_time, performance_file, strategy,freshness,freqPub,timePub,sensorNumb):
        self.iotvar_dir = iotvar_dir
        self.qodisco_address = qodisco_address
        self.performance_file = performance_file
        self.test_time = test_time
        self.iotvar_number = iotvar_number
        self.strategy = strategy
        self.freshness = freshness
        self.freqPub = freqPub
        self.timePub = timePub
        self.sensorNumb = sensorNumb

    def get_start_command(self):
        return '''
            cd {}/application;
            mvn exec:java -Dexec.mainClass=iotvarPerformance.QoDisco.IoTvarQoDiscoExample \
            -Dexec.args="{} {} {} {} {} {} {} {} {}"
            '''.format(self.iotvar_dir, self.iotvar_number, self.test_time, self.performance_file, self.qodisco_address,
                       self.strategy,self.freshness,self.freqPub,self.timePub,self.sensorNumb)

    def get_stop_command(self):
        return '''killall java'''

def performance_test(nb_iot, test_time, strategy,freshness,freqPub,timePub,sensorNumb,filename):
    localhost = Host("localhost", "touansi","touansi", "/home/touansi/iotvar/Sources/iothandler")
    b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")
    try:
        b313.connect()
        iotvarQoDiscoExampleService = IotvarQoDiscoExampleService(localhost.working_dir,
        b313.hostname + ":8080/qodisco/api", nb_iot, test_time, filename, strategy,freshness,freqPub,timePub,sensorNumb)
        os.system(''' cd /home/touansi/iotvar/Sources/iothandler/scripts/ ;
        echo "before test Qodisco {} " >> battery.txt;
        upower -i $(upower -e | grep BAT) | grep --color=never -E "percentage" | tee -a battery.txt;
        upower -i $(upower -e | grep BAT) | grep --color=never -E "energy:" | tee -a battery.txt;
        '''.format(iotvarQoDiscoExampleService.strategy))
        localhost.loclaDeploy(iotvarQoDiscoExampleService)
        print("Client launched")


    finally:
        os.system(''' cd /home/touansi/iotvar/Sources/iothandler/scripts/ ;
        echo "after test Qodisco {} " >> battery.txt;
        upower -i $(upower -e | grep BAT) | grep --color=never -E "percentage" | tee -a battery.txt;
        upower -i $(upower -e | grep BAT) | grep --color=never -E "energy:" | tee -a battery.txt;
        '''.format(iotvarQoDiscoExampleService.strategy))
        time.sleep(15)
        b313.ssh_session.exec_command("cd /mci/ei1518/touansi/iotvar/Sources/iothandler/application ;mysql -u root qodisco < qodisco.sql")

        b313.ssh_session.exec_command("killall java")
            #stark0.retrieve_file("application/"+filename, filename)
        b313.ssh_session.close()

        print("Performance Test Finished")


if __name__ == '__main__':
    if len(sys.argv) == 9 :
        iotvar_number = int(sys.argv[1])
        test_time =int(sys.argv[2])
        strategy = sys.argv[3]
        freshness = int(sys.argv[4])
        freqPub = int(sys.argv[5])
        timePub = int(sys.argv[6])
        sensorNumb = int(sys.argv[7])
        filename = sys.argv[8]
        performance_test(iotvar_number,test_time,strategy,freshness,freqPub,timePub,sensorNumb,filename)
    else :
        print("Usage : python PerformanceQoDisco.py [Time_of_each_test], [STRATEGY] , [freshness] , [freqPub] [timePub] [sensorNumb] [filename]")
        print("STRATEGY = sync | pubsub | contentpubsub ")
