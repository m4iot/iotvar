import paramiko
import sys
sys.path.insert(0, '/home/touansi/iotvar/Sources/iothandler/scripts')
import time
from Host import Host

class FusekiService:
    def get_start_command(self):
        return '''
            cd apache-jena-fuseki-2.6.0;
            ./fuseki-server --update --loc=resources /dataset'''

    def get_stop_command(self):
        return '''killall java'''

if __name__ == '__main__':
    b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")
    b313.connect()
    fuseki = FusekiService()
    print("Fuseki launched")
    b313.deploy(fuseki)
