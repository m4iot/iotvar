import paramiko
import time
import sys
sys.path.insert(0, '/home/touansi/iotvar/Sources/iothandler/scripts')
from Host import Host

class OM2MSimulatorService:
    def __init__(self, iotvar_dir, publishing_frequency, publishing_time,sensors_number):
        self.iotvar_dir = iotvar_dir
        self.publishing_frequency = publishing_frequency
        self.publishing_time = publishing_time
        self.sensors_number = sensors_number

    def get_start_command(self):
        return '''
            cd {}/simulation;
            export JAVA_HOME=/usr/lib/jvm/java-10-openjdk-10.0.1.10-4.fc28.x86_64/;
            export PATH=$JAVA_HOME/bin:$PATH;
            mvn exec:java -Dexec.mainClass=simulation.om2m.Om2mSimulatorPubsub -Dexec.args="{} {} {}"
            '''.format(self.iotvar_dir, self.publishing_frequency, self.publishing_time,self.sensors_number)

    def get_stop_command(self):
        return '''killall java'''


def simulator(publishing_frequency, publishing_time,sensors_number):
    b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")
    try:
        b313.connect()
        simulatorService = OM2MSimulatorService(b313.working_dir,publishing_frequency , publishing_time ,sensors_number)
        b313.deploy(simulatorService)

        #stark1.retrieve_file("application/"+output_file, output_file)
    finally:
        #stark0.close()
        #stark1.close()
        print("Simulator launched")

if __name__ == '__main__':
    if len(sys.argv) == 4 :
        publishing_frequency = int(sys.argv[1])
        publishing_time = int(sys.argv[2])
        sensors_number = int(sys.argv[3])
        simulator(publishing_frequency,publishing_time,sensors_number)

    else :
        print("Usage : python PerformanceOM2M.py [publishing_frequency] [publishing_time] [sensors_number]")
