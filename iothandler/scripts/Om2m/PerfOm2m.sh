echo "starting the broker"
python Om2mBroker.py
sleep 10
echo "starting the client"
python PerfOm2mBase.py $1 $2 $3 $4 $5 $6 $7 $8 &
sleep 20


if [ "$3" = "pubsub" ]
then
    echo "starting the simulator "
    python SimulatorOm2mPubSub.py $5 $6 $7 &

else
  echo "starting the simulator"
  python SimulatorOm2m.py $5 $6 $7 &
fi
