import paramiko
import time
import sys
sys.path.insert(0, '/home/touansi/iotvar/Sources/iothandler/scripts')
from Host import Host
import os



class IotvarOM2MExampleService:
    def __init__(self, iotvar_dir, number_of_IoT, running_time,strategy,freshness, om2m_address, performance_file ,freqPub,timePub,sensorNumb):
        self.iotvar_dir = iotvar_dir
        self.number_of_IoT = number_of_IoT
        self.running_time= running_time
        self.om2m_address = om2m_address
        self.performance_file = performance_file
        self.strategy = strategy
        self.freshness = freshness
        self.freqPub = freqPub
        self.timePub = timePub
        self.sensorNumb = sensorNumb

    def get_start_command(self):
        #ARGS : number of IoTvar | the running time | the output file | OM2M address
        return '''
            cd {}/application;
            mvn exec:java -Dexec.mainClass=iotvarPerformance.om2m.IoTvarExampleOM2M \
            -Dexec.args="{} {} {} {} {} {} {} {} {}"
            '''.format(self.iotvar_dir, self.number_of_IoT, self.running_time,self.performance_file, self.om2m_address , self.strategy,self.freshness,self.freqPub,self.timePub,self.sensorNumb)

    def get_stop_command(self):
        return '''killall java'''


def performance_test(nb_iot, test_time ,strategy ,freshness,freqPub,timePub,sensorNumb,filename):
    localhost = Host("localhost", "touansi","touansi", "/home/touansi/iotvar/Sources/iothandler")
    b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")
    try:
        b313.connect()
        iotvarOM2MExampleService = IotvarOM2MExampleService(localhost.working_dir, nb_iot ,test_time,strategy,freshness, b313.hostname + ":8080/~", filename,freqPub,timePub,sensorNumb)

        os.system(''' cd /home/touansi/iotvar/Sources/iothandler/scripts/ ;
        echo "before test Om2m {} " >> battery.txt;
        upower -i $(upower -e | grep BAT) | grep --color=never -E "percentage" | tee -a battery.txt;
        upower -i $(upower -e | grep BAT) | grep --color=never -E "energy:" | tee -a battery.txt;
        '''.format(iotvarOM2MExampleService.strategy))
        localhost.loclaDeploy(iotvarOM2MExampleService)


        print("Client launched")


    finally:
        os.system(''' cd /home/touansi/iotvar/Sources/iothandler/scripts/ ;
        echo "after test Om2m {} " >> battery.txt;
        upower -i $(upower -e | grep BAT) | grep --color=never -E "percentage" | tee -a battery.txt;
        upower -i $(upower -e | grep BAT) | grep --color=never -E "energy:" | tee -a battery.txt;
        '''.format(iotvarOM2MExampleService.strategy))
        time.sleep(15)
        b313.ssh_session.exec_command("killall java")
        #stark0.retrieve_file("application/"+filename, filename)
        b313.ssh_session.close()

        print("Performance Test Finished")

if __name__ == '__main__':
    if len(sys.argv) == 9 :
        nb_iot =int(sys.argv[1])
        test_time =int(sys.argv[2])
        strategy = sys.argv[3]
        freshness = int(sys.argv[4])
        freqPub = int(sys.argv[5])
        timePub = int(sys.argv[6])
        sensorNumb = int(sys.argv[7])
        filename = sys.argv[8]



        performance_test(nb_iot,test_time,strategy,freshness,freqPub,timePub,sensorNumb,filename)
    else :
        print("Usage : python PerformanceOM2M.py [iotvar_number] [Time_of_each_test (in s)] [Strategy] [freshness] [frequencyPub] [timePub] [sensorNumb] [filename]")
