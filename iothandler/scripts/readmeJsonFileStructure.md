The json file of  performance is structured as the following :
{"plateformePattern":"mudebs", "iotvarNumber":0, "CPUtime":0, "CPUtimeUnit":"ns", "MemUsage":0, "MemUsageUnit":"byte", "SensorsNumber":0, "PublishingPeriod":0, "freshnessPeriod":0,
"PeriodUnit":"s","numberOfPublishing":300, "numberOfmsg":0, "msgsRecSizeBytes":0, "errorRatePourcentage":0.0}

- the plateformePattern indicate the palteforme and the strategy used in the test
- iotvarNumber is the number of iot variables used in the test
- CPUtime is the processing time  in ns
- MemUsage is the memory consumption  in Byte
- CPUtimeUnit is the unit of CPUtime (ns)
- MemUsageUnit is the unit of MemUsage (Byte)
- SensorsNumber is the number of sensors used in the test
- PublishingPeriod is the publication period of sensors
- freshnessPeriod : in sync mode is the freshness used by the iot variable and in pubsub-filter is the filter used in the server side
- PeriodUnit is the unit of the publishing period
- numberOfPublishing is the number of publication sent by each sensor
- numberOfmsg is the number of messages received by all the iot variables
- msgsRecSizeBytes is the size in byte of all the messages received by all variables
- errorRatePourcentage is the pourcentage of messages sent by sensors but not received by variables