if you want to change the configuration and the hosts used for performance tests you shoud follow the insctructions bellow

1- Mudebs :
  -you should modify "MudebsBroker.py" and "SimulatorMudebs.py" files :

  change this line :
  "b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")"
-  hostname: the first field you should change it by the name of the new machine , the second and the third field
-  username, password: are the login and password , change them by yours.
- working_dir: the last field should not be changed
-  and also you could change the name of the variable "b313" by a new name

  -you should modify "PerfMudebsBase.py" file :
  change this line :   "b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")"
  the first field you should change it by the name of the new machine , the second and the third field
  are the login and password , change them by yours.
  and also you could change the name of the variable "b313" by a new name

  change this line like the first one :
  localhost = Host("localhost", "touansi","touansi", "/home/touansi/iotvar/Sources/iothandler")

  the localhost variable refers to the Client host in which only the client executes.
  In this configuration I used my personal computer as host for the client and I used a function
  called "loclaDeploy" which executes the client in the local machine without ssh session , if you want to execute the client in a remote host you
  should use the function "deploy" instead


  - if you changed the name of variables you should  update this ligne :
  iotvarMuDEBSExampleService = IotvarMuDEBSExampleService(localhost.working_dir, b313.hostname, 2000, "BrokerA", nb_iot,test_time, filename,freqPub,timePub,sensorNumb)
  you must update the tow first parameters with the new variables

finally you should change the different paths in the code by your own paths.


2- Qodsico :
  you should modify "Fuseki.py" , "QodiscoBroker.py" , "QodiscoSimulator.py" files :
  change this line :
  "b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")"
  the first field you should change it by the name of the new machine , the second and the third field
  are the login and password , change them by yours.
  and also you could change the name of the variable "b313" by a new name

  -you should modify "QodiscoClient.py" file :
  change this line :   "b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")"
  the first field you should change it by the name of the new machine , the second and the third field
  are the login and password , change them by yours.
  and also you could change the name of the variable "b313" by a new name

  change this line like the fist one :
  localhost = Host("localhost", "touansi","touansi", "/home/touansi/iotvar/Sources/iothandler")

  the localhost varibale refers to the Client host in which only the client execute.
  in this configuration i used my personal computer as host for the client and I used a function
  called "loclaDeploy" which execute the client in the local machine without ssh session , if you want to execute the client in a remote host you
  should use the function "deploy" instead

  - if you changed the name of variables you should  update this ligne :

  iotvarQoDiscoExampleService = IotvarQoDiscoExampleService(localhost.working_dir, b313.hostname + ":8080/qodisco/api", nb_iot, test_time, filename, strategy,freshness,freqPub,timePub,sensorNumb)

  you must update the tow first parameters with the new variables


finally you should change the different paths in the code by your own paths.

3- Om2m
you should modify "Om2mBroker.py" , "SimulatorOm2m.py" , "SimulatorOm2mPubSub.py" files :
change this line :
"b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")"
the first field you should change it by the name of the new machine , the second and the third field
are the login and password , change them by yours.
and also you could change the name of the variable "b313" by a new name

-you should modify "PerfOm2mBase.py" file :
change this line :   "b313 = Host("b313-13.int-evry.fr", "touansi","14Janvier2011*" ,"iotvar/Sources/iothandler")"
the first field you should change it by the name of the new machine , the second and the third field
are the login and password , change them by yours.
and also you could change the name of the variable "b313" by a new name

change this line like the fist one :
localhost = Host("localhost", "touansi","touansi", "/home/touansi/iotvar/Sources/iothandler")

the localhost varibale refers to the Client host in which only the client execute.
in this configuration i used my personal computer as host for the client and I used a function
called "loclaDeploy" which execute the client in the local machine without ssh session , if you want to execute the client in a remote host you
should use the function "deploy" instead


- if you changed the name of variables you should  update this ligne :

iotvarOM2MExampleService = IotvarOM2MExampleService(localhost.working_dir, nb_iot ,test_time,strategy,freshness, b313.hostname + ":8080/~", filename,freqPub,timePub,sensorNumb)


you must update the tow first parameters with the new variables

finally you should change the different paths in the code by your own paths.
