import json
import matplotlib.pyplot as plt


class ResultFile:
    def __init__(self, filename, iotvar_number, memoryRecords, timeRecords):
        self.filename = filename
        self.iotvar_number = iotvar_number
        self.memoryRecords = memoryRecords
        self.timeRecords = timeRecords

    def __str__(self):
        return self.filename + " " + self.iotvar_number + " MemRecords" + self.memoryRecords + " TimeRecords " + self.timeRecords


def load_plateform(filename_format, file_numbers):
    result = []
    for i in file_numbers:
        f = filename_format.format(i)
        with open(f) as file:
            j = json.load(file)
            result.append(ResultFile(f, i, j["MemoryUsage"], j["TimeResult"]))
    return result

# qodisco = load_plateform(r'QoDisco_with_*')
qodisco = load_plateform("QoDisco_with_{}_IotVar.json", [1, 41, 81, 121, 161])
om2m = load_plateform("OM2M_with_{}_IotVar.json", [1, 41, 81, 121, 161, 201])
mudebs = load_plateform("MuDBES_with_{}_IotVar.json", [1, 41, 81, 121, 161, 201])


def mean(l):
    return sum(l) / len(l)


def setupPlot(results, f):
    X = []
    Y = []
    for r in results:
        X.append(r.iotvar_number)
        Y.append(f(r))
    plt.plot(X, Y)
    plt.xlim(0, max(X))
    plt.ylim(0, max(Y))



# Handler Execution time
plateforms = [om2m, mudebs, qodisco]
for plateform in plateforms:
    setupPlot(plateform, lambda r: mean(r.timeRecords)/1e6)
plt.legend(('OM2M', 'MuDEBS', 'QoDisco'), loc='upper right')
plt.title("Mean time records")
plt.xlabel("Number of IoTVariable")
plt.ylabel("Mean time (ms)")
plt.xlim(0, 201)
plt.ylim(0, 1e2)
plt.show()

# Used memory

for plateform in plateforms:
    setupPlot(plateform, lambda r: mean(r.memoryRecords)/1e6)
plt.legend(('OM2M', 'MuDEBS', 'QoDisco'), loc='upper right')
plt.title("IoTVar used memory")
plt.xlabel("Number of IoTVariable")
plt.ylabel("Used memory (MBytes)")
plt.xlim(0, 201)
plt.ylim(0, 5e2)
plt.show()

# setupPlot(qodisco, lambda r: mean(r.memoryRecords), "QoDisco")
# setupPlot(om2m, lambda r: mean(r.memoryRecords), "OM2M")
# setupPlot(mudebs, lambda r: mean(r.memoryRecords), "MUDEBS")
# plt.title("IoTVar used memory")
# plt.xlabel("Number of IoTVariable")
# plt.ylabel("Memory")
# plt.show()

