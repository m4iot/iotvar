Middleware to handle iot variables 

Bellow you will find commands to execute demos for the IoTVar middleware. There are several demos for the different supported platforms available.
All the platforms demos stays at the core/demo/<platform> folder. The commands for each platform are demonstrated and 
to test, choose either the textual client or the graphical client and run the simulator.

IMPORTANTO NOTE: If you want to launch the demo with map you have to run the class of the map demo directly from eclipse.*

* The map window is not showing with maven

Do not forget to launch the platform. For instructions refer to https://fusionforge.int-evry.fr/www/iotvar/doc/iotvar.html .

QoDisco PLATFORM

The commands bellow shows how to execute a demo for the IoTVar middleware using the QoDisco platform. We provide demos for pubsub, content pubsub and synchronous.

QoDisco pubsub

Textual client
cd $IOTVAR_ROOT
cd Sources/iothandler/core 
mvn -q exec:java -e -Dexec.mainClass=demo.qodisco.IoTVarDemoQoDiscoPubSub

Graphical client
cd $IOTVAR_ROOT
cd Sources/iothandler/core 
mvn -q exec:java -e -Dexec.mainClass=demo.qodisco.IoTVarDemoQoDiscoPubSubMap

Simulator
cd $IOTVAR_ROOT
cd Sources/iothandler/simulation
mvn -q exec:java -e -Dexec.mainClass=simulation.qodisco.QodiscoSimulator -Dexec.args="PublishingPeriod numberOfPublishing sensorNumber"
EXAMPLE: mvn -q exec:java -e -Dexec.mainClass=simulation.qodisco.QodiscoSimulator -Dexec.args="10 10 10"

QoDisco content pubsub

Textual client
cd $IOTVAR_ROOT
cd Sources/iothandler/core 
mvn -q exec:java -e -Dexec.mainClass=demo.qodisco.IoTVarDemoQoDiscoContentPubSub

Graphical client
cd $IOTVAR_ROOT
cd Sources/iothandler/core 
mvn -q exec:java -e -Dexec.mainClass=demo.qodisco.IoTVarDemoQoDiscoContentPubSubMap

Simulator
cd $IOTVAR_ROOT
cd Sources/iothandler/simulation
mvn -q exec:java -e -Dexec.mainClass=simulation.qodisco.QodiscoSimulator -Dexec.args="PublishingPeriod numberOfPublishing sensorNumber"
EXAMPLE: mvn -q exec:java -e -Dexec.mainClass=simulation.qodisco.QodiscoSimulator -Dexec.args="10 10 10"

QoDisco synchronous

Textual client
cd $IOTVAR_ROOT
cd Sources/iothandler/core 
mvn -q exec:java -e -Dexec.mainClass=demo.qodisco.IoTVarDemoQoDiscoSync

Graphical client
cd $IOTVAR_ROOT
cd Sources/iothandler/core 
mvn -q exec:java -e -Dexec.mainClass=demo.qodisco.IoTVarDemoQoDiscoSyncMap

Simulator
cd $IOTVAR_ROOT
cd Sources/iothandler/simulation
mvn -q exec:java -e -Dexec.mainClass=simulation.qodisco.QodiscoSimulator -Dexec.args="PublishingPeriod numberOfPublishing sensorNumber"
EXAMPLE: mvn -q exec:java -e -Dexec.mainClass=simulation.qodisco.QodiscoSimulator -Dexec.args="10 10 10"


MuDebs PLATFORM

The commands bellow shows how to execute a demo for the IoTVar middleware using the MuDebs platform. Only pubsub is supported

Textual client
cd $IOTVAR_ROOT
cd Sources/iothandler/core
mvn -q exec:java -e -Dexec.mainClass=demo.mudebs.IoTVarDemoMudebs

Graphical client
cd $IOTVAR_ROOT
cd Sources/iothandler/core
mvn -q exec:java -e -Dexec.mainClass=demo.mudebs.IoTVarDemoMudebsMap

cd $IOTVAR_ROOT
cd Sources/iothandler/simulation 
mvn -q exec:java -e -Dexec.mainClass=simulation.mudebs.MuDEBSSimulator  -Dexec.args="PublishingPeriod numberOfPublishing sensorNumber"
EXAMPLE: mvn -q exec:java -e -Dexec.mainClass=simulation.mudebs.MuDEBSSimulator -Dexec.args="10 10 10"


Om2m PLATFORM

The commands bellow shows how to execute a demo for the IoTVar middleware using the Om2m platform. Demos are for synchronous and pubsub.

Om2m synchronous

Textual client
cd $IOTVAR_ROOT
cd Sources/iothandler/core
mvn -q exec:java -e -Dexec.mainClass=demo.onem2m.IoTVarDemoOnem2mSync

Graphical client
cd $IOTVAR_ROOT
cd Sources/iothandler/core
mvn -q exec:java -e -Dexec.mainClass=demo.onem2m.IoTVarDemoOnem2mSyncMap

Simulator
cd $IOTVAR_ROOT
cd Sources/iothandler/simulation 
mvn -q exec:java -e -Dexec.mainClass=simulation.om2m.Om2mSimulator  -Dexec.args="PublishingPeriod numberOfPublishing sensorNumber"
EXAMPLE: mvn -q exec:java -e -Dexec.mainClass=simulation.om2m.Om2mSimulator -Dexec.args="10 10 10"

Om2m pubsub

Textual client
cd $IOTVAR_ROOT
cd Sources/iothandler/core
mvn -q exec:java -e -Dexec.mainClass=demo.onem2m.IoTVarDemoOnem2mPubSub

Graphical client
cd $IOTVAR_ROOT
cd Sources/iothandler/core
mvn -q exec:java -e -Dexec.mainClass=demo.onem2m.IoTVarDemoOnem2mPubSubMap

Simulator
cd $IOTVAR_ROOT
cd Sources/iothandler/simulation 
mvn -q exec:java -e -Dexec.mainClass=simulation.om2m.Om2mSimulator  -Dexec.args="PublishingPeriod numberOfPublishing sensorNumber"
EXAMPLE: mvn -q exec:java -e -Dexec.mainClass=simulation.om2m.Om2mSimulator -Dexec.args="10 10 10"

FIWARE PLATFORM

The commands bellow shows how to execute a demo for the IoTVar middleware using the OpenDDS platform. The provided demos contains each two declared variables,
one with filters and another without. The pubsub and synchronous methods are supported.

Fiware synchronous

Textual client
cd $IOTVAR_ROOT
cd Sources/iothandler/core
mvn -q exec:java -e -Dexec.mainClass=demo.fiware.IoTVarDemoFiwareSync

Graphical client
cd $IOTVAR_ROOT
cd Sources/iothandler/core
mvn -q exec:java -e -Dexec.mainClass=demo.fiware.IoTVarMapDemoFiwareSync

Simulator
cd $IOTVAR_ROOT
cd Sources/iothandler/simulation 
mvn -q exec:java -e -Dexec.mainClass=simulation.fiware.OrionSimulator  -Dexec.args="PublishingPeriod numberOfPublishing sensorNumber"
mvn -q exec:java -e -Dexec.mainClass=simulation.fiware.OrionSimulator  -Dexec.args="10 10 10"

Fiware pubsub

Textual client
Textual client
cd $IOTVAR_ROOT
cd Sources/iothandler/core
mvn -q exec:java -e -Dexec.mainClass=demo.fiware.IoTVarDemoFiwareAsync

Graphical client
cd $IOTVAR_ROOT
cd Sources/iothandler/core
mvn -q exec:java -e -Dexec.mainClass=demo.fiware.IoTVarMapDemoFiwareAsync

Simulator
cd $IOTVAR_ROOT
cd Sources/iothandler/simulation 
mvn -q exec:java -e -Dexec.mainClass=simulation.fiware.OrionSimulator  -Dexec.args="PublishingPeriod numberOfPublishing sensorNumber"
mvn -q exec:java -e -Dexec.mainClass=simulation.fiware.OrionSimulator  -Dexec.args="10 10 10"


