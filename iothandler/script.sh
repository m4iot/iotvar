#!/bin/bash
iotvarDir='/home/courtais/duong/Sources/MiddAppli/IOTVAR/Sources/iothandler'
powerAPIDir='/home/courtais/powerapi-cli'
strategy=$1
echo "executionTime;nbIoTVar;poolSize;strategy;freshnessDeviation;CPUTime;power" > test_results.txt


cd $homeDir/duong/Sources/MiddAppli/IOTVAR/Sources/iothandler/src/main/resources


for j in `seq 1 4`;
do
    cd $iotvarDir
    mvn clean compile
    mvn exec:java -Dexec.mainClass=demo.IoTVarDemo -Dexec.args="1 ${strategy} 10 5" &
    pid=$!
    cd $powerAPIDir
    sleep 1m
    ./bin/powerapi modules procfs-cpu-simple monitor --frequency 1000 --pids $pid --agg mean --file $iotvarDir/src/main/resources/power.txt duration 540
    sleep 1m
    cd $iotvarDir/src/main/resources
    old_IFS=$IFS
    IFS=$';'
    totalpower=0.0
    while IFS=';': read muid timestamp targets devices power
    do
	p=$(echo $power |grep -Eo [0-9]+[.]+[0-9]+)
	totalpower=$(echo $totalpower + $p | bc)
    done < $iotvarDir/src/main/resources/power.txt
    cd $iotvarDir/src/main/resources
    totalpower=$(echo $totalpower / 540.0 | bc)
    echo $totalpower >> test_results.txt
    cd $iotvarDir
    mvn clean compile
    mvn exec:java -Dexec.mainClass=demo.IoTVarDemo -Dexec.args="10 ${strategy} 10 5" &
    pid=$!
    cd $powerAPIDir
    sleep 1m
    ./bin/powerapi modules procfs-cpu-simple monitor --frequency 1000 --pids $pid --agg mean --file $iotvarDir/src/main/resources/power.txt duration 540
    sleep 1m
    cd $iotvarDir/src/main/resources
    old_IFS=$IFS
    IFS=$';'
    totalpower=0.0
    while IFS=';': read muid timestamp targets devices power
    do
	p=$(echo $power |grep -Eo [0-9]+[.]+[0-9]+)
	totalpower=$(echo $totalpower + $p | bc)
    done < $iotvarDir/src/main/resources/power.txt
    cd $iotvarDir/src/main/resources
    totalpower=$(echo $totalpower / 540.0 | bc)
    echo $totalpower >> test_results.txt
    
    cd $iotvarDir
    mvn exec:java -Dexec.mainClass=demo.IoTVarDemo -Dexec.args="50 ${strategy} 10 5" &
    pid=$!
    cd $powerAPIDir
    sleep 1m
    ./bin/powerapi modules procfs-cpu-simple monitor --frequency 1000 --pids $pid --agg mean --file $iotvarDir/src/main/resources/power.txt duration 540
    sleep 1m
    cd $iotvarDir/src/main/resources
    old_IFS=$IFS
    IFS=$';'
    totalpower=0.0
    while IFS=';': read muid timestamp targets devices power
    do
	p=$(echo $power |grep -Eo [0-9]+[.]+[0-9]+)
	totalpower=$(echo $totalpower + $p | bc)
    done < $iotvarDir/src/main/resources/power.txt
    cd $iotvarDir/src/main/resources
    totalpower=$(echo $totalpower / 540.0 | bc)
    echo $totalpower >> test_results.txt
    
    cd $iotvarDir
    mvn exec:java -Dexec.mainClass=demo.IoTVarDemo -Dexec.args="100 ${strategy} 10 5" &
    pid=$!
    cd $powerAPIDir
    sleep 1m
    ./bin/powerapi modules procfs-cpu-simple monitor --frequency 1000 --pids $pid --agg mean --file $iotvarDir/src/main/resources/power.txt duration 540
    sleep 1m
    cd $iotvarDir/src/main/resources
    old_IFS=$IFS
    IFS=$';'
    totalpower=0.0
    while IFS=';': read muid timestamp targets devices power
    do
	p=$(echo $power |grep -Eo [0-9]+[.]+[0-9]+)
	totalpower=$(echo $totalpower + $p | bc)
    done < $iotvarDir/src/main/resources/power.txt
    cd $iotvarDir/src/main/resources
    totalpower=$(echo $totalpower / 540.0 | bc)
    echo $totalpower >> test_results.txt
    
    cd $iotvarDir
    mvn exec:java -Dexec.mainClass=demo.IoTVarDemo -Dexec.args="150 ${strategy} 10 5" &
    pid=$!
    cd $powerAPIDir
    sleep 1m
    ./bin/powerapi modules procfs-cpu-simple monitor --frequency 1000 --pids $pid --agg mean --file $iotvarDir/src/main/resources/power.txt duration 540
    sleep 1m
    cd $iotvarDir/src/main/resources
    old_IFS=$IFS
    IFS=$';'
    totalpower=0.0
    while IFS=';': read muid timestamp targets devices power
    do
	p=$(echo $power |grep -Eo [0-9]+[.]+[0-9]+)
	totalpower=$(echo $totalpower + $p | bc)
    done < $iotvarDir/src/main/resources/power.txt
    cd $iotvarDir/src/main/resources
    totalpower=$(echo $totalpower / 540.0 | bc)
    echo $totalpower >> test_results.txt
    
    cd $iotvarDir
    mvn exec:java -Dexec.mainClass=demo.IoTVarDemo -Dexec.args="200 ${strategy} 10 5" &
    pid=$!
    cd $powerAPIDir
    sleep 1m
    ./bin/powerapi modules procfs-cpu-simple monitor --frequency 1000 --pids $pid --agg mean --file $iotvarDir/src/main/resources/power.txt duration 540
    sleep 1m
    cd $iotvarDir/src/main/resources
    old_IFS=$IFS
    IFS=$';'
   totalpower=0.0
    while IFS=';': read muid timestamp targets devices power
    do
	p=$(echo $power |grep -Eo [0-9]+[.]+[0-9]+)
	totalpower=$(echo $totalpower + $p | bc)
    done < $iotvarDir/src/main/resources/power.txt
    cd $iotvarDir/src/main/resources
    totalpower=$(echo $totalpower / 540.0 | bc)
    echo $totalpower >> test_results.txt
    
done

cd $iotvarDir/src/main/resources
fileName="test_results_${strategy}_$(date "+%Y-%m-%d")"
cp test_results.txt $fileName.txt

exit
