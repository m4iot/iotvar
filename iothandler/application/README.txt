To run the performance testing make sure you have the target platform running.

To run the performance testing each platform requires a set of parameters.

Mandatory:

SENSOR_NUMBER: Number of sensors to be declared
TEST_TIME: Time for the testing in seconds (Total test time is 60 seconds (Warm up phase) + the one declared here)
FILE_NAME: Name of the file where the data is going to be saved
URL_OF_PLATFORM: Put the full url of the platform protocol:ip:port/route
 - This is different for Fiware. Put only ip:port.
HANDLER_TYPE: sync or pubsub
REFRESH_TIME: Used for paltforms that support refresh_time filters
NOTIFICATION_URL: Used only by Fiware. This is the full URL (protocol:ip:port) of the machine trying to get data from Fiware.
                  The notifications send from Fiware will arrive at this ip and port.

For all the platforms except Fiware use as follows:

mvn exec:java -Dexec.mainClass=iotvarPerformance.om2m.IoTvarExampleOM2M -Dexec.args="SENSOR_NUMBER TEST_TIME FILE_NAME URL_OF_PLATFORM HANDLER_TYPE REFRESH_TIME"
mvn exec:java -Dexec.mainClass=iotvarPerformance.mudebs.IoTvarMuDEBSExample -Dexec.args="SENSOR_NUMBER TEST_TIME FILE_NAME URL_OF_PLATFORM HANDLER_TYPE REFRESH_TIME"
mvn exec:java -Dexec.mainClass=iotvarPerformance.QoDisco.IoTvarQoDiscoExample -Dexec.args="SENSOR_NUMBER TEST_TIME FILE_NAME URL_OF_PLATFORM HANDLER_TYPE REFRESH_TIME"

For Fiware:

mvn exec:java -Dexec.mainClass=iotvarPerformance.QoDisco.IoTvarQoDiscoExample -Dexec.args="SENSOR_NUMBER TEST_TIME FILE_NAME URL_OF_PLATFORM HANDLER_TYPE REFRESH_TIME NOTIFICATION_URL"
