package performance;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class PerformanceAspectFiware {
	
	private ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
	private static final Logger LOGGER = LogManager.getLogger(PerformanceAspectFiware.class);
	
	@Around("execution(* iotvar.platform.fiware.OrionSyncHandler.run())")
	public void fiwareSyncPerformance(ProceedingJoinPoint joinPoint) {
		LOGGER.debug("Begin of OrionSyncHandler aspect (FIWARE).");
		Long startCpuTime = threadMXBean.getCurrentThreadCpuTime();
        long startTime = System.nanoTime();
        try {
			joinPoint.proceed();
		} catch (Throwable e) {
			LOGGER.error(e.getLocalizedMessage());
			e.printStackTrace();
		}
        long endTime = System.nanoTime();
        Long delta = threadMXBean.getCurrentThreadCpuTime() - startCpuTime;
        PerformanceStore.addTimeRecord(delta);
        long duration = (endTime - startTime);
        PerformanceStore.addProcessRecord(duration);
        PerformanceStore.addMemoryRecord(PerformanceStore.getReallyUsedMemory());
        LOGGER.debug("End of OrionSyncHandler aspect (FIWARE).");
	}
	
	@Around("execution(* iotvar.platform.fiware.OrionServerHttpHandler.handleRequest(..))")
	public void fiwareAsyncPerformance(ProceedingJoinPoint joinPoint) {
		LOGGER.debug("Begin of OrionServerHttpHandler aspect (FIWARE).");
		Long startCpuTime = threadMXBean.getCurrentThreadCpuTime();
        long startTime = System.nanoTime();
        try {
			joinPoint.proceed();
		} catch (Throwable e) {
			LOGGER.error(e.getLocalizedMessage());
			e.printStackTrace();
		}
        long endTime = System.nanoTime();
        Long delta = threadMXBean.getCurrentThreadCpuTime() - startCpuTime;
        PerformanceStore.addTimeRecord(delta);
        long duration = (endTime - startTime);
        PerformanceStore.addProcessRecord(duration);
        PerformanceStore.addMemoryRecord(PerformanceStore.getReallyUsedMemory());
        LOGGER.debug("End of OrionServerHttpHandler aspect (FIWARE).");
	}

}
