/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/

package performance;

public class Record {
	String plateformePattern;
	int iotvarNumber;
	long CPUtime;
	String CPUtimeUnity = "ns";
	long MemUsage;
	String MemUsageUnity = "byte";
	String FreqUnity = "s";
	int freq;
	int timePub;
	int numSensor;
	int freshnessFrequency;
	long numberOfmsg;
	long msgsRecSize;
	double error_rate;
	double process;
	double bat;

	public Record(String plateformePattern, int iotvarNumber, int freshnessFrequency, long cPUtime, long memUsage,
			int freq, int timePub, int numSensor, long numberOfmsg, long msgsRecSize, double error_rate, double process,
			double bat) {
		this.iotvarNumber = iotvarNumber;
		CPUtime = cPUtime;
		MemUsage = memUsage;
		this.plateformePattern = plateformePattern;
		this.freq = freq;
		this.timePub = timePub;
		this.numSensor = numSensor;
		this.freshnessFrequency = freshnessFrequency;
		this.numberOfmsg = numberOfmsg;
		this.msgsRecSize = msgsRecSize;
		this.error_rate = error_rate;
		this.process = process;
		this.bat = bat;

	}

	@Override
	public String toString() {
		return "{\"plateformePattern\":" + "\"" + plateformePattern + "\"" + ", \"iotvarNumber\":" + iotvarNumber
				+ ", \"CPUtime\":" + CPUtime + ", \"CPUtimeUnit\":" + "\"" + CPUtimeUnity + "\"" + ", \"MemUsage\":"
				+ MemUsage + ", \"MemUsageUnit\":" + "\"" + MemUsageUnity + "\"" + ", \"SensorsNumber\":" + numSensor
				+ ", \"PublishingPeriod\":" + freq

				+ ", \"freshnessPeriod\":" + freshnessFrequency

				+ ", \"PeriodUnit\":" + "\"" + FreqUnity + "\"" + ", \"numberOfPublishing\":" + timePub
				+ ", \"numberOfmsg\":" + numberOfmsg + ", \"msgsRecSizeBytes\":" + msgsRecSize +

				", \"errorRatePourcentage\":" + error_rate + ", \"process\":" + process + ", \"battery\":" + bat + "}";
	}

}
