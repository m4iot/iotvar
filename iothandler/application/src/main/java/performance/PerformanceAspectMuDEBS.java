package performance;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class PerformanceAspectMuDEBS {
	
	private ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
	private static final Logger LOGGER = LogManager.getLogger(PerformanceAspectMuDEBS.class);
	
	@Around("execution(* iotvar.platform.mudebs.ContextMuDEBSConsumer.consume(*))")
	public void mudebsAsyncPerformance(ProceedingJoinPoint joinPoint) {
		LOGGER.debug("Begin of ContextMuDEBSConsumer aspect (MuDEBS).");
		Long startCpuTime = threadMXBean.getCurrentThreadCpuTime();
        long startTime = System.nanoTime();
        try {
			joinPoint.proceed();
		} catch (Throwable e) {
			LOGGER.error(e.getLocalizedMessage());
			e.printStackTrace();
		}
        long endTime = System.nanoTime();
        Long delta = threadMXBean.getCurrentThreadCpuTime() - startCpuTime;
        PerformanceStore.addTimeRecord(delta);
        long duration = (endTime - startTime);
        PerformanceStore.addProcessRecord(duration);
        PerformanceStore.addMemoryRecord(PerformanceStore.getReallyUsedMemory());
        LOGGER.debug("End of ContextMuDEBSConsumer aspect (MuDebs).");
	}

}
