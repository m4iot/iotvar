package performance;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class PerformanceAspectQoDisco {
	
	private ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
	private static final Logger LOGGER = LogManager.getLogger(PerformanceAspectQoDisco.class);
	
	@Around("execution(* iotvar.platform.qodisco.SynchronousCallHandler.run())")
	public void qoDiscoSyncPerformance(ProceedingJoinPoint joinPoint) {
		LOGGER.debug("Begin of SynchronousCallHandler aspect (QoDisco).");
		Long startCpuTime = threadMXBean.getCurrentThreadCpuTime();
        long startTime = System.nanoTime();
        try {
			joinPoint.proceed();
		} catch (Throwable e) {
			LOGGER.error(e.getLocalizedMessage());
			e.printStackTrace();
		}
        long endTime = System.nanoTime();
        Long delta = threadMXBean.getCurrentThreadCpuTime() - startCpuTime;
        PerformanceStore.addTimeRecord(delta);
        long duration = (endTime - startTime);
        PerformanceStore.addProcessRecord(duration);
        PerformanceStore.addMemoryRecord(PerformanceStore.getReallyUsedMemory());
        LOGGER.debug("End of SynchronousCallHandler aspect (QoDisco).");
	}
	
	@Around("execution(* iotvar.platform.IoTVarMqttClient.messageArrived(..))")
	public void qoDiscoAsyncPerformance(ProceedingJoinPoint joinPoint) {
		LOGGER.debug("Begin of messageArrived aspect (QoDisco).");
		Long startCpuTime = threadMXBean.getCurrentThreadCpuTime();
        long startTime = System.nanoTime();
        try {
			joinPoint.proceed();
		} catch (Throwable e) {
			LOGGER.error(e.getLocalizedMessage());
			e.printStackTrace();
		}
        long endTime = System.nanoTime();
        Long delta = threadMXBean.getCurrentThreadCpuTime() - startCpuTime;
        PerformanceStore.addTimeRecord(delta);
        long duration = (endTime - startTime);
        PerformanceStore.addProcessRecord(duration);
        PerformanceStore.addMemoryRecord(PerformanceStore.getReallyUsedMemory());
        LOGGER.debug("End of messageArrived aspect (QoDisco).");
	}

}
