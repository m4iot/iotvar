package performance;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class PerformanceAspectOm2m {
	
	private ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
	private static final Logger LOGGER = LogManager.getLogger(PerformanceAspectOm2m.class);
	
	@Around("execution(* iotvar.platform.om2m.SynchronousCallHandlerOM2M.run())")
	public void fiwareSyncPerformance(ProceedingJoinPoint joinPoint) {
		LOGGER.debug("Begin of SynchronousCallHandlerOM2M aspect (OM2M).");
		Long startCpuTime = threadMXBean.getCurrentThreadCpuTime();
        long startTime = System.nanoTime();
        try {
			joinPoint.proceed();
		} catch (Throwable e) {
			LOGGER.error(e.getLocalizedMessage());
			e.printStackTrace();
		}
        long endTime = System.nanoTime();
        Long delta = threadMXBean.getCurrentThreadCpuTime() - startCpuTime;
        PerformanceStore.addTimeRecord(delta);
        long duration = (endTime - startTime);
        PerformanceStore.addProcessRecord(duration);
        PerformanceStore.addMemoryRecord(PerformanceStore.getReallyUsedMemory());
        LOGGER.debug("End of SynchronousCallHandlerOM2M aspect (OM2M).");
	}
	
	@Around("execution(* iotvar.platform.IoTVarMqttClientOm2m.messageArrived(..))")
	public void fiwareAsyncPerformance(ProceedingJoinPoint joinPoint) {
		LOGGER.debug("Begin of IoTVarMqttClientOm2m aspect (OM2M).");
		Long startCpuTime = threadMXBean.getCurrentThreadCpuTime();
        long startTime = System.nanoTime();
        try {
			joinPoint.proceed();
		} catch (Throwable e) {
			LOGGER.error(e.getLocalizedMessage());
			e.printStackTrace();
		}
        long endTime = System.nanoTime();
        Long delta = threadMXBean.getCurrentThreadCpuTime() - startCpuTime;
        PerformanceStore.addTimeRecord(delta);
        long duration = (endTime - startTime);
        PerformanceStore.addProcessRecord(duration);
        PerformanceStore.addMemoryRecord(PerformanceStore.getReallyUsedMemory());
        LOGGER.debug("End of IoTVarMqttClientOm2m aspect (OM2M).");
	}

}
