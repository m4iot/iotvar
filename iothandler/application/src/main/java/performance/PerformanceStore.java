/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/

package performance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.ToDoubleFunction;
import java.util.function.ToLongFunction;

public class PerformanceStore {
	
	public static String filename = "DEFAULT.txt";
	public static final ArrayList<Long> timeResults = new ArrayList<Long>();
	public static final ArrayList<Long> processResults = new ArrayList<Long>();
	private static final SortedSet<Long> memoryUsage = new TreeSet<Long>();
	public static long startTime;
	public static boolean startComputing = false;
	public static Double beginBattery;
	
	public static void initializeBeginBattery(Double d) {
		if (startComputing) {
			beginBattery = d;
		}
	}

	public static Double updatePower() throws IOException {
		ProcessBuilder builder = new ProcessBuilder("sudo", "systemctl", "restart", "upower");
		builder.redirectErrorStream(true);
		Process process = builder.start();
		InputStream is = process.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));

		String line = null;
		while ((line = reader.readLine()) != null) {
			// Testing
		}

		return 0.0;
	}

	public static Double getBatteryPower() throws IOException {
		ProcessBuilder builder = new ProcessBuilder("upower", "-i", "/org/freedesktop/UPower/devices/battery_BAT0");
		builder.redirectErrorStream(true);
		Process process = builder.start();
		InputStream is = process.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));

		String line = null;
		while ((line = reader.readLine()) != null) {
			if (line.contains("energy:")) {
				String[] energy = line.split(" ");
				// [temp.length - 2] = energy
				// [temp.length - 1] = unity
				return Double.parseDouble(energy[energy.length - 2].replace(",", "."));
			}
		}

		return 0.0;
	}

	public static Double getBatteryVoltage() throws IOException {
		ProcessBuilder builder = new ProcessBuilder("upower", "-i", "/org/freedesktop/UPower/devices/battery_BAT0");
		builder.redirectErrorStream(true);
		Process process = builder.start();
		InputStream is = process.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));

		String line = null;
		while ((line = reader.readLine()) != null) {
			if (line.contains("voltage:")) {
				String[] energy = line.split(" ");
				// [temp.length - 2] = energy
				// [temp.length - 1] = unity
				return Double.parseDouble(energy[energy.length - 2].replace(",", "."));
			}
		}

		return 0.0;
	}

	public static void addProcessRecord(long process) {
		if (startComputing) {
			processResults.add(Long.valueOf(process));
		}
	}

	public static void addMemoryRecord(Long memory) {
		if (startComputing) {
			memoryUsage.add(memory);
			try {
				beginBattery = PerformanceStore.getBatteryPower();
			} catch (IOException e) {
				System.out.println("It was not possible get retrieve the battery power.");
			}
		}
	}
	
	public static void addTimeRecord(Long d) {
		if (startComputing) {
			timeResults.add(d);
		}
	}

	public static void warmUp(int time) throws InterruptedException {
		Thread.sleep(time * 1000); 
		startComputing = true; 
	}

	public static boolean isStartComputing() {
		return startComputing;
	}

	public static void setStartComputing(boolean startComputing) {
		PerformanceStore.startComputing = startComputing;
	}

	public static double getUsedEnergy(String type) throws IOException {
		if (type.equals("BATTERY")) {
			double batteryCom = beginBattery.doubleValue() - PerformanceStore.getBatteryPower().doubleValue();
			double batteryComMaH = 1000 * batteryCom / PerformanceStore.getBatteryVoltage().doubleValue();
			return batteryComMaH;
		} else if (type.equals("WATTMETER")) {
			return 0.0;
		}
		return 0.0;
	}

	private static long getCurrentlyUsedMemory() {
		return ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed()
				+ ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage().getUsed();
	}

	private static long getGcCount() {
		long sum = 0;
		for (GarbageCollectorMXBean b : ManagementFactory.getGarbageCollectorMXBeans()) {
			long count = b.getCollectionCount();
			if (count != -1) {
				sum += count;
			}
		}
		return sum;
	}

	public static long getReallyUsedMemory() {
		long before = getGcCount();
		System.gc();
		while (getGcCount() == before);
		return getCurrentlyUsedMemory();
	}

	public static void write(String plateformePattern, int iotVarNumber, int freshnessFrequency, int freq, int timePub,
			int numSensor, long numberOfmsg, long msgsRecSize, double error_rate, String energyType) {
		try {
			startComputing = false;
			// Writ
			PrintWriter writer;

			Long totalCpuTime = timeResults.stream().mapToLong(new ToLongFunction<Long>() {
				public long applyAsLong(Long num) {
					return num.longValue();
				}
			}).sum();

			Long totalMemory = memoryUsage.last();

			double processTime = processResults.stream().mapToDouble(new ToDoubleFunction<Long>() { 
				public double applyAsDouble(Long value) {
					return value.doubleValue();
				}
			}).average().getAsDouble();

			double energy = getUsedEnergy(energyType);

			Record record = new Record(plateformePattern, iotVarNumber, freshnessFrequency, totalCpuTime, totalMemory,
					freq, timePub, numSensor, numberOfmsg, msgsRecSize, error_rate, processTime, energy);

			File f = new File(filename);

			if (f.exists() && !f.isDirectory()) {
				writer = new PrintWriter(new FileOutputStream(f, true));
				String content = new Scanner(new File(filename)).useDelimiter("\\Z").next();
				String withoutLastCharacter = content.substring(0, content.length() - 1);
				writer.close();
				PrintWriter pw = new PrintWriter(filename);
				pw.write(withoutLastCharacter);
				pw.append("," + record.toString());
				pw.append("]");
				pw.close();
			} else {
				writer = new PrintWriter(filename, "UTF-8");
				writer.println("[");
				writer.println(record.toString());
				writer.println("]");
				writer.close();
			}
		} catch (IOException ex) {
			System.out.println("Writer ERROR: " + ex.getMessage());
			System.exit(0);
		}

	}
}
