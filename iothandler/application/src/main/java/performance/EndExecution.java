/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/

package performance;

import java.util.TimerTask;

import iotvar.IoTVariableCommon;

public class EndExecution extends TimerTask{
	
	String plateformePattern;
	int iotVarNumber;
	int freshnessFrequency;
	int freq;
	int timePub;
	int numSensor;
	long numberOfmsg;
	long msgsRecSize;
	double error_rate;
	
	public EndExecution(String plateformePattern, int iotVarNumber, int freshnessFrequency, int freq, int timePub,
			int numSensor, long numberOfmsg, long msgsRecSize, double error_rate) {
		this.plateformePattern = plateformePattern;
		this.iotVarNumber = iotVarNumber;
		this.freshnessFrequency = freshnessFrequency;
		this.freq = freq;
		this.timePub = timePub;
		this.numSensor = numSensor;
		this.numberOfmsg = numberOfmsg;
		this.msgsRecSize = msgsRecSize;
		this.error_rate = error_rate;
	}

	@Override
	public void run() {
		System.out.println("Write and exit.");
		PerformanceStore.write("fiware " + plateformePattern, iotVarNumber, freshnessFrequency,
				freq, timePub, numSensor, IoTVariableCommon.getNumberofmsg(),
				IoTVariableCommon.getNumberofmsg() * 350, 0, "BATTERY");
		TimedExit.exit(0, 5000);
	}

}
