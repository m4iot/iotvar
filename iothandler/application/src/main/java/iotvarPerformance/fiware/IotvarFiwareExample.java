/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/

package iotvarPerformance.fiware;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import iotvar.IoTVariableCommon;
import iotvar.IoTVariableFiware;
import iotvar.basics.FilterPublishSubscribeMethod;
import iotvar.basics.Freshness;
import iotvar.basics.HandlerStrategy;
import iotvar.basics.Location;
import iotvar.platform.fiware.Orion;
import performance.EndExecution;
import performance.PerformanceStore;
import performance.TimedExit;

public class IotvarFiwareExample {

	private static final int WARM_UP_TIME = 10;

	/**
	 * 
	 * @param args [0] : number of IoTvar | time of the test (in seconds)
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public static void main(String[] args) throws InterruptedException, IOException {

		/* parse the main arguments */
		if (args.length < 7) {
			throw new IllegalArgumentException(
					"Invalid argument. Need to specify the number of IoTvar, the running time of the test (in seconds), "
							+ "the output file ,the FIWARE address, the strategy (sync or pubsub),"
							+ "the freshness frequency, the notification address (for PUBSUB)");
		}

		/* Disable loggers */
//		final Logger logger = LogManager.getLogger(IotvarFiwareExample.class);
//		logger.setLevel(Level.OFF);
		disableLoggers();

		// Try to get the arguments
		int numberOfIoTvar = Integer.parseInt(args[0]);
		int timeofTest = Integer.parseInt(args[1]);
		String performanceFile = args[2];
		String fiwareAddress = args[3];
		String strategy = args[4];
		int freshnessFrequency = Integer.parseInt(args[5]);
		String notificationUrl = args[6];
		int publishingFrequency = 0;
		int numberOfPublishing = 0;
		int numberOfSensor = 0;

		if (args.length == 10) {
			publishingFrequency = Integer.parseInt(args[7]);
			numberOfPublishing = Integer.parseInt(args[8]);
			numberOfSensor = Integer.parseInt(args[9]);
		}

		// Check handler strategy
		HandlerStrategy handlerStrategy;

		if (strategy.equals("pubsub")) {
			handlerStrategy = HandlerStrategy.PUBSUB;
		} else if (strategy.equals("green")) {
			handlerStrategy = HandlerStrategy.PUBSUB;
		} else {
			handlerStrategy = HandlerStrategy.SYNC;
		}

		/* initialize filters and display */
		FiwareTextDisplay<String> text = new FiwareTextDisplay<String>(timeofTest);
		List<FilterPublishSubscribeMethod> filters = new ArrayList<FilterPublishSubscribeMethod>();
		ArrayList<IoTVariableFiware<String>> iotvar = new ArrayList<IoTVariableFiware<String>>();
		filters.add(new FilterPublishSubscribeMethod("freshness", 1000 * freshnessFrequency));

		PerformanceStore.filename = performanceFile;

		/* create the iotVariables */
		for (int i = 0; i < numberOfIoTvar; i++) {
			iotvar.add(i,
					new IoTVariableFiware<String>("sensor" + (i + 1), "Sensor", "temperature",
							new Location("location", 48.6223426, 2.4404356, 100.0),
							new Freshness(freshnessFrequency, TimeUnit.SECONDS), 10, (String) null, new Orion("http",
									fiwareAddress.split(":")[0], fiwareAddress.split(":")[1], notificationUrl),
							handlerStrategy));

			iotvar.get(i).registerIoTListenerP(text);
		}

		System.out.println("All FIWARE IoTvar created");

		// Test warm up (Default 60 seconds)
		System.out.println("Beggin warmup");
		PerformanceStore.warmUp(WARM_UP_TIME);
		System.out.println("End warmup");

		// Declare the timed exit to write the test values
		new TimedExit(timeofTest,
				new EndExecution("fiware " + handlerStrategy, numberOfIoTvar, freshnessFrequency, publishingFrequency,
						numberOfPublishing, numberOfSensor, IoTVariableCommon.getNumberofmsg(),
						IoTVariableCommon.getNumberofmsg() * 350, 0));

	}

	private static void disableLoggers() {
		List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
		loggers.add(LogManager.getRootLogger());
		for (Logger logger : loggers) {
			logger.setLevel(Level.OFF);
		}
	}
}
