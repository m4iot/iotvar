/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/

package iotvarPerformance.mudebs;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import iotvar.IoTVariableCommon;
import iotvar.IoTVariableMuDEBS;
import iotvar.basics.Freshness;
import iotvar.basics.Location;
import iotvarPerformance.PerfTextDisplay;
import mudebs.common.MuDEBSException;
import performance.PerformanceStore;

public class IotvarMuDEBSExample {
	private static final Logger logger = LogManager.getLogger(IotvarMuDEBSExample.class);

	
    public static String applicationSubFilter1 = "function evaluate(doc) {"
            + "load(\"nashorn:mozilla_compat.js\");"
            + "importPackage(javax.xml.xpath);"
            + "var xpath = XPathFactory.newInstance().newXPath();"
            + "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
            + "if (!n1.getTextContent().equalsIgnoreCase('Temperature')) return false;"
            + "xpath = XPathFactory.newInstance().newXPath();"
            + "return true;}";;
            
    public static String applicationSubFilter2 = "function evaluate(doc) {"
                    + "load(\"nashorn:mozilla_compat.js\");"
                    + "importPackage(javax.xml.xpath);"
                    + "var xpath = XPathFactory.newInstance().newXPath();"
                    + "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
                    + "if (!n1.getTextContent().equalsIgnoreCase('Humidity')) return false;"
                    + "xpath = XPathFactory.newInstance().newXPath();"
                    + "return true;}";;
	
     public static long exact_value;
 	
	/**
	 * 
	 * @param args should define mudebs'URI and broker's address.
	 * @throws MuDEBSException
	 * @throws UnsupportedEncodingException 
	 * @throws FileNotFoundException 
	 * @throws InterruptedException 
	 */
               
	public static void main(String[] args) throws MuDEBSException, FileNotFoundException, UnsupportedEncodingException, InterruptedException {
		
		if (args.length < 3) {
			throw new IllegalArgumentException("Invalid argument. Need to specify the number of IoTvar, the running time of the test (in seconds), the URI and broker address \n "
					+ "For example : \"100 30 out.json --uri mudebs://localhost:2102/ApplicationHelloWorld --broker mudebs://localhost:2000/BrokerA\"");
		}
		int numberOfIoTvar = Integer.parseInt(args[0]);
		int timeofTest = Integer.parseInt(args[1]);
		String performanceFile = args[2];
		int publishingFrequency = 0;
		int numberOfPublishing = 0;
		int numberOfSensor = 0;
		if(args.length==10) {
			publishingFrequency = Integer.parseInt(args[7]);
			numberOfPublishing = Integer.parseInt(args[8]);
			numberOfSensor = Integer.parseInt(args[9]);}

		ArrayList<String> applicationArguments=new ArrayList<String>();
		for (int i = 3; i<args.length; i++) {
			applicationArguments.add(args[i]);
		}
		String[] MuDEBSargs  = new String[applicationArguments.size()];
		applicationArguments.toArray(MuDEBSargs);

		PerformanceStore.filename = performanceFile;		
		PerfTextDisplay<Double> Display = new PerfTextDisplay<>(timeofTest);
		ArrayList<IoTVariableMuDEBS<Double>>iotvar = new ArrayList<IoTVariableMuDEBS<Double>>();
		for (int i=0; i<numberOfIoTvar; i++) {
			
				/* IoTvar for Temperature next to the Tour Eiffel */
				iotvar.add(i, new IoTVariableMuDEBS<Double>("Temperature", new Location("Tour_Eiffel01", 48.858373, 2.294572, 0.0),
	                new Freshness(10, TimeUnit.SECONDS), 10, null, MuDEBSargs, applicationSubFilter1));
			
			iotvar.get(i).registerIoTListener(Display);
		}
		System.out.println("[INFO] all IoTvarMuDEBS created //////");
		PerformanceStore.warmUp(60);

		final long startingTime = System.currentTimeMillis();
		IoTVariableCommon.setCompute(true);
		
		if(numberOfIoTvar == 0) {
			PerformanceStore.addMemoryRecord((long) 0);
		}
 		while(true) {
			if(System.currentTimeMillis() - startingTime > timeofTest*1000) {
				long exact_value=((numberOfSensor)*numberOfIoTvar*((timeofTest-publishingFrequency)/publishingFrequency));
				double error_rate = ((double)((double)(exact_value - IoTVariableCommon.getNumberofmsg())/(double)exact_value))*100 ;
				if(error_rate<10) 
					error_rate=0;
 				PerformanceStore.write("muDEBS pubsub",numberOfIoTvar,0,publishingFrequency,numberOfPublishing,numberOfSensor,
						IoTVariableCommon.getNumberofmsg(),IoTVariableCommon.getNumberofmsg()*855,error_rate, "BATTERY"
						
 						
 						);
				logger.info("error rat "+error_rate  );
				logger.info("msg number "+IoTVariableCommon.getNumberofmsg()  );

 				IoTVariableCommon.setNumberofmsg(0);
				IoTVariableCommon.setCompute(false);
				logger.info("Tests finished...");
				PerformanceStore.setStartComputing(false);
				logger.info("exact , value " +exact_value );

				System.exit(0);

			}
		}
        
	}

}
