/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/

package iotvarPerformance;

import org.apache.log4j.LogManager;

import org.apache.log4j.Logger;
import iotvar.IoTVarObserver;
import iotvar.basics.Observation;
import iotvar.exception.IoTVarNotFoundException;
import performance.PerformanceStore;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;


/**
 * TextDisplay prints the information of the last observation
 *
 * @author dylan
 * @see    iotvar.IoTVarObserver
 */

public class PerfTextDisplay<T> implements IoTVarObserver<T> {

	
	private int testingTime; 	//in seconds
	private long startingTime;	//in milliseconds
	
	public PerfTextDisplay(int testingTime) {
		this.testingTime=testingTime;
		this.startingTime = System.currentTimeMillis();
	}
	
	private static final Logger logger = LogManager.getLogger(PerfTextDisplay.class);
	
	public void onUpdate(Observation<T> newObservation) {
		
		/* check if the test is run out of time */
	/*	if (System.currentTimeMillis()-startingTime>(testingTime)) {
			try {
				PerformanceStore.write();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			logger.info("Tests finished...");
			System.exit(0);
		}*/
		if (newObservation == null)
			updateIssue();
		//logger.info(Thread.currentThread().getName()+ " " + newObservation);
		
	}

	public void updateIssue() {
		throw new IoTVarNotFoundException("IoT variable searched doesn't exist");
	}

}
