/*******************************************************************************
 *
 * This file is part of the IoTvar middleware.
 *
 * Copyright (C) 2017-2019 Télécom SudParis
 *
 * The IoTvar software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IoTvar software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the IoTvar middleware. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributor(s):
 * Chantal Taconet chantal.taconet@telecom-sudparis.eu
 * Maxence Vandoolaeghe (QoDisco platform/synchronous mode)
 * Rui Xiong (QoDisco platform/synchronous mode)
 * Clément Courtais (QoDisco platform publish subscribe, OM2M synchronous strategy)
 * Thomas Coutelou (refactoring, MuDebs platform)
 * Charles Caporali (refactoring, MuDebs platform)
 * Houssem Touansi (OM2M platform, publish/subscribe strategy)
 * Jing YE (OpenDDS platform)
 * Idriss El Aichaoui (OpenDDS platform)
 * Pedro Victor Borges (FIWARE platform)
 *
 *******************************************************************************/

package iotvarPerformance.QoDisco;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import iotvar.IoTVariableCommon;
import iotvar.IoTVariableQoDisco;
import iotvar.basics.FilterPublishSubscribeMethod;
import iotvar.basics.Freshness;
import iotvar.basics.HandlerStrategy;
import iotvar.basics.Location;
import iotvarPerformance.PerfTextDisplay;
import iotvarPerformance.mudebs.IotvarMuDEBSExample;
import performance.PerformanceStore;

public class IoTvarQoDiscoExample {

	
	/**
	 * 
	 * @param args [0] : number of IoTvar | time of the test (in seconds)
	 * @throws InterruptedException
	 * @throws UnsupportedEncodingException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws InterruptedException, FileNotFoundException, UnsupportedEncodingException {

		/* parse the main arguments*/
		if (args.length < 6) {
			throw new IllegalArgumentException(
					"Invalid argument. Need to specify the number of IoTvar, the running time of the test (in seconds), the output file, the qodisco address and the strategy , the freshness frequency");
		}
	    final Logger logger = LogManager.getLogger(IoTvarQoDiscoExample.class);

		int numberOfIoTvar = Integer.parseInt(args[0]);
		int timeofTest = Integer.parseInt(args[1]);
		String performanceFile = args[2];
		String qodiscoAddress = args[3];
		String strategy = args[4];
		int freshnessFrequency = Integer.parseInt(args[5]);
		int publishingFrequency = 0;
		int numberOfPublishing = 0;
		int numberOfSensor = 0;
		if(args.length==9) {
			publishingFrequency = Integer.parseInt(args[6]);
			numberOfPublishing = Integer.parseInt(args[7]);
			numberOfSensor = Integer.parseInt(args[8]);}
		HandlerStrategy handlerStrategy;
		if (strategy.equals("pubsub")){
            handlerStrategy = HandlerStrategy.PUBSUB;
        } else if (strategy.equals("pubsub-filter")){
            handlerStrategy = HandlerStrategy.CONTENTPUBSUB;
        } else {
            handlerStrategy = HandlerStrategy.SYNC;
        }
 		/* initialize filters and display*/
		PerfTextDisplay<Float> text = new PerfTextDisplay<Float>(timeofTest);
		List<FilterPublishSubscribeMethod> filters = new ArrayList<FilterPublishSubscribeMethod>();
		ArrayList<IoTVariableQoDisco<Float>>iotvar = new ArrayList<IoTVariableQoDisco<Float>>();
		//filters.add(new FilterPublishSubscribeMethod("freshness", 100000000));
		filters.add(new FilterPublishSubscribeMethod("freshness", 1000*freshnessFrequency));


		PerformanceStore.filename = performanceFile;
		
		 if (strategy.equals("pubsub")){
             filters=null;
     }


		/* create the iotVariables */
		for (int i=0;i<numberOfIoTvar;i++) {
			
				/* IoTvar for Temperature next to the Tour Eiffel */
				iotvar.add(i,new IoTVariableQoDisco<Float>("Temperature", qodiscoAddress, "admin", "admin", new Location("EiffelTower", 48.8, 2.3, 9.0),
				new Freshness(freshnessFrequency, TimeUnit.SECONDS), 10, filters, handlerStrategy));
			
			iotvar.get(i).registerIoTListener(text);
		}
		
		PerformanceStore.warmUp(60);
		final long startingTime = System.currentTimeMillis();
		IoTVariableCommon.setCompute(true);
		if(numberOfIoTvar == 0) {
			PerformanceStore.addMemoryRecord((long) 0);
		}
 		while(true) {
 			if(System.currentTimeMillis() - startingTime > timeofTest*1000) {
 				
				if(handlerStrategy.equals(HandlerStrategy.SYNC)) {
					PerformanceStore.write("Qodisco "+handlerStrategy,numberOfIoTvar,freshnessFrequency,publishingFrequency,numberOfPublishing,numberOfSensor,IoTVariableCommon.getNumberofmsg(),IoTVariableCommon.getNumberofmsg()*725,0, "BATTERY");

				}
				else if (handlerStrategy.equals(HandlerStrategy.CONTENTPUBSUB)) {
					long exact_value=((numberOfSensor)*numberOfIoTvar*((timeofTest-publishingFrequency)/freshnessFrequency));
					double error_rate = ((double)((double)(exact_value - IoTVariableCommon.getNumberofmsg())/(double)exact_value))*100 ;
					if(error_rate<10) 
						error_rate=0;
					PerformanceStore.write("Qodisco "+handlerStrategy,numberOfIoTvar,freshnessFrequency,publishingFrequency,numberOfPublishing,numberOfSensor,IoTVariableCommon.getNumberofmsg(),IoTVariableCommon.getNumberofmsg()*725,error_rate, "BATTERY");

				}
				else {
					long exact_value=((numberOfSensor)*numberOfIoTvar*((timeofTest-publishingFrequency)/publishingFrequency));
					double error_rate = ((double)((double)(exact_value - IoTVariableCommon.getNumberofmsg())/(double)exact_value))*100 ;
					if(error_rate<10) 
						error_rate=0;
					PerformanceStore.write("Qodisco "+handlerStrategy,numberOfIoTvar,freshnessFrequency,publishingFrequency,numberOfPublishing,numberOfSensor,IoTVariableCommon.getNumberofmsg(),IoTVariableCommon.getNumberofmsg()*725,error_rate, "BATTERY");

				}
				
				IoTVariableCommon.setNumberofmsg(0);
				iotvar.get(0).clearDatabase();
				logger.info("Tests finished...");
				IoTVariableCommon.setCompute(false);

				PerformanceStore.setStartComputing(false);

				System.exit(0);

			}
		}
		

	}

}
