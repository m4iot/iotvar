import demo.mudebs.BasicContextConsumer;
import demo.qodisco.TextDisplay;
import iotvar.IoTVariableMuDEBS;
import iotvar.basics.Freshness;
import iotvar.basics.Location;
import mucontext.contextcapsule.BasicContextCapsule;
import mucontext.datamodel.context.ContextDataModelFacade;
import mucontext.datamodel.contextcontract.BasicContextConsumerContract;
import mucontext.datamodel.contextcontract.BasicContextProducerContract;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.OperationalMode;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import simulation.mudebs.MuDEBSBroker;
import simulation.mudebs.MuDEBSRegister;

import javax.xml.transform.TransformerException;
import java.util.concurrent.TimeUnit;

public class TestScenarioMuDEBS {

    private static Thread brokerAThread;
    private static MuDEBSRegister muDEBSRegister;
    private static double payload = 42;
    private static double payload2 = 10;


    /**
     * the MuDEBS filter.
     */
    private static String collectorAdvFilter = "function evaluate(doc) { return true; }";
    private static String capsuleAdvFilter = "function evaluate(doc) {"
            + "load(\"nashorn:mozilla_compat.js\");"
            + "importPackage(javax.xml.xpath);"
            + "var xpath = XPathFactory.newInstance().newXPath();"
            + "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
            + "if (!n1.getTextContent().equalsIgnoreCase('SomethingElse')) return false;"
            + "xpath = XPathFactory.newInstance().newXPath();"
            + "var n2 = xpath.evaluate('//entity/name', n1, XPathConstants.NODE);"
            + "if (!n2.getTextContent().equals('SomebodyElse" +
            "')) return false;"
            + "return true;}";
    private static String capsuleSubFilter = "function evaluate(doc) {"
            + "load(\"nashorn:mozilla_compat.js\");"
            + "importPackage(javax.xml.xpath);"
            + "var xpath = XPathFactory.newInstance().newXPath();"
            + "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
            + "if (!n1.getTextContent().equalsIgnoreCase('Something')) return false;"
            + "xpath = XPathFactory.newInstance().newXPath();"
            + "var n2 = xpath.evaluate('//entity/name', n1, XPathConstants.NODE);"
            + "if (!n2.getTextContent().equals('Somebody')) return false;"
            + "return true;}";

    public static String applicationSubFilter = "function evaluate(doc) {"
            + "load(\"nashorn:mozilla_compat.js\");"
            + "importPackage(javax.xml.xpath);"
            + "var xpath = XPathFactory.newInstance().newXPath();"
            + "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
            + "if (!n1.getTextContent().equalsIgnoreCase('SomethingElse')) return false;"
            + "xpath = XPathFactory.newInstance().newXPath();"
            + "var n2 = xpath.evaluate('//entity/name', n1, XPathConstants.NODE);"
            + "if (!n2.getTextContent().equals('SomebodyElse')) return false;"
            + "return true;}";

    static final String[] basicApplicationArgs ="--uri mudebs://localhost:2102/ApplicationHelloWorld --broker mudebs://localhost:2000/BrokerA".split(" ");
    
    static final String brokerPath = "mudebs://localhost:2000/BrokerA";

    /**
     * Create in specific capsule-thread and consume the data from the collector.
     * @throws MuDEBSException
     * @throws TransformerException
     * @throws InterruptedException
     */
    private static void createACapsule() throws MuDEBSException, TransformerException, InterruptedException {
        BasicContextCapsule capsule = new BasicContextCapsule(("--uri mudebs://localhost:2101/CapsuleHelloWorld --broker " +
                brokerPath).split( " "));
        ContextDataModelFacade facade = new ContextDataModelFacade("facade");
        BasicContextConsumer consumer = new BasicContextConsumer(facade,capsule);
        capsule.setContextConsumerContract(
                new BasicContextConsumerContract("someconsumercontract",
                        OperationalMode.GLOBAL, capsuleSubFilter, null), consumer );
        capsule.setContextProducerContract(new BasicContextProducerContract(
                "someproducercontract", OperationalMode.LOCAL,
                capsuleAdvFilter, null));
    }

    @BeforeClass
    public static void setup() throws Exception {
        brokerAThread = new Thread(new MuDEBSBroker());
        brokerAThread.start();
        muDEBSRegister = new MuDEBSRegister(brokerPath, "mudebs://localhost:2100/CollectorHelloWorld", collectorAdvFilter, "Somebody", "Something");
        createACapsule();
    }

    @Test
    public void main() throws InterruptedException, MuDEBSException {
        TextDisplay<Double> textDisplay = new TextDisplay<>();
        IoTVariableMuDEBS<Double> t1 = new IoTVariableMuDEBS<>("Temperature", new Location("Home", 48.8, 2.3, 9),
                new Freshness(1, TimeUnit.SECONDS), 10, null, basicApplicationArgs, applicationSubFilter);
        IoTVariableMuDEBS<Double> t2 = new IoTVariableMuDEBS<>("Temperature2", new Location("TSP", 32.1, 33.5, 1),
                new Freshness(1, TimeUnit.SECONDS), 10, null, basicApplicationArgs, applicationSubFilter);
        t1.registerIoTListener(textDisplay);
        t2.registerIoTListener(textDisplay);
        Thread.sleep(1000);
        muDEBSRegister.post(payload);
        muDEBSRegister.post(payload2);
        Thread.sleep(4000);
        // 3 observations are expected as the initialization of IoTVariable creates one.
        Assert.assertEquals(2, t1.getObservationInstances().size());
        Assert.assertEquals(2, t2.getObservationInstances().size());

        System.out.println(t2.getObservationInstances().size());
        System.out.println(t1.getObservationInstances().size());

        Assert.assertEquals(payload2, (t1.getLastObservation().getValue()), 0.01);
        Assert.assertEquals(payload2, (t2.getLastObservation().getValue()), 0.01);
    }

    @AfterClass
    public static void tearDown(){
        brokerAThread.interrupt();
    }
}
